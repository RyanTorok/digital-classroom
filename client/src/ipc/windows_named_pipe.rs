use crate::utils::err_handle::ResultBacktrace;
use crate::ipc::{IPCServer, IPCConnection};
use std::io::{Result, Read, Write};

struct WindowsNamedPipeListener {

}

impl WindowsNamedPipeListener {
    pub(crate) fn new(directory: &str, filename: &str) -> ResultBacktrace<WindowsNamedPipeListener> {
        Ok(WindowsNamedPipeListener { })
    }
}

impl IPCServer for WindowsNamedPipeListener {
    type Connection = WindowsNamedPipeConnection;

    fn listen(&mut self, handler: impl FnMut(Self::Connection)) {
        unimplemented!()
    }
}

struct WindowsNamedPipeConnection {

}

impl WindowsNamedPipeConnection {

}

impl Read for WindowsNamedPipeConnection {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        unimplemented!()
    }
}

impl Write for WindowsNamedPipeConnection {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        unimplemented!()
    }

    fn flush(&mut self) -> Result<()> {
        unimplemented!()
    }
}