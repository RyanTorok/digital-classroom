// FILE: wasm_stream.rs
// 
// DESCRIPTION: This file contains a socket API to interface with the backend client installed on the
// user's machine from inside the web client. If the backend is not installed, WasmStream also allows
// the ability to send commands to a virtual session stored on the server.
// 
// CHANGE LOG:
// 24 Dec 2020 -- Initial generation

use web_sys::{MessageEvent, ErrorEvent, CloseEvent, WebSocket};
use std::{
    net::TcpListener,
    io::{Read, Write, Result},
    sync::{
        mpsc::Sender,
        mpsc::Receiver,
        mpsc::channel,
        mpsc::RecvError,
        Arc,
        Mutex
    },
    net::TcpStream,
    io::Error,
    net::Shutdown,
};
use wasm_bindgen::JsValue;
#[cfg(feature = "web")]
use wasm_bindgen::prelude::*;
use js_sys::Uint8Array;
use wasm_bindgen::JsCast;
use crate::{
    utils::receive_queue::{ReceiveQueue, SendMsg, RecvMsg, Length},
    utils::err_handle::{ResultBacktrace, wrap_std_err, leaf_err, wrap_err, ErrSpec, leaf_err_spec, unwrap_or_wrap_std_err},
    ipc::IPCConnection,
    ipc::IPCServer,
    utils::socket_utils::ShutdownSocket
};
use std::io::ErrorKind;
#[cfg(feature = "backend")]
use tungstenite::{
    protocol::CloseFrame,
    Message,
    handshake::server::NoCallback,
    ServerHandshake,
    HandshakeError,
    server::accept,
    protocol::frame::coding::CloseCode
};
use std::borrow::Cow;

const DCL_CLIENT_URL: &str = "wss://digitalclassroom.org:443";

#[cfg(feature = "libdcl-web")]
pub(crate) struct WasmClient {
    sock: web_sys::WebSocket,
    on_message: Closure<dyn FnMut(MessageEvent)>,
    on_close: Closure<dyn FnMut(CloseEvent)>,
    receive_queue: Receiver<std::io::Result<Uint8Array>>,
    current: Uint8Array,
    off: usize,
    arc_tx: Arc<Mutex<Sender<std::result::Result<js_sys::Uint8Array, std::io::Error>>>>
}

#[cfg(feature = "libdcl-web")]
impl WasmClient {
    pub fn new(local_port: u16, server_host: &str, server_port: u16) -> ResultBacktrace<WasmClient> {
        let ws = match WebSocket::new(&format!("wss://localhost:{}", local_port)) {
            Ok(local) => local,
            Err(local_err) => {
                //TODO This throws SECURITY_ERR or an invalid URL error. For the latter, we are
                // doing the correct thing, but for the former, we might want to notify the user
                // that the backend they have installed isn't being used because the port is blocked.
                match WebSocket::new(DCL_CLIENT_URL) {
                    Ok(remote) => remote,
                    Err(remote_err) => {
                        // Uh oh, we couldn't connect to a local or remote backend.
                        return leaf_err("Could not connect to local or remote backend.");
                    }
                }
            }
        };
        ws.set_binary_type(web_sys::BinaryType::Arraybuffer);
        let (tx, rx) = channel();
        let arc_tx = Arc::new(Mutex::new(tx));
        let weak_tx = Arc::downgrade(&arc_tx.clone());
        let onmessage_callback = Closure::wrap(Box::new(move |e: MessageEvent| {
            if let Some(arc_tx) = weak_tx.upgrade() {
                let tx = arc_tx.lock().unwrap();
                if let Ok(abuf) = e.data().dyn_into::<js_sys::ArrayBuffer>() {
                    let array = Uint8Array::new(&abuf);
                    tx.send(Ok(array));
                } else if let Ok(blob) = e.data().dyn_into::<web_sys::Blob>() {
                    /*let fr = web_sys::FileReader::new().unwrap();
                    let fr_c = fr.clone();
                    // create onLoadEnd callback
                    let onloadend_cb = Closure::wrap(Box::new(move |_e: web_sys::ProgressEvent| {
                        let array = js_sys::Uint8Array::new(&fr_c.result().unwrap());
                        tx.send(Ok(array));
                    }) as Box<dyn FnMut(web_sys::ProgressEvent)>);
                    fr.set_onloadend(Some(onloadend_cb.as_ref().unchecked_ref()));*/
                    eprintln!("message event, received Blob");
                } else if let Ok(txt) = e.data().dyn_into::<js_sys::JsString>() {
                    eprintln!("message event, received Text: {:?}", txt);
                } else {
                    eprintln!("message event, received Unknown: {:?}", e.data());
                }
            }
        }) as Box<dyn FnMut(MessageEvent)>);
        ws.set_onmessage(Some(onmessage_callback.as_ref().unchecked_ref()));

        // On close, we our received message queue.
        let onclose_callback = Closure::wrap(Box::new(move |e: CloseEvent| {
            // The memory for `onmessage_callback` is managed by JS, not Rust, so we need to make
            // sure we deallocate `tx` when the socket closes, since `onmessage_callback` will not
            // be run again after this point.

            // Make sure the other closure isn't using tx, then drop its reference counter to
            // deallocate it.
        }) as Box<dyn FnMut(CloseEvent)>);
        ws.set_onclose(Some(onclose_callback.as_ref().unchecked_ref()));
        Ok(WasmClient { sock: ws, on_message: onmessage_callback, on_close: onclose_callback, receive_queue: rx, current: Uint8Array::new_with_length(0), off: 0, arc_tx })
    }
}

#[cfg(feature = "libdcl-web")]
impl Read for WasmClient {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        // Keep reading from current until we run out of bytes, then read the next one.
        let mut read_bytes: usize = 0;
        let len = buf.len();
        loop {
            let to_read: usize = std::cmp::min(buf.len() - read_bytes, self.current.length() as usize - self.off);
            if self.off + to_read >  0xFFFFFFFF {
                return Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
                                               "Received message length exceeded maximum size of JavaScript array"))
            }
            for i in 0..to_read {
                buf[read_bytes + i] = self.current.get_index((self.off + i) as u32);
            }
            read_bytes += to_read;
            self.off += to_read;
            if read_bytes < len {
                match self.receive_queue.recv() {
                    Ok(res_arr) => {
                        match res_arr {
                            Ok(array) => {
                                self.current = array;
                                self.off = 0;
                            }
                            Err(err) => {
                                // An IO error occurred
                                return Err(err);
                            }
                        }
                    },
                    Err(err) => {
                        // If we stop receiving, this is not an actual I/O error, just an internal
                        // notice that we did not get all the bytes we expected because the WebSocket
                        // closed.
                        return Ok(read_bytes);
                    },
                }
            } else {
                return Ok(len);
            }
        }
    }
}

#[cfg(feature = "libdcl-web")]
impl Write for WasmClient {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        match self.sock.send_with_u8_array(buf) {
            Ok(()) => Ok(buf.len()),
            Err(js_value) => Err(std::io::Error::from(std::io::ErrorKind::NotConnected))
        }
    }

    fn flush(&mut self) -> Result<()> {
        // No code here, since send() sends immediately.
        Ok(())
    }
}

#[cfg(feature = "libdcl-web")]
impl ShutdownSocket for WasmClient {
    fn shutdown_socket(&mut self, which: Shutdown) {
        self.sock.close();
    }
}

#[cfg(feature = "backend")]
pub(crate) struct WasmServer {
    listener: TcpListener
}

#[cfg(feature = "backend")]
impl WasmServer {
    pub fn new(host: &str, port: u16) -> WasmServer {
        WasmServer { listener: TcpListener::bind(format!("{}{}", host, port)).unwrap() }
    }
}

#[cfg(feature = "backend")]
impl IPCServer for WasmServer {

    type Connection = ReceiveQueue<tungstenite::WebSocket<TcpStream>, Vec<u8>>;

    fn accept(&self) -> ResultBacktrace<Self::Connection> {
        match self.listener.accept() {
            Ok(stream) => {
                match tungstenite::accept(stream.0) {
                    Ok(web_socket) => {
                        Ok(ReceiveQueue::new(web_socket))
                    },
                    Err(e) => {
                        wrap_std_err("WebSocket handshake failed.", e)
                    },
                }
            }
            Err(err) => { wrap_std_err("Could not accept WebSocket TcpStream from WasmServer", err) }
        }
    }

    fn handle_all(&mut self, handler: fn(Self::Connection)) {
        for res_conn in self.listener.incoming() {
            match res_conn {
                Ok(conn) => {
                    match tungstenite::accept(conn) {
                        Ok(web_socket) => {
                            handler(ReceiveQueue::new(web_socket))
                        },
                        Err(e) => {
                            eprintln!("ERROR: WebSocket handshake failed because of an exception: {}", e);
                        },
                    }
                },
                Err(e) => {
                    eprintln!("ERROR: WebSocket stream failed to connect: {}", e);
                },
            }
        }
    }
}

#[cfg(feature = "backend")]
impl SendMsg for tungstenite::WebSocket<TcpStream> {
    fn send(&mut self, msg: &[u8]) -> ResultBacktrace<()> {
        unwrap_or_wrap_std_err(
            self.write_message(tungstenite::Message::Binary(Vec::from(msg))),
            "Could not send message because of a WebSocket exception."
        )
    }
}

#[cfg(feature = "backend")]
impl RecvMsg<Vec<u8>> for tungstenite::WebSocket<TcpStream> {
    fn receive(&mut self) -> std::io::Result<(Vec<u8>, bool)> {
        match self.read_message() {
            Ok(msg) => {
                Ok(match msg {
                    Message::Text(s) => {
                        // This does not copy.
                        (Vec::from(s), false)
                    }
                    Message::Binary(vec) => {
                        (vec, false)
                    }
                    // TODO these three results need special handling.
                    Message::Ping(p) => {
                        (p, false)
                    }
                    Message::Pong(p) => {
                        (p, false)
                    }
                    Message::Close(c) => {
                        (vec![], true)
                    }
                })
            }
            Err(e) => {
                Err(std::io::Error::new(ErrorKind::Other, leaf_err_spec(e)))
            }
        }
    }
}

#[cfg(feature = "backend")]
impl ShutdownSocket for tungstenite::WebSocket<TcpStream> {
    fn shutdown_socket(&mut self, _which: Shutdown) {
        self.close(Some(CloseFrame {
            code: CloseCode::Normal, reason: Cow::from("Normal Socket Shutdown.")
        }));
    }
}

#[cfg(feature = "backend")]
fn unwrap_websocket_error(e: tungstenite::error::Error) -> ResultBacktrace<()> {
    match e {
        tungstenite::error::Error::ConnectionClosed => {Ok(())}
        tungstenite::error::Error::AlreadyClosed => {leaf_err("Attempted to use a WebSocket connection that was already closed.")}
        tungstenite::error::Error::Io(io_error) => {leaf_err(&format!("A Fatal IO error occurred in the WebSocket: {}", io_error))}
        tungstenite::error::Error::Tls(tls_error) => {leaf_err(&format!("A Fatal TLS error occurred in the WebSocket: {}", tls_error))}
        tungstenite::error::Error::Capacity(cap_error) => {leaf_err(&format!("A WebSocket capacity error occurred: {}", cap_error))}
        tungstenite::error::Error::Protocol(pro_error) => {leaf_err(&format!("A WebSocket protocol error occurred: {}", pro_error))}
        tungstenite::error::Error::SendQueueFull(_) => {leaf_err("A WebSocket message failed to send because the send queue was full.")}
        tungstenite::error::Error::Utf8 => {leaf_err("WebSocket string message was rejected because it was not valid UTF-8.")}
        tungstenite::error::Error::Url(url_error) => {leaf_err(&format!("WebSocket received a URL error: {}", url_error))}
        tungstenite::error::Error::Http(http_error) => {
            leaf_err(&format!("WebSocket received an HTTP error code: {} ({}).", http_error.as_str(),
                              match http_error.canonical_reason() {
                                  None => {""}
                                  Some(s) => {s}
                              })
            )}
        tungstenite::error::Error::HttpFormat(fmt_error) => {wrap_std_err("WebSocket received an HTTP format error.", fmt_error)}
    }
}