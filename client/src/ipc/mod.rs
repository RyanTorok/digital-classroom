// FILE: ipc/mod.rs
//
// DESCRIPTION: Specifies a high-level API for several interprocess communication (IPC) methods
// across different platforms.
//
// CHANGE LOG:
// 27 Jan 2021 -- Added docs header

use crate::utils::err_handle::{ResultBacktrace, ErrSpec, not_implemented};
use std::io::{Read, Write};
use std::io::Result;
use std::path::Iter;
use std::net::Incoming;
#[cfg(all(not(feature = "web"), target_family = "unix"))]
use crate::ipc::unix_socket::UnixSocketListener;
#[cfg(all(not(feature = "web"), target_family = "unix"))]
use std::os::unix::net::UnixStream;
#[cfg(feature = "web")]
use crate::ipc::wasm_stream::WasmClient;
#[cfg(feature = "backend")]
use crate::ipc::wasm_stream::WasmServer;
use std::net::Shutdown;
use crate::utils::socket_utils::ShutdownSocket;

#[cfg(all(feature = "desktop", target_family = "unix"))]
pub(crate) mod unix_socket;
#[cfg(any(feature = "web", feature = "backend"))]
pub(crate) mod wasm_stream;

#[cfg(target_family = "windows")]
pub(crate) mod windows_named_pipe;

pub(crate) enum ConnectionMode<'a> {
    #[cfg(all(feature = "desktop", target_family = "unix"))]
    UnixSocket(&'a str), //path: &'a str

    #[cfg(all(feature = "desktop", target_family = "unix"))]
    UnixNamedPipe,

    #[cfg(feature = "web")]
    WebSocket(&'a str, u16),

    #[cfg(target_family = "windows")]
    WindowsSocket(&'a str),

    #[cfg(target_family = "windows")]
    WindowsNamedPipe,

    SharedMemory
}

#[cfg(feature = "backend")]
pub(crate) fn new_listener(mode: ConnectionMode) -> ResultBacktrace<impl IPCServer> {
    match mode {
        #[cfg(all(feature = "desktop", target_family = "unix"))]
        ConnectionMode::UnixSocket(path) => {
            UnixSocketListener::new(path)
        },
        #[cfg(all(feature = "desktop", target_family = "unix"))]
        ConnectionMode::UnixNamedPipe => {
            unimplemented!()
        },
        #[cfg(target_feature = "web")]
        ConnectionMode::WebSocket(host, port) => {
          WasmServer::new(host, port)
        },
        #[cfg(all(feature = "desktop", target_family = "windows"))]
        ConnectionMode::WindowsSocket(_, _) => {
            not_implemented()
        },
        #[cfg(all(feature = "desktop", target_family = "windows"))]
        ConnectionMode::WindowsNamedPipe => {
            not_implemented()
        },
        #[cfg(feature = "desktop")]
        ConnectionMode::SharedMemory => {
            unimplemented!()
        },
    }
}

#[cfg(all(feature = "desktop", target_family = "unix"))]
pub(crate) type IPCConnection = UnixStream;
#[cfg(all(feature = "desktop", target_family = "windows"))]
pub(crate) type IPCConnection = WindowsNamedPipe;
#[cfg(feature = "web")]
pub(crate) type IPCConnection = WasmClient;

pub(crate) trait IPCServer {

    type Connection : Read + Write + ShutdownSocket;

    fn accept(&self) -> ResultBacktrace<Self::Connection>;

    //A workaround because Rust doesn't like us iterating over a placeholder type from a trait
    fn handle_all(&mut self, handler: fn(Self::Connection));
}


/*
pub(crate) struct Incoming<'a, T : IPCConnection> {

    listener: &'a dyn IPCServer<Connection = T>
}

impl <'a, T : IPCConnection> Iterator for Incoming<'a, T> {
    type Item = ResultBacktrace<T>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.listener.accept())
    }

    //TODO not sure what this does, copied from rust library's implementation of UnixListener's Incoming
    fn size_hint(&self) -> (usize, Option<usize>) {
        (usize::max_value(), None)
    }
}
*/
