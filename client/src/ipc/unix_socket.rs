use std::os::unix::net::{UnixListener, UnixStream, Incoming};
use crate::ipc::{IPCConnection, IPCServer};
use crate::utils::socket_utils::ShutdownSocket;
use crate::utils::err_handle::{ResultBacktrace, wrap_err, wrap_or_wrap_err, unwrap_or_wrap_err, unwrap_or_wrap_std_err, leaf_err_spec, leaf_err, wrap_std_err, ErrSpec, wrap_result_or_wrap_err};
use std::path::{Path, PathBuf};
use std::thread;
use std::io::{Error, Read, Write};
use std::process::Command;
use std::io::Result;
use crate::system::linux_syscalls::chmod;
use std::ffi::OsStr;
use std::net::Shutdown;

pub(crate) struct UnixSocketListener {
    path: PathBuf,
    listener: UnixListener,
}

impl UnixSocketListener {
    ///
    /// Constructs a new UnixSocketConnection implementation of IPC.
    ///
    pub(crate) fn new(path: &str) -> ResultBacktrace<UnixSocketListener> {
        let full_path = Path::new(path);
        if full_path.is_dir() {
            return leaf_err(format!("Cannot create socket on {}: is a directory.", path).as_ref())
        }
        let opt_directory = full_path.parent();
        if opt_directory.is_none() {
            return leaf_err(format!("Cannot create socket on {}: file has no parent.", path).as_ref())
        }
        let directory = opt_directory.unwrap();
        let opt_filename = full_path.file_name();
        if opt_filename.is_none() {
            return leaf_err(format!("Cannot create socket on {}: path has no filename", path).as_ref())
        }
        let filename = opt_filename.unwrap();
        let result = UnixSocketListener::init_socket_dir(&directory, filename);
        wrap_result_or_wrap_err(result, |_| {
            let mut buf = directory.to_path_buf();
            buf.push(Path::new(filename));
            let socket_path = buf.as_path();

            let listener = unwrap_or_wrap_std_err(UnixListener::bind(socket_path), "Could not initialize UnixListener")?;
            chmod(socket_path.as_os_str(), 0o777);
            Ok(UnixSocketListener { path: socket_path.to_owned(), listener })
        }, "Could not start socket listener.")
    }

    fn init_socket_dir(socket_path: &Path, socket_filename: &OsStr) -> ResultBacktrace<()> {
        if !socket_path.exists() {
            return unwrap_or_wrap_std_err(std::fs::create_dir_all(socket_path), "Could not create socket directory");
        } else {
            let mut buf = socket_path.to_path_buf();
            buf.push(Path::new(socket_filename));
            let path = buf.as_path();
            // We can't create a socket if the socket file already exists.
            // We should be the only instance running, so we're safe to delete the old socket file.
            if path.exists() {
                let result = std::fs::remove_file(path);
                if result.is_err() {
                    return wrap_err("Could not remove old socket file.", leaf_err_spec(result.unwrap_err().to_string().as_str()));
                }
            }
        }
        Ok(())
    }

    /*// Starts the Unix Server Socket to receive requests from programs started by calling attach() below.
    // This function should never return unless there is an error.
    pub(crate) fn start_listener(path: &str) -> ResultBacktrace<()> {
        let path = Path::new(SOCKET_PATH);
        let result = init_socket_dir(&path);
        if result.is_err() {
            return wrap_err("Could not start socket listener.", result.unwrap_err());
        }
        let mut buf = path.to_path_buf().to_path_buf();
        buf.push(Path::new(SOCKET_FILE_NAME));
        let socket_path = buf.as_path();

        let _listener: std::Result<std::os::unix::net::UnixListener, Error> = UnixListener::bind(socket_path);
        //TODO for debug only, need more robust solution
        let result1 = Command::new("chmod").arg("777").arg("/usr/share/digital-classroom/sockets/gui.sock").output();
        let listener: UnixListener;
        if _listener.is_ok() {
            listener = _listener.unwrap();
        } else {
            panic!("listener was null. {}", _listener.unwrap_err().to_string());
        }

        for mut stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    /* connection succeeded */
                    //TODO get error back on failure
                    thread::spawn(|| handle_gui_client(stream));
                    eprintln!("Connection success.");
                }
                Err(err) => {
                    /* connection failed */
                    eprintln!("Connection failed.");
                    break;
                }
            }
        }
        //should never get here
        Ok(())
    }*/
}

impl IPCServer for UnixSocketListener {
    type Connection = UnixStream;

    fn accept(&self) -> ResultBacktrace<Self::Connection> {
        match self.listener.accept() {
            Ok(stream) => {
                Ok(stream.0)
            }
            Err(err) => { wrap_std_err("Could not accept UnixStream from IPCListener", err) }
        }
    }

    fn handle_all(&mut self, handler: fn(IPCConnection)) {
        for res_stream in self.listener.incoming() {
            match res_stream {
                Ok(stream) => {
                    thread::spawn(move || {
                        handler(stream);
                    });
                },
                Err(e) => {
                    eprintln!("ERROR: stream failed to connect: {}", e);
                }
            }
        }
    }
}

impl Iterator for UnixSocketListener {
    type Item = UnixStream;

    fn next(&mut self) -> Option<Self::Item> {
        match self.accept() {
            Ok(stream) => { Some(stream) }
            Err(err) => {
                eprintln!("Could not accept UnixStream from IPCListener: {}", err.to_string());
                None
            }
        }
    }
}

impl ShutdownSocket for UnixStream {
    fn shutdown_socket(&mut self, which: Shutdown) {
        self.shutdown(which);
    }
}


