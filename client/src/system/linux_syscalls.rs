extern crate libc;

use std::ffi::{CString, OsStr};
use libc::c_int;
use libc::c_void;
use crate::utils::err_handle::{ErrSpec, leaf_err};
use std::convert::TryFrom;
use std::os::unix::ffi::OsStrExt;


pub(crate) fn fork() -> i32 {
    unsafe { return libc::fork(); }
}

pub(crate) fn execl(path: &str, arg: &str) -> Result<(), ErrSpec> {
    let code: c_int;
    //Don't combine these into one line because it will cause premature deallocation.
    let path_c_string_owned = CString::new(path).expect("CString failed");
    let path_c_string = ((path_c_string_owned.as_ptr()) as u64 + 8) as *const i8;
    //Same with these
    let arg_c_string_owned = CString::new(arg).expect("CString failed");
    let arg_c_string = ((arg_c_string_owned.as_ptr()) as u64 + 8) as *const i8;

    code = unsafe { libc::execl(path_c_string, path_c_string, arg_c_string, 0 as *const libc::c_schar) };

    leaf_err(format!("Failed to exec {} with error code {}.", path, code).as_str())
}

pub(crate) fn write(fd: usize, buf: *const u8, count: usize) -> isize {
    unsafe { libc::write(fd as c_int, buf as *const libc::c_void, count) }
}

#[cfg(target_family = "unix")]
pub(crate) fn chmod (path: &OsStr, mode: usize) -> i32 {
    //Don't combine these two lines because it would result in premature deallocation
    let path_c_string = CString::new(path.as_bytes()).expect("CString failed");
    let path_ptr: *const libc::c_char = path_c_string.as_ptr();
    unsafe { libc::chmod(path_ptr, u32::try_from(mode).expect("Value was too large")) }
}

