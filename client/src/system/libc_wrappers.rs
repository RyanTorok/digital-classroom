use libc::c_char;
use std::ffi::{CString, CStr};
use std::str::Utf8Error;

#[cfg(target_family = "unix")]
pub(crate) fn mkfifo(path: &str, mode: u32) -> i32 {
    /*
        We need the CString as its own variable otherwise it might get deallocated before 
        it's used in the unsafe block.
    */
    let path_c_string = CString::new(path).expect("CString failed");
    let ptr =   path_c_string.as_ptr() as *const libc::c_char;
    unsafe { libc::mkfifo(ptr, mode as libc::mode_t) }
}
