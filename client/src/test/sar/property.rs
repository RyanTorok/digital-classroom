// FILE: test/sar/property.rs
// 
// DESCRIPTION: 
// 
// CHANGE LOG:
// 14 Nov 2020 -- Initial generation

pub(crate) trait Property<T> {
    fn verify() -> T;
}

