use crate::command_utils::params::{Argument, ReturnValue};
use crate::command_executor::command_response::CommandResponse;
use crate::command_executor::command::Command;
use crate::commands::cmd_decode_error;
use crate::utils::byte_utils::ByteWidth;
use crate::utils::byte_utils::SignMode;
use byteorder::{LittleEndian, ByteOrder};
use std::str::{from_utf8, Utf8Error};
use crate::utils::err_handle::{ResultBacktrace, ErrSpec, leaf_err};
use crate::command_executor::timing::CommandTimingData;
use crate::command_utils::type_codes::*;
use crate::ipc::IPCConnection;
use std::io::Read;
use std::time::Duration;
use uuid::Uuid;

///
/// Decodes the command byte encoding used by the GUI socket and returns a Command object for
/// the command which can be passed directly to the executor.
///
/// Command encoding structure:
///
/// Command Opcode  [2 bytes]
/// Argument Count  [2 bytes]
/// Argument Types  [1 byte per argument]
/// Argument Values [Size given by argument types] -- String arguments store their lengths here (4 bytes)
/// String contents [Size given by argument values]
///
/// This structure allows command validation when only the first packet has been received.
///
/// We have to return the arguments along with the command because the command contains references to
/// the argument array, which itself can contain string slices that reference the bytes in `encoding`.
/// This is also the purpose of the lifetime 'a. It specifies that the function that eventually executes
/// the command may only live as long as the argument data read from the socket, since the command borrows
/// data from the read bytes.
///
/// Returned tuple contains command response and a boolean dictating whether the socket should
/// be closed as a result (opcode 0, which is read by default if the client closed the socket).
///

pub(crate) fn execute_gui_command(stream: &mut impl Read) -> (CommandResponse, bool) {
    let mut res_response = || -> Result<(CommandResponse, bool), std::io::Error> {
        let mut buffer = [0u8; 16];

        // read opcode and argument value count
        stream.read_exact(&mut buffer[0..4])?;

        let opcode: u16 = LittleEndian::read_u16(&buffer[0..2]);
        //println!("opcode = {}", opcode);

        if opcode == 0 {
            // shut down socket
            return Ok((CommandResponse::new(Duration::from_millis(0), vec![]), true));
        }

        let arg_count = LittleEndian::read_u16(&buffer[2..4]) as usize;
        //println!("arg_count = {}", arg_count);
        let mut string_lengths = vec![0u32; arg_count];
        let arg_types_buf: &mut [u8];
        let mut vec;
        if arg_count <= 16 {
            // We have <= 16 bytes of return types, so we can reuse the buffer we already have on the stack.
            arg_types_buf = &mut buffer[0..arg_count];
        } else {
            // Otherwise we need a new vector on the heap that can fit enough return types.
            vec = vec![0u8; arg_count];
            arg_types_buf = vec.as_mut_slice();
        }
        // Read return types into buffer
        stream.read_exact(arg_types_buf)?;
        
        let mut arguments = vec![];

        // Find out the total size of the return values so we only have to invoke the receive syscall once.
        let mut value_size: usize = 0;
        for i in 0..arg_count {
            match arg_types_buf[i] {
                ENCODING_U8 | ENCODING_I8 => {
                    value_size += 1;
                }
                ENCODING_U16 | ENCODING_I16 => {
                    value_size += 2;
                }
                // String is here because the length is stored as the value as a u32.
                ENCODING_U32 | ENCODING_I32 | ENCODING_F32 | ENCODING_STRING => {
                    value_size += 4;
                }
                ENCODING_U64 | ENCODING_I64 | ENCODING_F64 => {
                    value_size += 8;
                }
                ENCODING_U128 | ENCODING_I128 | ENCODING_UUID => {
                    value_size += 16;
                }
                _ => {
                    return Ok((argument_decode_error(format!("Invalid argument type code for argument {}: {}", i + 1, arg_types_buf[i])), false));
                }
            };
        }

        // Read the return values
        let mut value_bytes = vec![0u8; value_size];
        stream.read_exact(&mut value_bytes)?;
        let mut values_offset = 0;
        arguments.extend(std::iter::repeat(Argument::U8(0)).take(arg_count));
        for i in 0..arg_count {
            match arg_types_buf[i] {
                ENCODING_U8 => {
                    arguments[i] = Argument::U8(value_bytes[values_offset]);
                    values_offset += 1;
                }
                ENCODING_U16 => {
                    arguments[i] = Argument::U16(LittleEndian::read_u16(&value_bytes[values_offset..values_offset + 2]));
                    values_offset += 2;
                }
                ENCODING_U32 => {
                    arguments[i] = Argument::U32(LittleEndian::read_u32(&value_bytes[values_offset..values_offset + 4]));
                    values_offset += 4;
                }
                ENCODING_U64 => {
                    arguments[i] = Argument::U64(LittleEndian::read_u64(&value_bytes[values_offset..values_offset + 8]));
                    values_offset += 8;
                }
                ENCODING_U128 => {
                    arguments[i] = Argument::U128(LittleEndian::read_u128(&value_bytes[values_offset..values_offset + 16]));
                    values_offset += 16;
                }
                ENCODING_I8 => {
                    arguments[i] = Argument::I8(value_bytes[values_offset] as i8);
                    values_offset += 1;
                }
                ENCODING_I16 => {
                    arguments[i] = Argument::I16(LittleEndian::read_i16(&value_bytes[values_offset..values_offset + 2]));
                    values_offset += 2;
                }
                ENCODING_I32 => {
                    arguments[i] = Argument::I32(LittleEndian::read_i32(&value_bytes[values_offset..values_offset + 4]));
                    values_offset += 4;
                }
                ENCODING_I64 => {
                    arguments[i] = Argument::I64(LittleEndian::read_i64(&value_bytes[values_offset..values_offset + 8]));
                    values_offset += 8;
                }
                ENCODING_I128 => {
                    arguments[i] = Argument::I128(LittleEndian::read_i128(&value_bytes[values_offset..values_offset + 16]));
                    values_offset += 16;
                }
                ENCODING_F32 => {
                    arguments[i] = Argument::F32(LittleEndian::read_f32(&value_bytes[values_offset..values_offset + 4]));
                    values_offset += 4;
                }
                ENCODING_F64 => {
                    arguments[i] = Argument::F64(LittleEndian::read_f64(&value_bytes[values_offset..values_offset + 8]));
                    values_offset += 8;
                }
                ENCODING_UUID => {
                    arguments[i] = Argument::Uuid(uuid::Uuid::from_u128(LittleEndian::read_u128(&value_bytes[values_offset..values_offset + 16])));
                    values_offset += 16;
                }
                ENCODING_STRING => {
                    string_lengths[i] = LittleEndian::read_u32(&value_bytes[values_offset..values_offset + 4]);
                    values_offset += 4;
                }
                _ => {
                    unreachable!("Should have caught an invalid type code in the previous loop.")
                }
            };
        }
        //Add string contents
        for i in 0..arg_count {
            match arg_types_buf[i] {
                ENCODING_STRING => {
                    let length = string_lengths[i] as usize;
                    let mut string_buf = vec![0u8; length];
                    stream.read_exact(&mut string_buf);
                    arguments[i] = Argument::String(match String::from_utf8(string_buf) {
                        Ok(string) => string,
                        Err(e) => {
                            return Ok((argument_decode_error(format!("Got non-UTF-8 string in return value index: {}", i)), false));
                        }
                    })
                }
                ENCODING_BINARY => {
                    let length = string_lengths[i] as usize;
                    let mut string_buf = vec![0u8; length];
                    stream.read_exact(&mut string_buf);
                    arguments[i] = Argument::Binary(string_buf);
                }
                _ => {}
            };
        }
        Ok((Command::new(opcode, arguments).execute(), false))
    };
    let result = res_response();
    match result {
        Ok(response) => { response }
        Err(e) => {
            //(argument_decode_error(format!("Not enough bytes received from socket on command return: {}", e)), false)
            // This branch only runs if the socket is closed, so we should break out of our loop.
            (CommandResponse::new(Duration::from_millis(0), vec![]), true)
        }
    }
}

#[inline]
fn argument_decode_error(message: String) -> CommandResponse {
    Command::decode_error(message).execute()
}

/**
 * Decodes a plaintext command from the command-line interface and returns a Command object for
 * the command which can be passed directly to the executor.
 */
pub(crate) fn decode_plaintext_command(command: &str) -> Command {
    let mut split = command.split_whitespace();
    match split.next() {
        Some(opcode_str) => {
            Command::decode_error(String::from("unimplemented"))
        }
        None => {
            Command::decode_error(String::from("Empty command received"))
        }
    };
    Command::decode_error(String::from("Placeholder"))
}

///
/// Pushes the given number of bytes to the end of the given vec and returns a pointer
/// to the added bytes
///
fn push_bytes(vec: &mut Vec<u8>, num: usize) -> &mut [u8] {
    let len = vec.len();
    for _ in 0..num {
        vec.push(0);
    }
    &mut vec[len..len+num]
}

///
/// encode_response() places the byte encoding of the given command response in the buffer Vec.
/// The buf need not be empty, and any data already present will not be modified. All data added by this
/// function will be added after the data already present.
///
/// The command response data is formatted identically to the encoding used for incoming commands
/// by execute_gui_command(), except that the 2 bytes for the command opcode are used instead for the
/// execution time, in milliseconds, or 0xFFFF if the total time exceeds a 16 bit unsigned integer.
///
pub(crate) fn encode_response(buf: &mut Vec<u8>, response: CommandResponse, timing_data: CommandTimingData) -> (usize, usize, bool) {
    let mut offset = buf.len();
    // The lengths of the timing data and return types are known, so we can extend ahead of time.
    buf.resize(buf.len() + 4 + response.return_values.len(), 0);
    //append timing data
    LittleEndian::write_u16(&mut buf[offset..offset + 2], timing_data.total_time_millis());
    offset += 2;
    //append return type count
    LittleEndian::write_u16(&mut buf[offset..offset + 2], response.return_values.len() as u16);
    offset += 2;
    //calculate return types/values in correct byte format
    let mut return_types = &mut buf[offset..offset + response.return_values.len()];
    let mut return_values = vec![0u8; 0];
    let mut index: usize = 0;
    let ref_return_types = &response.return_values;
    let mut need_strings_or_binary = false;
    for arg in ref_return_types {
        match arg {
            ReturnValue::U8(val) => {
                return_types[index] = ENCODING_U8;
                return_values.push(*val);
            },
            ReturnValue::U16(val) => {
                return_types[index] = ENCODING_U16;
                let ptr = push_bytes(&mut return_values, 2);
                LittleEndian::write_u16(ptr, *val);
            },
            ReturnValue::U32(val) => {
                return_types[index] = ENCODING_U32;
                let ptr = push_bytes(&mut return_values, 4);
                LittleEndian::write_u32(ptr, *val);
            },
            ReturnValue::U64(val) => {
                return_types[index] = ENCODING_U64;
                let ptr = push_bytes(&mut return_values, 8);
                LittleEndian::write_u64(ptr, *val);
            },
            ReturnValue::U128(val) => {
                return_types[index] = ENCODING_U128;
                let ptr = push_bytes(&mut return_values, 16);
                LittleEndian::write_u128(ptr, *val);
            },
            ReturnValue::I8(val) => {
                return_types[index] = ENCODING_I8;
                return_values.push(*val as u8);
            },
            ReturnValue::I16(val) => {
                return_types[index] = ENCODING_I16;
                let ptr = push_bytes(&mut return_values, 2);
                LittleEndian::write_i16(ptr, *val);
            },
            ReturnValue::I32(val) => {
                return_types[index] = ENCODING_I32;
                let ptr = push_bytes(&mut return_values, 4);
                LittleEndian::write_i32(ptr, *val);
            },
            ReturnValue::I64(val) => {
                return_types[index] = ENCODING_I64;
                let ptr = push_bytes(&mut return_values, 8);
                LittleEndian::write_i64(ptr, *val);
            },
            ReturnValue::I128(val) => {
                return_types[index] = ENCODING_I128;
                let ptr = push_bytes(&mut return_values, 16);
                LittleEndian::write_i128(ptr, *val);
            },
            ReturnValue::F32(val) => {
                return_types[index] = ENCODING_F32;
                let ptr = push_bytes(&mut return_values, 4);
                LittleEndian::write_f32(ptr, *val);
            },
            ReturnValue::F64(val) => {
                return_types[index] = ENCODING_F64;
                let ptr = push_bytes(&mut return_values, 8);
                LittleEndian::write_f64(ptr, *val);
            },
            ReturnValue::Uuid(uuid) => {
                return_types[index] = ENCODING_UUID;
                let ptr = push_bytes(&mut return_values, 16);
                LittleEndian::write_u128(ptr, uuid.as_u128())
            },
            ReturnValue::String(string) => {
                need_strings_or_binary = true;
                return_types[index] = ENCODING_STRING;
                let ptr = push_bytes(&mut return_values, 4);
                // write the string length - we'll deal with the contents after we merge everything
                // to avoid copying the string contents.
                LittleEndian::write_u32(ptr, string.len() as u32);
            },
            ReturnValue::Binary(bytes) => {
                need_strings_or_binary = true;
                return_types[index] = ENCODING_BINARY;
                let ptr = push_bytes(&mut return_values, 4);
                // write the binary length - we'll deal with the contents after we merge everything
                // to avoid copying the string contents.
                LittleEndian::write_u32(ptr, bytes.len() as u32);
            }
            _ => { unreachable!("Remaining types are for internal use only") }
        }
        index += 1;
    }
    let structure = (return_types.len(), return_values.len(), need_strings_or_binary);
    buf.append(return_values.as_mut());

    //append string contents
    for arg in response.return_values {
        match arg {
            ReturnValue::String(string) => {
                buf.append(&mut string.into_bytes());
            },
            ReturnValue::Binary(bytes) => {
                buf.extend_from_slice(&bytes);
            }

            _ => {
                //Any number types already had their values added in the previous section.
            }
        }
    }
    structure
}
