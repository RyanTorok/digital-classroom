use crate::command_utils::params::ReturnValue;
use std::time::Duration;

pub(crate) struct CommandResponse {
    pub time_millis: u128,
    pub return_values: Vec<ReturnValue>
}

impl CommandResponse {
    pub(crate) fn new(time: Duration, return_values: Vec<ReturnValue>) -> CommandResponse {
        CommandResponse { time_millis: time.as_millis(), return_values }
    }
}
