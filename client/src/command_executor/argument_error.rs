// FILE: command_executor/argument_error.rs
//
// DESCRIPTION: The ArgumentError struct represents how a command execution failed because of the
// wrong number or type of arguments.
//
// CHANGE LOG:
// 12 Oct 2020 -- Initial Generation

use crate::command_utils::params::{Argument, ReturnValue};

pub const COMMAND_NOT_FOUND: u8 = 0x0;
pub const TOO_FEW_ARGUMENTS: u8 = 0x1;
pub const TOO_MANY_ARGUMENTS: u8 = 0x2;
pub const INCORRECT_TYPE: u8 = 0x3;

pub const CODE_U8: u8 = 0;
pub const CODE_U16: u8 = 1;
pub const CODE_U32: u8 = 2;
pub const CODE_U64: u8 = 3;
pub const CODE_U128: u8 = 4;
pub const CODE_I8: u8 = 5;
pub const CODE_I16: u8 = 6;
pub const CODE_I32: u8 = 7;
pub const CODE_I64: u8 = 8;
pub const CODE_I128: u8 = 9;
pub const CODE_UUID: u8 = 10;
pub const CODE_STRING: u8 = 11;

pub(crate) enum ArgumentError<'a> {
    CommandNotFound(u16),
    TooFewArguments(u8),
    TooManyArguments(u8, u8),
    IncorrectType(u8, Argument<'a>, &'a Argument<'a>)
}

impl ArgumentError<'_> {

    fn encode_arg<'a>(arg: &Argument<'a>) -> ReturnValue {
        match arg {
            Argument::U8(val) => { ReturnValue::U8(*val) },
            Argument::U16(val) => { ReturnValue::U16(*val) },
            Argument::U32(val) => { ReturnValue::U32(*val) },
            Argument::U64(val) => { ReturnValue::U64(*val) },
            Argument::U128(val) => { ReturnValue::U128(*val) },
            Argument::I8(val) => { ReturnValue::I8(*val) },
            Argument::I16(val) => { ReturnValue::I16(*val) },
            Argument::I32(val) => { ReturnValue::I32(*val) },
            Argument::I64(val) => { ReturnValue::I64(*val) },
            Argument::I128(val) => { ReturnValue::I128(*val) },
            Argument::F32(val) => { ReturnValue::F32(*val) },
            Argument::F64(val) => { ReturnValue::F64(*val) },
            Argument::Uuid(val) => { ReturnValue::Uuid(uuid::Uuid::from_u128(val.as_u128())) },
            Argument::String(val) => { ReturnValue::String(val.to_string()) },
            Argument::StringSlice(val) => { ReturnValue::String(val.to_string()) },
            _ => { panic!("Type error caused by internally generated command argument type.") }
        }
    }

    pub(crate) fn encode(&self) -> Vec<ReturnValue> {
        match self {
            ArgumentError::CommandNotFound(opcode) => {
                vec![ReturnValue::U8(COMMAND_NOT_FOUND), ReturnValue::U16(*opcode)]
            },
            ArgumentError::TooFewArguments(expected) => {
                vec![ReturnValue::U8(TOO_FEW_ARGUMENTS), ReturnValue::U8(*expected)]
            },
            ArgumentError::TooManyArguments(expected, actual) => {
                vec![ReturnValue::U8(TOO_FEW_ARGUMENTS), ReturnValue::U8(*expected), ReturnValue::U8(*actual)]
            },
            ArgumentError::IncorrectType(index, expected, actual) => {
                vec![ReturnValue::U8(INCORRECT_TYPE), ReturnValue::U8(*index), Self::encode_arg(expected), Self::encode_arg(actual)]
            },
        }
    }
}