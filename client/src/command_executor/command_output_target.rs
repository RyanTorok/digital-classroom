use crate::ipc::IPCConnection;

pub(crate) enum CommandOutputTarget<'a> {
    Console,
    Socket(&'a mut IPCConnection)
}

