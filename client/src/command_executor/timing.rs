use std::time::{Instant, Duration};

pub(crate) struct CommandTimingData {
    submit_time: Instant,
    time_submit_start: Duration,
    time_start_end: Duration,
}

impl CommandTimingData {
    pub(crate) fn new(submit_time: Instant, time_submit_start: Duration, time_start_end: Duration) -> CommandTimingData {
        CommandTimingData { submit_time, time_submit_start, time_start_end }
    }

    ///
    /// Returns the total execution time in milliseconds. If the time is > 65535 milliseconds,
    /// 65535 (0xFFFF) is returned.
    ///
    pub(crate) fn total_time_millis(&self) -> u16 {
        let elapsed = self.time_submit_start.as_millis() + self.time_start_end.as_millis();
        if elapsed > 0xFFFF {
            0xFFFF
        } else {
            elapsed as u16
        }
    }
}