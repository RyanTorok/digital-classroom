use crate::command_utils::params::Argument;
use crate::command_executor::argument_error::ArgumentError;

pub(crate) fn unwrap_arg_uint8<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<u8, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::U8(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::U8(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_uint16<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<u16, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::U16(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::U16(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_uint32<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<u32, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::U32(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::U32(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_uint64<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<u64, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::U64(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::U64(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_uint128<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<u128, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::U128(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I128(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_int8<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<i8, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::I8(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I8(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_int16<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<i16, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::I16(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I16(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_int32<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<i32, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::I32(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I32(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_int64<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<i64, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::I64(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I64(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_int128<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<i128, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::I128(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I128(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_uuid<'a>(arguments: &'a [Argument<'a>], index: u8, last: bool) -> Result<uuid::Uuid, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::Uuid(argument) => Ok(*argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::I128(0), argument))
        }
    }
}

pub(crate) fn unwrap_arg_string<'a>(arguments: &'a Vec<Argument<'a>>, index: u8, last: bool) -> Result<&'a str, ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::String(argument) => Ok(argument),
            Argument::StringSlice(argument) => Ok(argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::StringSlice(""), argument))
        }
    }
}

static EMPTY_VEC: Vec<u8> = Vec::new();

pub(crate) fn unwrap_arg_binary<'a>(arguments: &'a Vec<Argument<'a>>, index: u8, last: bool) -> Result<&'a [u8], ArgumentError<'a>> {
    if index >= arguments.len() as u8 {
        Err(ArgumentError::TooFewArguments(index))
    } else if last && arguments.len() as u8 > index + 1 {
        Err(ArgumentError::TooManyArguments(index + 1, arguments.len() as u8))
    } else {
        let argument = &arguments[index as usize] ;
        match argument {
            Argument::Binary(argument) => Ok(argument),
            _ => Err(ArgumentError::IncorrectType(index, Argument::Binary(EMPTY_VEC.clone()), argument))
        }
    }
}
