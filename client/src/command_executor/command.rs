use crate::command_utils::params::{Argument, ReturnValue};
use crate::command_executor::execute_command::execute_command;
use std::time::Instant;
use crate::commands::opcodes::OPCODE_DECODE_ERROR;
use crate::command_executor::command_response::CommandResponse;

pub(crate) struct Command<'a> {
    opcode: u16,
    arguments: Vec<Argument<'a>>
}

impl Command<'_> {

    pub(crate) fn new(opcode: u16, arguments: Vec<Argument>) -> Command {
        Command { opcode, arguments }
    }

    pub(crate) fn execute(&self) -> CommandResponse {
        let start = Instant::now();
        CommandResponse::new(Instant::now().duration_since(start), match execute_command(self.opcode, &self.arguments) {
            Ok(return_values) => return_values,
            Err(argument_error) => argument_error.encode()
        })
    }

    pub(crate) fn decode_error<'a>(message: String) -> Command<'a> {
        Command::new(OPCODE_DECODE_ERROR, vec![Argument::String(message)])
    }
}
