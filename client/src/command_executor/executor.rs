use crate::command_executor::command_output_target::CommandOutputTarget;
use std::os::unix::net::UnixStream;
use std::thread;
use crate::utils::string_utils;
use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};
use crate::command_executor::{encoding, executor};
use crate::utils::err_handle::{ResultBacktrace, wrap_err, leaf_err_spec, not_implemented};
use std::process::exit;
use std::pin::Pin;
use crate::command_utils::params::{Argument, ReturnValue};
use crate::command_executor::command_response::CommandResponse;


/**
 * Bytes in CommandResponse, excluding those of the 'message' variable.
*/
pub(crate) const COMMAND_RESPONSE_HEADER_BYTES: usize = 12;

fn write_output(response: CommandResponse, output_target: CommandOutputTarget) {
     match output_target {
         CommandOutputTarget::Console=> {
             
         },
         CommandOutputTarget::Socket(out_pipe)  => {

         }
     }
}

fn gui_decode(cmd: String) -> String {
    return cmd;
}

fn execute_script_file(script_file_name: &str) -> ResultBacktrace<()> {
    println!("Executing script file {} ...", script_file_name);
    let path = Path::new(script_file_name);
    if !path.exists() {
        panic!("Fatal error: {} : No such file or directory.", script_file_name)
    }
    let script_file  = File::open(script_file_name);

    if script_file.is_err() {
        panic!("Fatal error: {}", script_file.unwrap_err());
    }

    let script_file = script_file.unwrap();
    let mut reader = BufReader::new(script_file);
    loop {
        let mut cmd = String::new();
        let result = reader.read_line(&mut cmd);
        if result.is_err() {
            return wrap_err("Script file execution failed:\n-->{}", leaf_err_spec(result.unwrap_err().to_string().as_str()));
        }
        if cmd.is_empty() {
            break;
        }
        //execute(CommandParams::from_plaintext(cmd));
    }
    Ok(())
}