use crate::user::account::Account;
use crate::user::name::Name;
use crate::user::email::EmailAddress;
use crate::user::schedule::Schedule;

struct User {
    account: Account,
    name: Name,
    email: EmailAddress,
    schedule: Schedule
}