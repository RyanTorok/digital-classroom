use crate::utils::err_handle::{ErrSpec, wrap_err, leaf_err, not_implemented, ResultBacktrace, wrap_result_mut_or_wrap_err};
use crate::identifiers::uuid::Uuid;
use std::sync::Mutex;
use crate::crypto::{Crypto, IV_BYTES, CryptoOperations, random_salt};
use rand::{Rng, RngCore};

const NOT_STORED: u8 = 0;
const PIN_HW_HASHED: u8 = 1;
const HW_HASHED: u8 = 2; //TODO probably insecure, should remove


pub(crate) struct Account {
    username: String,
    password_storage_type: u8,
    password: Option<Vec<u8>>,
}

impl Account {
    pub(crate) fn create_account(crypto: &Crypto, username: String, password: String) -> Result<Account, ErrSpec> {
        let salt = random_salt(crypto);
        if salt.is_err() {
            return wrap_err("Account Creation Failed", salt.unwrap_err());
        }
        let client_encrypted_pw = crypto.password_hash_stage_1(&password, salt.unwrap().as_ref());

        Ok(Account {
            username,
            password_storage_type: NOT_STORED,
            password: None,
        })
    }

    #[cfg(not(feature = "web"))]
    pub(crate) fn store_pin_protected_password(&mut self, crypto: &Crypto, password: String, pin: String) -> ResultBacktrace<()> {
        //TODO doesn't compile on WASM - need localStorage version
        let key = crypto.local_password_encryption_key(Option::Some(pin));
        if key.is_err() {
            return wrap_err("Password pin-storage failed.", key.unwrap_err());
        }
        let mut iv = [0u8; IV_BYTES];
        rand::thread_rng().fill_bytes(&mut iv);
        let res_encrypted_pw = crypto.symmetric_encrypt(&password.into_bytes(), &key.unwrap(), &iv);
        wrap_result_mut_or_wrap_err(res_encrypted_pw, |encrypted_pw| {
            let was_password_storage_type = self.password_storage_type;
            let was_password = self.password.clone();

            self.password_storage_type = PIN_HW_HASHED;

            self.password = Option::from((*encrypted_pw.clone()).to_owned());

            let result = self.serialize();
            if result.is_err() {
                //roll back changes
                self.password_storage_type = was_password_storage_type;
                self.password = was_password;
                return wrap_err("Account data serialization failed.", result.unwrap_err());
            }
            Ok(())
        }, "Could not symmetric encrypt password to store using local key.")

    }

    #[cfg(feature = "web")]
    pub(crate) fn store_pin_protected_password(&mut self, crypto: &Crypto, password: String, pin: String) -> ResultBacktrace<()> {
        unimplemented!()
    }

    fn serialize(&self) -> Result<(), ErrSpec> {
        not_implemented()
    }

    fn deserialize(username: String) -> Result<Account, ErrSpec> {
        not_implemented()
    }
}