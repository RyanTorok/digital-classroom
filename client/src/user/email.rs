use std::io::Split;
use std::slice::SplitN;
use crate::utils::err_handle::{ResultBacktrace, leaf_err, not_implemented};

/// The EmailAddress struct represents an actual email address.
pub(crate) struct EmailAddress {
    username: String,
    host: String
}

impl EmailAddress {

    pub(crate) fn from_str(email: &str) -> ResultBacktrace<EmailAddress> {
        //username@host
        let split: Vec<&str> = email.split("@").collect();
        if split.len() != 2 {
            return leaf_err(format!("Invalid email: '{}'", email).as_ref());
        } else {
            return Ok(EmailAddress { username: split[0].to_string(), host: split[1].to_string() })
        }
    }

    pub(crate) fn to_string(&self) -> String {
        let mut email = String::new();
        email.push_str(self.username.as_ref());
        email.push('@');
        email.push_str(self.host.as_ref());
        email
    }
}

/// The EmailMessage struct represents a single email message that can be sent using SMTP or IMAP protocols.
struct EmailMessage {
    to: Vec<EmailAddress>,
    cc: Vec<EmailAddress>,
    bcc: Vec<EmailAddress>,
    subject: String,
    body: String
}

impl EmailMessage {

    pub(crate) fn new() -> EmailMessage {
        EmailMessage {
            to: vec![],
            cc: vec![],
            bcc: vec![],
            subject: String::new(),
            body: String::new()
        }
    }

    pub(crate) fn send(&self) -> ResultBacktrace<()> {
        if self.to.is_empty() {
            return leaf_err(format!("Cannot send email with subject '{}' because no recipients were specified.", self.subject.as_str()).as_str());
        }
        not_implemented()
    }
}