use crate::utils::err_handle::{ErrSpec, leaf_err};
use crate::user::name::Suffix::Other;

pub(crate) struct Name {
    prefix: Option<Prefix>,
    first: String,
    middle: Option<String>,
    last: String,
    suffix: Option<Suffix>
}

impl Name {
    pub(crate) fn full_name(&self) -> String {
        let mut s = String::new();
        if self.prefix.is_some() {
            s.push_str(self.prefix.as_ref().unwrap().to_str());
            s.push(' ');
        }
        s.push_str(self.first.as_str());
        s.push(' ');
        if self.middle.is_some() {
            s.push_str(self.middle.as_ref().unwrap().as_str());
            s.push(' ');
        }
        s.push_str(self.last.as_str());
        if self.middle.is_some() {
            s.push(' ');
            s.push_str(self.suffix.as_ref().unwrap().to_str());
        }
        s
    }


    /// Attempts to parse a name struct based on a string input.
    ///
    /// Accepts any of the following formats:
    ///
    /// <first> <last>
    /// <first> <middle> <last>
    /// <prefix> <first> <last>
    /// <first> <last> <suffix>
    /// <prefix> <first> <middle> <last>
    /// <first> <middle> <last> <suffix>
    /// <prefix> <first> <last> <suffix>
    /// <prefix> <first> <middle> <last> <suffix>
    ///
    /// See name::Prefix::from_str() and name::Suffix::from_str() to see what strings are accepted as
    /// <prefix> and <suffix> respectively. Matching either of these to Other(_) does not constitute a match.
    ///
    /// Some notes about parsing behavior:
    ///
    /// After separating out the prefix and/or suffix, the number of remaining words determine the behavior.
    /// If less than 2 words remain, we don't have at least one of the first and last name, so an error results.
    /// Otherwise:
    /// 2 words remain -> <first> <last>
    /// 3 words remain -> <first> <middle> <last>
    /// 4 words remain -> <2-word first> <middle> <last>
    /// 5 words remain -> <2-word first> <middle> <2-word last>
    ///
    /// Note that we don't distinguish between middle names and middle initials. If a middle initial
    /// is provided, it will be stored as the middle name and the period won't be removed if it is included.

    pub(crate) fn parse(name: String) -> Result<Name, ErrSpec> {
        //TODO simplify by calling split.collect()?
        let split = name.split_ascii_whitespace();
        let mut vec: Vec<&str> = vec![];
        for word in split {
            vec.push(word);
        }
        let len = vec.len();
        if len < 2 || len > 5 {
            return leaf_err(format!("Could not parse name '{}', got <prefix> <suffix>, expected <prefix> <first> <last> <suffix>", name).as_str());
        }

        //Is there a prefix?
        let prefix_maybe = Prefix::from_str(vec[0]);
        let has_prefix: bool;
        match prefix_maybe {
            Prefix::Other(_) => has_prefix = false,
            _ => has_prefix = true
        }

        //Or a suffix?
        let suffix_maybe = Suffix::from_str(vec[len - 1]);
        let has_suffix: bool;
        match suffix_maybe {
            Suffix::Other(_) => has_suffix = false,
            _ => has_suffix = true
        }

        let prefix: Option<Prefix>;
        if has_prefix {
            prefix = Option::Some(prefix_maybe);
        } else {
            prefix = None;
        }

        let suffix: Option<Suffix>;
        if has_suffix {
            suffix = Option::Some(suffix_maybe);
        } else {
            suffix = None;
        }

        let body_start: usize;
        if has_prefix {
            body_start = 1;
        } else {
            body_start = 0;
        }
        let body_end: usize;
        if has_suffix {
            body_end = len - 1;
        } else {
            body_end = len;
        }
        let body_words = body_end - body_start;
        match body_words {
            0 => {
                if has_prefix && has_suffix {
                    leaf_err(format!("Could not parse name '{}', got <prefix> <suffix>, expected <prefix> <first> <last> <suffix>", name).as_str())
                } else if has_prefix {
                    leaf_err(format!("Could not parse name '{}', got <prefix> expected <prefix> <first> <last>", name).as_str())
                } else if has_suffix {
                    leaf_err(format!("Could not parse name '{}', got <suffix>, expected <first> <last> <suffix>", name).as_str())
                } else {
                    leaf_err(format!("Could not parse name '{}', got <shouldn't see this>, expected <first> <last>", name).as_str())
                }

            }
            1 => {
                if has_prefix && has_suffix {
                    leaf_err(format!("Could not parse name '{}', got <prefix> <name> <suffix>, expected <prefix> <first> <last> <suffix>", name).as_str())
                } else if has_prefix {
                    leaf_err(format!("Could not parse name '{}', got <prefix> <first>, expected <prefix> <first> <last>", name).as_str())
                } else if has_suffix {
                    leaf_err(format!("Could not parse name '{}', got <first> <suffix>, expected <first> <last> <suffix>", name).as_str())
                } else {
                    leaf_err(format!("Could not parse name '{}', got <first>, expected <first> <last>", name).as_str())
                }
            },
            2 => {
                //<first> <last>
                Ok(Name {
                    prefix,
                    first: vec[body_start].to_string(),
                    middle: None,
                    last: vec[body_start + 1].to_string(),
                    suffix
                })
            },
            3 => {
                //<first> <middle> <last>
                Ok(Name {
                    prefix,
                    first: vec[body_start].to_string(),
                    middle: Option::from(vec[body_start + 1].to_string()),
                    last: vec[body_start + 2].to_string(),
                    suffix
                })
            },
            4 => {
                //<compound first> <middle> <last>
                let mut compound_first = String::new();
                compound_first.push_str(vec[0]);
                compound_first.push(' ');
                compound_first.push_str(vec[1]);

                Ok(Name {
                    prefix,
                    first: compound_first,
                    middle: Option::from(vec[body_start + 2].to_string()),
                    last: vec[body_start + 3].to_string(),
                    suffix
                })
            },
            5 => {
                //<compound first> <middle> <compound last>
                let mut compound_first = String::new();
                compound_first.push_str(vec[0]);
                compound_first.push(' ');
                compound_first.push_str(vec[1]);

                let mut compound_last = String::new();
                compound_last.push_str(vec[3]);
                compound_last.push(' ');
                compound_last.push_str(vec[4]);

                Ok(Name {
                    prefix,
                    first: compound_first,
                    middle: Option::from(vec[body_start + 2].to_string()),
                    last: compound_last,
                    suffix
                })
            },
            _ => {leaf_err(format!("Cannot parse name '{}'. Expected 2-5 words.", name).as_str())}
        }
    }
}

enum Prefix {
    MR, MS, MRS, DR, MISS, MSTR, Other(String)
}

impl Prefix {
    //TODO other languages?
    fn from_str(s: &str) -> Prefix {
        match s.to_ascii_uppercase().as_str() {
            "MR." | "MR" => (Prefix::MR),
            "MS." | "MS" => (Prefix::MS),
            "MRS." | "MRS" => (Prefix::MRS),
            "DR." | "DR" => (Prefix::DR),
            "MISS" => (Prefix::MISS),
            "MSTR." | "MSTR" => (Prefix::MSTR),
            _ => {Prefix::Other(s.to_owned())}
        }
    }

    pub(crate) fn to_str(&self) -> &str {
        match self {
            Prefix::MR => {"Mr."},
            Prefix::MS => {"Ms."},
            Prefix::MRS => {"Mrs."},
            Prefix::DR => {"Dr."},
            Prefix::MISS => {"Miss"},
            Prefix::MSTR => {"Mstr."},
            Prefix::Other(prefix) => { prefix }
        }
    }
}

enum Suffix {
   JR, SR, Other(String)
}

impl Suffix {
    pub(crate) fn from_str(s: &str) -> Suffix {
        match s.to_ascii_uppercase().as_str() {
            "JR." | "JR" | "JUNIOR" => {Suffix::JR}
            "SR." | "SR" | "SENIOR" => {Suffix::SR}
            _=> {Other(s.to_owned())}
        }
    }

    pub(crate) fn to_str(&self) -> &str {
        match self {
            Suffix::JR => {"Jr."},
            Suffix::SR => {"Sr."},
            Other(suffix) => {suffix},
        }
    }
}