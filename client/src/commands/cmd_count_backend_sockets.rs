// FILE: commands/cmd_count_backend_sockets.rs
// 
// DESCRIPTION: Implementation of the command count_backend_sockets
// 
// CHANGE LOG:
// 23 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::OK;
use crate::gui_socket::gui_client::THREAD_POOL_SIZE;

/// Command: count_backend_sockets
/// 
/// Returns the number of parallel sockets currently open for communication with the client backend.
/// 
/// Arguments:
/// None
/// 
/// Returns:
/// (uint16) The number of open sockets.
/// 
/// Throws:
/// None

pub(crate) fn count_backend_sockets() -> (u16, u16) {
	let guard = THREAD_POOL_SIZE.read().unwrap();
	(OK, *guard)
}

// Tests for this command are found in cmd_start_backend_socket
