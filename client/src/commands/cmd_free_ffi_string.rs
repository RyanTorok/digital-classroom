// FILE: commands/cmd_free_ffi_string.rs
// 
// DESCRIPTION: Implementation of the command free_ffi_string
// 
// CHANGE LOG:
// 20 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::OK;
use std::ffi::CString;

/// Command: free_ffi_string
/// 
/// Returns a string resource allocated on the heap as a return value from a previous command to the API
/// to be deallocated from memory.
/// 
/// Arguments:
/// resource (pointer) The address of the string resource to be deallocated
/// 
/// Returns:
/// None
/// 
/// Throws:
/// ERR_NONUTF8 -- If any argument string is not valid UTF-8
/// ERR_NULLPTR -- If any pointer argument is null

pub(crate) unsafe fn free_ffi_string(resource: *mut u8) ->  u16 {
	CString::from_raw(resource as *mut libc::c_char);
	OK
}