// FILE: commands/cmd_test_noargs_noreturn_linked.rs
// 
// DESCRIPTION: Implementation of the command test_noargs_noreturn_linked
// 
// CHANGE LOG:
// 19 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::OK;

/// Command: test_noargs_noreturn_linked
/// 
/// Tests to make sure the linked command API can receive and execute commands with no arguments and no return
/// values other than the exit code.
/// 
/// Arguments:
/// None
/// 
/// Returns:
/// None
/// 
/// Throws:
/// None

pub(crate) fn test_noargs_noreturn_linked() ->  u16 {
	OK
}