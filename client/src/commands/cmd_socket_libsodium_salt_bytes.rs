// FILE: commands/cmd_socket_libsodium_salt_bytes.rs
// 
// DESCRIPTION: Implementation of the command socket_libsodium_salt_bytes
// 
// CHANGE LOG:
// 14 Jan 2021 -- Initial generation 

use crate::command_utils::err_codes::OK;
use crate::crypto::embedded::SALT_LENGTH;

/// Command: socket_libsodium_salt_bytes
/// 
/// Retrieves the length of the libsodium constant SODIUM_CRYPTO_PWHASH_SALTBYTES for the client backend
/// installation on the local machine
/// 
/// Arguments:
/// None
/// 
/// Returns:
/// (uint32) The libsodium password salt length, in bytes
/// 
/// Throws:
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available

pub(crate) fn socket_libsodium_salt_bytes() -> (u16, u32) {
	(OK, SALT_LENGTH)
}