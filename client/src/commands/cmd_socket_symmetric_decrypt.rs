// FILE: commands/cmd_socket_symmetric_decrypt.rs
// 
// DESCRIPTION: Implementation of the command socket_symmetric_decrypt
// 
// CHANGE LOG:
// 14 Jan 2021 -- Initial generation 

use crate::command_utils::err_codes::{OK, ERR_SECURITY, ERR_ARGLENGTH};
use crate::crypto::embedded::EmbeddedCrypto;
use crate::crypto::CryptoOperations;
use crate::utils::err_handle::ErrSpec;

/// Command: socket_symmetric_decrypt
/// 
/// WASM-to-desktop wrapper around crypto_aead_xchacha20poly1305_ietf_decrypt in libsodium
/// 
/// Arguments:
/// ciphertext (binary) The ciphertext to decrypt
/// key (binary) Encryption key
/// iv (binary) The IV that was used to encrypt the message
/// 
/// Returns:
/// (binary) The decrypted plaintext
/// 
/// Throws:
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available
/// ERR_ARGLENGTH -- If the key or iv are not the correct length

pub(crate) fn socket_symmetric_decrypt<'a>(ciphertext: &[u8], key: &[u8], iv: &'a [u8]) -> (u16, Vec<u8>) {
	if iv.len() != 192 {
		return (ERR_ARGLENGTH, vec![]);
	}
	// This is safe because we verify the length beforehand, and the unsafe code is only to circumvent
	// that the compiler can't verify we have an array of the correct length.
	let iv_fixed_size: &'a[u8; 192] = unsafe { std::mem::transmute::<*const u8, &'a[u8; 192]>(iv.as_ptr()) };
	match EmbeddedCrypto::new().symmetric_decrypt(ciphertext, key, iv_fixed_size) {
		Ok(plaintext) => (OK, plaintext),
		Err(e) => (ERR_SECURITY, vec![]),
	}
}