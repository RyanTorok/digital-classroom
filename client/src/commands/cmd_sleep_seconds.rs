// FILE: commands/cmd_sleep_seconds.rs
// 
// DESCRIPTION: Implementation of the command sleep_seconds
// 
// CHANGE LOG:
// 23 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::OK;

/// Command: sleep_seconds
/// 
/// Sleeps for the given number of seconds. Used for testing timing of concurrent command execution.
/// 
/// Arguments:
/// seconds (uint16) The number of seconds to sleep for
/// 
/// Returns:
/// None
/// 
/// Throws:
/// None

pub(crate) fn sleep_seconds(seconds: u16) ->  u16 {
	OK
}