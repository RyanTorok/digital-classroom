// FILE: commands/cmd_socket_symmetric_encrypt.rs
// 
// DESCRIPTION: Implementation of the command socket_symmetric_encrypt
// 
// CHANGE LOG:
// 14 Jan 2021 -- Initial generation 

use crate::command_utils::err_codes::OK;

/// Command: socket_symmetric_encrypt
/// 
/// WASM-to-desktop wrapper around crypto_aead_xchacha20poly1305_ietf_encrypt in libsodium
/// 
/// Arguments:
/// plaintext (binary) plaintext string to be encrypted
/// key (binary) encryption key
/// iv (binary) Initialization vector to use for encryption. This value does not need to be secret, but MUST be unique for each message encrypted.
/// 
/// Returns:
/// (binary) The encrypted ciphertext
/// 
/// Throws:
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available

pub(crate) fn socket_symmetric_encrypt(plaintext: &[u8], key: &[u8], iv: &[u8]) -> (u16, Vec<u8>) {
	(OK, vec![])
}