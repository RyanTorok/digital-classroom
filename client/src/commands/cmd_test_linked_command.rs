use crate::command_utils::err_codes::OK;

///
/// Command Name: Test linked command
///
/// Description: returns a message the indicates the message sent. For debug
/// and testing purposes.
///
/// Input(s):
/// The message to send
///
/// Outputs:
/// A message that indicates that the service received the message sent
///

pub(crate) fn test_linked_command(message: &str) -> (u16, String) {
    (OK, format!("Received message: {}", message))
}