use crate::command_utils::err_codes::ERR_DECODE;

#[inline]
pub(crate) fn decode_error(msg: &str) -> (u16, &str) {
    (ERR_DECODE, msg)
}