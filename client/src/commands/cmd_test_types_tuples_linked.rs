// FILE: commands/test_types_tuples_linked.rs
//
// DESCRIPTION: Implementation of the command test_types_tuples_linked
//
// CHANGE LOG:
// 12 Oct 2020 -- Initial generation

use crate::command_utils::err_codes::OK;

/// Command: test_types_tuples_linked
///
/// Tests to make sure the linked command API can receive commands of all specified types, and receive and
/// return multiple values.
///
/// Arguments:
/// int8 (int8) an 8-bit integer
/// int16 (int16) a 16-bit integer
/// int32 (int32) a 32-bit integer
/// int64 (int64) a 64-bit integer
/// int128 (int128) a 128-bit integer
/// uint8 (uint8) an 8-bit unsigned integer
/// uint16 (uint16) a 16-bit unsigned integer
/// uint32 (uint32) a 32-bit unsigned integer
/// uint64 (uint64) a 64-bit unsigned integer
/// uint128 (uint128) a 128-bit unsigned integer
/// uuid (uuid) a 128-bit universally unique identifier (uuid)
/// string (string) a string of text
///
/// Returns:
/// (int8) an 8-bit integer
/// (int16) a 16-bit integer
/// (int32) a 32-bit integer
/// (int64) a 64-bit integer
/// (int128) a 128-bit integer
/// (uint8) an 8-bit unsigned integer
/// (uint16) a 16-bit unsigned integer
/// (uint32) a 32-bit unsigned integer
/// (uint64) a 64-bit unsigned integer
/// (uint128) a 128-bit unsigned integer
/// (uuid) a 128-bit universally unique identifier (uuid)
/// (string) a string of text
///
/// Throws:
/// None

pub(crate) fn test_types_tuples_linked(int8: i8, int16: i16, int32: i32, int64: i64, int128: i128, uint8: u8, uint16: u16, uint32: u32, uint64: u64, uint128: u128, uuid: uuid::Uuid, string: &str) -> (u16, i8, i16, i32, i64, i128, u8, u16, u32, u64, u128, uuid::Uuid, String) {
    (OK, int8 + 1, int16 + 1, int32 + 1, int64 + 1, int128 + 1, uint8 + 1, uint16 + 1, uint32 + 1, uint64 + 1, uint128 + 1, uuid::Uuid::from_u128(uuid.as_u128() + 1), format!("Received string: {}", string))
}