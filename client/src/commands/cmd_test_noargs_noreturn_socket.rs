// FILE: commands/cmd_test_noargs_noreturn_socket.rs
// 
// DESCRIPTION: Implementation of the command test_noargs_noreturn_socket
// 
// CHANGE LOG:
// 19 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::OK;

/// Command: test_noargs_noreturn_socket
/// 
/// Tests to make sure the command socket can receive and execute commands with no arguments and no return
/// values other than the exit code.
/// 
/// Arguments:
/// None
/// 
/// Returns:
/// None
/// 
/// Throws:
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available

pub(crate) fn test_noargs_noreturn_socket() ->  u16 {
	OK
}