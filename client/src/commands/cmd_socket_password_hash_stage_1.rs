// FILE: commands/cmd_socket_password_hash_stage_1.rs
// 
// DESCRIPTION: Implementation of the command socket_password_hash_stage_1
// 
// CHANGE LOG:
// 10 Jan 2021 -- Initial generation 

use crate::command_utils::err_codes::{OK, ERR_SECURITY};
use crate::crypto::embedded::EmbeddedCrypto;
use crate::crypto::CryptoOperations;

/// Command: socket_password_hash_stage_1
/// 
/// Performs stage 1 of a password hash using libsodium on the client backend on the local machine.
/// 
/// Arguments:
/// plaintext (string) The password to hash
/// salt (binary) The password salt
/// 
/// Returns:
/// (binary) The hashed password (ciphertext)
/// 
/// Throws:
/// ERR_NONUTF8 -- If any argument string is not valid UTF-8
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available
/// ERR_NULLPTR -- If any pointer argument is null
/// ERR_SECURITY -- If the password hashing algorithm produces a non-success return value.

pub(crate) fn socket_password_hash_stage_1(plaintext: &str, salt: &[u8]) -> (u16, Vec<u8>) {
    let res_hash = EmbeddedCrypto{}.password_hash_stage_1(plaintext, salt);
    match res_hash {
        Ok(hash) => (OK, (*hash.clone()).to_owned()),
        Err(e) => (ERR_SECURITY, vec![])
    }
}