// FILE: commands/cmd_start_backend_socket.rs
// 
// DESCRIPTION: Implementation of the command start_backend_socket
// 
// CHANGE LOG:
// 22 Oct 2020 -- Initial generation 

use crate::command_utils::err_codes::{OK, ERR_NOBACKEND};
use crate::gui_socket::gui_client::{connect, THREAD_POOL_SIZE, THREAD_KILL_ACKS, dequeue_command};
use crate::ipc::{IPCConnection, ConnectionMode};
use std::iter::{repeat, repeat_with};
use std::ops::DerefMut;
use std::sync::{Mutex, Condvar, Arc, RwLockWriteGuard};
use std::borrow::BorrowMut;
use crate::api::api::test_socket_command;
use crate::api::tuple::CTuple2;
use std::thread;
use std::net::Shutdown;
use crate::utils::socket_utils::ShutdownSocket;

lazy_static! {
	static ref mtx: Mutex<()> = Mutex::<()>::new(());
}

/// Command: start_backend_socket
/// 
/// Initializes the IPC socket to execute commands on the dcl backend, with the specified number of concurrent
/// sockets. For optimal performance, we recommend this value be set as the number of logical cores on your
/// machine.
///
/// Arguments:
/// sockets (uint16) The number of concurrent connections to open.
/// 
/// Returns:
/// None
/// 
/// Throws:
/// ERR_NOBACKEND -- If a connection to the backend failed for one or more of the sockets. If this failure
/// occurs, the socket count will return to the value it was before the command was invoked, but the added
/// threads that did succeed reserve the right to execute at most one command off the queue before they are
/// terminated.

pub(crate) fn start_backend_socket(sockets: u16) ->  u16 {
	// Since we release the write lock on THREAD_POOL_SIZE later to allow the worker threads to
	// have a chance to acknowledge a size change, we have to make sure we can't have two callers
	// concurrently, otherwise the pool size might be made larger before all the threads acknowledge,
	// and they might not terminate, which could leave us with a larger thread pool than the caller
	// requested.
	let _fn_guard = mtx.lock(); //scope last entire function
	let old_pool_size;
	{
		let mut guard = THREAD_POOL_SIZE.write().expect("Fatal error: The lock on the IPC connection count \
		was double-acquired within a single thread.");
		let pool_size_ref = guard.deref_mut();
		old_pool_size = *pool_size_ref;
		//println!("write lock successfully acquired.");
		if sockets > old_pool_size {
			// We add more threads
			let results = vec![Arc::new((Mutex::new(0), Condvar::new())); (sockets - old_pool_size) as usize];
			let mut kill_acks = THREAD_KILL_ACKS.write().unwrap();
            for _i in old_pool_size..sockets {
				(*kill_acks).push(Arc::new((Mutex::new(false), Condvar::new())));
			}
			drop(kill_acks);
			let mut kill_acks = THREAD_KILL_ACKS.read().unwrap();
			for i in old_pool_size..sockets {
				// The indices are different because `results` is a local variable and only stores
				// entries for the new threads, whereas kill_acks is the size of the number of
				// currently active threads, plus the new ones.
				let clone = results[(i - old_pool_size) as usize].clone();
				let arc = kill_acks[i as usize].clone();
				thread::spawn(move || {
					//println!("Starting thread {}", i);
					let (kill_indicator, kill_notifier) = &*arc;
					let lock = || {
						let (lock, cvar) = &*clone;
						(lock.lock().expect("Fatal error: The lock on the success condition \
						of establishing a new socket was double-acquired within a single thread."), cvar)
					};
					match connect() {
						Ok(conn) => {
							let mut conn: IPCConnection = conn;
							let (mut result, cvar) = lock();
							*result = 1;
							// release the lock so the thread dispatcher can continue on its merry way
							cvar.notify_one();
							drop(result);
							loop {
								// If we didn't time out, then run the command. The timeout prevents
								// sockets that need to be closed from hanging because they didn't get
								// another command to run while they were waiting.
								//println!("About to read a command on {}", i);
								if let Ok(fn_ptr_send_command_logic) = dequeue_command() {
									// This is safe because:

									// fn_ptr_send_command_logic is a pointer to the Box containing
									// the closure to run. It is guaranteed to be a valid reference
									// because the owner of the closure is `send_socket_command`,
									// which blocks until the command returns, let alone until the
									// arguments are dequeued here and written to the socket.
									// It isn't possible for the compiler to verify this because the
									// Queue itself has a static lifetime, even though the closures
									// it stores deal with data that isn't static. We keep that safe
									// by making sure the owner (`send_socket_command`) of the
									// less-than-static data (the command arguments) cannot return
									// until the closure has executed.
									let fn_ptr_send_command_logic = fn_ptr_send_command_logic as
										*const Box<dyn Fn(&mut IPCConnection) + Send + Sync>;
									unsafe {
										let send_command_logic = &*fn_ptr_send_command_logic;
										(*send_command_logic)(&mut conn);
									}
									//println!("Got a command");
								} else {
								}
								let read_guard = THREAD_POOL_SIZE.read().unwrap();
								let thread_pool_size: &u16 = &*read_guard;
								//println!("i = {}, pool_size = {}", i, *thread_pool_size);
								if i >= *thread_pool_size {
									// the socket count was reduced. We need to kill our socket and end our thread.
									conn.shutdown_socket(Shutdown::Both);
									break;
								}
							}
							// Notify the thread dispatcher that the thread has ended so if a future
							// call to increase the thread count is made, we won't end up with too many
							// threads, because the dispatcher won't have a chance to increase the
							// pool size until all threads acknowledge the count decreased earlier,
							// which is required for start_backend_socket() to return.
							let mut guard = kill_indicator.lock().unwrap();
							*guard = true;
							//println!("acknowledge kill for socket {}", i);
							kill_notifier.notify_all();
						},
						Err(e) => {
							let (mut result, cvar) = lock();
							*result = 2;
							cvar.notify_all();
						}
					}
				});
			}
			let mut success = true;
			for i in old_pool_size..sockets {
				let (lock, cvar) = &*results[(i - old_pool_size) as usize];
				let mut result = lock.lock().unwrap();
				while *result == 0 {
					result = cvar.wait(result).unwrap();
				}
				if *result == 2 {
					success = false;
				}
			}
			if success {
				*pool_size_ref = sockets;
			} else {
				// Uh oh, one or more sockets failed. We should clean up by setting the thread count
				// to what it was. The new threads that did succeed are allowed to execute a command
				// before they realize they should be terminated.
				return ERR_NOBACKEND;
			}
		} else if sockets < old_pool_size {
			// We have to kill some threads/sockets, but above we programmed the threads to automatically
			// terminate if the pool size is decreased, so we can just set the value lower and release the
			// write lock.
			//println!("setting sockets down to {}", sockets);
			*pool_size_ref = sockets;
		}
	} // this scope ending closes the pool size RwLock for writing.
	//println!("write lock released");
	if sockets < old_pool_size {
		// Wait until all the threads we're terminating notify us they've acknowledged the termination
		// If we didn't do this, this command could be called again to increase the pool size, and the
		// threads we intended to terminate here might never notice the size shrank and continue
		// running, and we'll end up with more connections than the caller specified.
		let kill_acks = THREAD_KILL_ACKS.read().unwrap();
		for i in sockets..old_pool_size {
			let arc: &Arc<(Mutex<bool>, Condvar)> = &kill_acks[i as usize];
			let (lock, cvar) = &**arc;
			let mut lock_guard = lock.lock().unwrap();
			while !*lock_guard {
				lock_guard = cvar.wait(lock_guard).unwrap();
			}
		}
		drop(kill_acks);

		// Shrink kill_acks array
		let mut kill_acks = THREAD_KILL_ACKS.write().unwrap();
		// remove backwards to prevent useless copying
		for i in (sockets..old_pool_size).rev() {
			(*kill_acks).remove(i as usize);
		}
		//println!("Kill acknowledge complete.");
	}
	OK
}

#[cfg(test)]
mod tests {
	use crate::api::api::test_socket_command;
	use crate::commands::cmd_start_backend_socket::start_backend_socket;
	use crate::commands::cmd_count_backend_sockets::count_backend_sockets;
	use std::convert::TryFrom;
	use crate::api::tuple::CTuple2;
	use crate::command_utils::err_codes::{OK, ERR_NOBACKEND};
	use crate::gui_socket::gui_client::{THREAD_POOL_SIZE, THREAD_KILL_ACKS};
	use std::ffi::CStr;
	use std::thread;
	use std::sync::{Arc, Mutex, Condvar};

	// helper function
	unsafe fn deref(pointer: *const CTuple2<u16, *const libc::c_char>) -> (u16, *const libc::c_char) {
		((*pointer).a, (*pointer).b)
	}

	/// Basic test to verify that the command correctly causes a socket to open for commanding.
	#[test]
	fn start_backend_socket_once() {
		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		start_backend_socket(1);
		assert_eq!((OK, 1), count_backend_sockets());
		// This is the API's test_socket_command, not the direct command invocation, since we need to
		// test to make sure the socket is established (the direct command isn't linked with libdcl
		// anyway).
		let message = "Test message";
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(OK, response.0);
		unsafe { assert_eq!(format!("Received message: {0}", message), CStr::from_ptr(response.1).to_str().unwrap()); }
	}

	/// Repeat of start_backend_socket_once(), but with multiple sockets established
	#[test]
	fn start_backend_socket_multi() {
		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		start_backend_socket(8);
		assert_eq!((OK, 8), count_backend_sockets());
		let message = "Test message";
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(OK, response.0);
		unsafe { assert_eq!(format!("Received message: {0}", message), CStr::from_ptr(response.1).to_str().unwrap()); }
	}

	/// Verifies that passing 0 does not open a socket for commanding
	#[test]
	fn start_backend_socket_none() {
		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		let message = "Test message";
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(ERR_NOBACKEND, response.0);

		start_backend_socket(4);
		start_backend_socket(0);
		let response = test_socket_command(message.as_ptr() as *const i8);
	}

	/// Verifies that passing the same value twice does not change the number of open sockets
	#[test]
	fn start_backend_socket_repeat() {
		let message = "Test message";

		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());

		start_backend_socket(4);
		assert_eq!((OK, 4), count_backend_sockets());

		start_backend_socket(4);
		assert_eq!((OK, 4), count_backend_sockets());
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(OK, response.0);

		start_backend_socket(4);
		assert_eq!((OK, 4), count_backend_sockets());
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(OK, response.0);

		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(ERR_NOBACKEND, response.0);

		start_backend_socket(0);
		assert_eq!((OK, 0), count_backend_sockets());
		let response = test_socket_command(message.as_ptr() as *const i8);
		let response = unsafe { deref(response) };
		assert_eq!(ERR_NOBACKEND, response.0);
	}

	/*#[test]
	fn start_backend_socket_sequential_add_remove() {
		for i in 0..8 {
			start_backend_socket(i as u16);
			assert_eq!((OK, i as u16), count_backend_sockets());
		}
		for i in (0..8).rev() {
			start_backend_socket(i as u16);
			assert_eq!((OK, i as u16), count_backend_sockets());
		}
	}*/ //commented out because this test is really slow and doesn't add much coverage.

	#[test]
	fn start_backend_socket_stress_test() {
		let iterations = 32;
		for i in 0..iterations {
			start_backend_socket(i);
			assert_eq!((OK, i as u16), count_backend_sockets());
			start_backend_socket(0);
			assert_eq!((OK, 0 as u16), count_backend_sockets());
		}
	}

	#[test]
	fn start_backend_socket_concurrent() {
		// Test to make sure the array sizes don't get messed up by running multiple threads
		// i.e. make sure the function lock works.
		let iterations: u16 = 32;
		let count = Arc::new((Mutex::new(0u16), Condvar::new()));
		for i in 0..iterations {
			let count = count.clone();
			thread::spawn(move || {
				start_backend_socket(i);
				let mut guard = count.0.lock().unwrap();
				*guard += 1;
				count.1.notify_all();
			});
		}
		// Wait for the threads to finish
		let mut guard = count.0.lock().unwrap();
		while *guard < iterations {
			guard = count.1.wait(guard).unwrap();
		}
		// How many sockets we have depends on what order the threads ran in. However, the array sizes
		// should match the number of sockets we get.
		let (result, sockets) = count_backend_sockets();
		assert_eq!(OK, result);
		let pool_size = THREAD_POOL_SIZE.read().unwrap();

		//Test to make sure count_backend_sockets() returns the correct value.
		assert_eq!(*pool_size, sockets);

		let kill_acks = THREAD_KILL_ACKS.read().unwrap();
		assert_eq!(sockets as usize, (*kill_acks).len())
	}
}
