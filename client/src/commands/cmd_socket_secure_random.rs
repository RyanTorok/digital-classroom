// FILE: commands/cmd_socket_secure_random.rs
// 
// DESCRIPTION: Implementation of the command socket_secure_random
// 
// CHANGE LOG:
// 24 Jan 2021 -- Initial generation 

use crate::command_utils::err_codes::OK;

/// Command: socket_secure_random
/// 
/// WASM wrapper around randombytes_buf in libsodium. Returns an array of `n` secure random bytes.
/// 
/// Arguments:
/// n (uint32) The number of random bytes to return
/// 
/// Returns:
/// (binary) An array of `n` random bytes
/// 
/// Throws:
/// ERR_NOBACKEND -- If this command is invoked in socket mode and the backend is not available

pub(crate) fn socket_secure_random(n: u32) -> (u16, Vec<u8>) {
	(OK, vec![])
}