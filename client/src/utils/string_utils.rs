pub(crate) fn socket_unescape(src: &str) -> String {
    return src.replace("\\n", "\n").replace("\\\\", "\\");
}

pub(crate) fn socket_escape(src: &str) -> String {
    return src.replace("\n", "\\n").replace("\\", "\\\\");
}

pub(crate) fn push_byte_vec<T>(to_fill: &mut Vec<T>, src: Vec<T>) {
    for t in src {
        to_fill.push(t);
    }
}

