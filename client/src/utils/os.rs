pub(crate) enum OS {
    WIN32,
    WIN64,
    MAC,
    LINUX,
    FREEBSD
}

impl OS {
    pub(crate) fn to_str(&self) -> &str {
        return match self {
            OS::WIN32 => {"Microsoft Windows 32-Bit"},
            OS::WIN64 => {"Microsoft Windows 64-Bit"},
            OS::MAC => {"Mac OS X"},
            OS::LINUX => {"Linux"},
            OS::FREEBSD => {"FreeBSD"},
        }
    }
}

pub(crate) const OS: OS = OS::LINUX;
