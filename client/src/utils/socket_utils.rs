// FILE: utils/socket_utils.rs
// 
// DESCRIPTION: Contains useful traits and functions related to socket communication
// 
// CHANGE LOG:
// 27 Jan 2021 -- Initial generation; moved ShutdownSocket trait over here from ipc to maintain
// compatibility with mobile targets.

use std::net::Shutdown;

pub(crate) trait ShutdownSocket {
    fn shutdown_socket(&mut self, which: Shutdown);
}

