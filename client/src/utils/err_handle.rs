use std::error::Error;
use std::fmt::{Formatter, Debug};

const INDENT: u32 = 4;
const HEADER: &str = "--> ";

/// ErrSpec allows the backtrace of errors to propagate to the command origin so the user can trace the source of a command failure.

pub(crate) struct ErrSpec {
    message: String,
    caused_by: Box<Option<ErrSpec>>
}

impl ErrSpec {

    fn child_string(&self, tabs: u32) -> String {
        let mut s= String::new();
        for i in 0..(tabs*INDENT) {
            s.push(' ');
        }
        if tabs != 0 {
            s.push_str(HEADER);
        } else {
            //get new line after panic statement
            s.push('\n');
        }
        s.push_str(self.message.as_ref());
        s.push('\n');
        if self.caused_by.is_some() {
            let cb = self.caused_by.as_ref().as_ref(); //Rust is weird.
            s.push_str(cb.unwrap().child_string(tabs + 1).as_ref());
        }
        s
    }
}

impl std::error::Error for ErrSpec {}

impl std::fmt::Display for ErrSpec {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl std::fmt::Debug for ErrSpec {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        Ok(()) //TODO
    }
}

/*impl Into<Box<dyn Error + Send + Sync>> for ErrSpec {
    fn into(self) -> Box<dyn Error + Send + Sync> {
         Box::new(self)
    }
}*/

pub(crate) type ResultBacktrace<O> = Result<O, ErrSpec>;

#[inline]
pub(crate) fn wrap_err<O>(parent: &str, caused_by: ErrSpec) -> ResultBacktrace<O> {
    Err(wrap_err_spec(parent, caused_by))
}

#[inline]
pub(crate) fn wrap_err_spec(parent: &str, caused_by: ErrSpec) -> ErrSpec {
    ErrSpec { message: parent.to_owned(), caused_by: Box::new(Option::from(caused_by)) }
}

pub(crate) fn wrap_std_err<O, E: std::error::Error>(parent: &str, caused_by: E) -> ResultBacktrace<O> {
    Err(wrap_err_spec(parent, leaf_err_spec(caused_by.to_string())))
}

pub(crate) fn wrap_std_err_spec<E: std::error::Error>(parent: &str, caused_by: E) -> ErrSpec {
    wrap_err_spec(parent, leaf_err_spec(caused_by.to_string()))
}


#[inline]
pub(crate) fn leaf_err<O>(msg: &str) -> ResultBacktrace<O> {
    Err(leaf_err_spec(msg))
}


#[inline]
pub(crate) fn leaf_err_spec<O : ToString>(result: O) -> ErrSpec {
    ErrSpec {message: result.to_string(), caused_by: Box::new(None)}
}

/**
 * Returns an ErrSpec which reflects the given error message, with the system error reflected by 
 * ERRNO as its cause.
 */
#[inline]
pub(crate) fn wrap_sys_error_spec(msg: &str) -> ErrSpec {
    wrap_std_err_spec(msg, std::io::Error::last_os_error())
}

/**
 * Returns a ResultBacktrace which is an Err equal to the output of wrap_sys_err_spec(msg)
 */
#[inline]
pub(crate) fn wrap_sys_error<O>(msg: &str) -> ResultBacktrace<O> {
    Err(wrap_sys_error_spec(msg))
}



pub(crate) fn unwrap_or_wrap_err<O>(result: Result<O, ErrSpec>, message_on_error: &str) -> ResultBacktrace<O> {
    if result.is_err() {
        return wrap_err(message_on_error, result.err().unwrap());
    }
    //if the result is Ok, we can reuse the same Result object.
    return result;
}

pub(crate) fn unwrap_or_wrap_std_err<O, E: std::error::Error>(result: Result<O, E>, message_on_error: &str) -> ResultBacktrace<O> {
    match result {
        Ok(o) => {
            Ok(o)
        },
        Err(err) => {
            return wrap_err(message_on_error, leaf_err_spec(err.to_string()));
        }
    }
}

pub(crate) fn unwrap_or_leaf_err<O, E : Debug>(result: Result<O, E>, message_on_error: &str) -> ResultBacktrace<O> {
    if result.is_err() {
        return Err(leaf_err_spec(message_on_error));
    }
    return Ok(result.unwrap());
}

pub(crate) fn unwrap_or_wrap_err_msg<O: Debug, E: ToString + Debug>(result: Result<O, E>, message_on_error: &str) -> ResultBacktrace<O> {
    if result.is_err() {
        return wrap_err(message_on_error, leaf_err_spec(result.unwrap_err()));
    }
    return Ok(result.unwrap());
}

pub(crate) fn wrap_or_wrap_err<S, R, F>(result: Result<S, ErrSpec>, func: F, message_on_error: &str) -> ResultBacktrace<R> where
F: Fn(S) -> R
{
    match result {
        Ok(s) => {
            Ok(func(s))
        },
        Err(err) => {
            wrap_err(message_on_error, err)
        }
    }
}

pub(crate) fn wrap_result_or_wrap_err<S, R, F>(result: Result<S, ErrSpec>, func: F, message_on_error: &str) -> ResultBacktrace<R> where
F: Fn(S) -> ResultBacktrace<R>
{
    match result {
        Ok(s) => {
            func(s)
        },
        Err(err) => {
            wrap_err(message_on_error, err)
        }
    }
}

pub(crate) fn wrap_result_mut_or_wrap_err<S, R, F>(result: Result<S, ErrSpec>, mut func: F, message_on_error: &str) -> ResultBacktrace<R> where
    F: FnMut(S) -> ResultBacktrace<R>
{
    match result {
        Ok(s) => {
            func(s)
        },
        Err(err) => {
            wrap_err(message_on_error, err)
        }
    }
}

pub(crate) fn wrap_or_wrap_leaf_err<S, E, R, F>(result: Result<S, E>, func: F, message_on_error: &str) -> ResultBacktrace<R> where
F: Fn(S) -> R,
E: std::error::Error
{
    match result {
        Ok(s) => {
            Ok(func(s))
        },
        Err(err) => {
            leaf_err(&format!("{}\n{}{}", message_on_error, HEADER, &err.to_string()))
        }
    }
}

pub(crate) fn wrap_result_or_wrap_leaf_err<S, E, R, F>(result: Result<S, E>, func: F, message_on_error: &str) -> ResultBacktrace<R> where
F: Fn(S) -> ResultBacktrace<R>,
E: std::error::Error
{
    match result {
        Ok(s) => {
            func(s)
        },
        Err(err) => {
            leaf_err(&format!("{}\n{}{}", message_on_error, HEADER, &err.to_string()))
        }
    }
}

pub(crate) fn mut_wrap_result_or_wrap_leaf_err<S, E, R, F>(result: Result<S, E>, mut func: F, message_on_error: &str) -> ResultBacktrace<R> where
F: FnMut(S) -> ResultBacktrace<R>,
E: std::error::Error
{
    match result {
        Ok(s) => {
            func(s)
        },
        Err(err) => {
            leaf_err(&format!("{}\n{}{}", message_on_error, HEADER, &err.to_string()))
        }
    }
}

/**
 * If the destination is ok, execute the function src and assign its success status to the 
 * destination. 
 * If the destination is err, do nothing.
 * 
 * This function is intended for performing several successive actions tentatively as long
 * as all the previous actions succeeded, and retrieving one overall Result at the end.
 * 
 * If the actions depend on each other, use the wrap_... family instead, so a child event 
 * can be added to the backtrace as the cause for failure of its parent.
 * 
 * If the success type of the result produced by src is not (), it will be discarded.
 * 
 * use merge() if the result type produced by src is ErrSpec, otherwise use merge_std().
 * 
 * If the src function needs to mutate its captured state, use merge_mut() or merge_std_mut(),
 * respectively.
 */
pub(crate) fn merge<O, F>(destination: ResultBacktrace<()>, src: F, message_on_error: &str) -> ResultBacktrace<()>
where F : Fn() -> ResultBacktrace<O>,
{
    match destination {
        Ok(()) => {
            match src() {
                Ok(o) => {
                    Ok(())
                },
                 Err(err) => {
                    wrap_err(message_on_error, err)
                }
            }
        },
        Err(err_spec) => {
            Err(err_spec)
        }
    }
}

pub(crate) fn merge_std<O, E, F>(destination: ResultBacktrace<()>, src: F, message_on_error: &str) -> ResultBacktrace<()>
where F : Fn() -> Result<O, E>,
E: Error 
{
    match destination {
        Ok(()) => {
            match src() {
                Ok(o) => {
                    Ok(())
                },
                 Err(err) => {
                    wrap_std_err(message_on_error, err)
                }
            }
        },
        Err(err_spec) => {
            Err(err_spec)
        }
    }
}

pub(crate) fn merge_mut<O, F>(destination: ResultBacktrace<()>, mut src: F, message_on_error: &str) -> ResultBacktrace<()>
where F : FnMut() -> ResultBacktrace<O>,
{
    match destination {
        Ok(()) => {
            match src() {
                Ok(o) => {
                    Ok(())
                },
                 Err(err) => {
                    wrap_err(message_on_error, err)
                }
            }
        },
        Err(err_spec) => {
            Err(err_spec)
        }
    }
}

pub(crate) fn merge_std_mut<O, E, F>(destination: ResultBacktrace<()>, mut src: F, message_on_error: &str) -> ResultBacktrace<()>
where F : FnMut() -> Result<O, E>,
E: Error 
{
    match destination {
        Ok(()) => {
            match src() {
                Ok(o) => {
                    Ok(())
                },
                 Err(err) => {
                    wrap_std_err(message_on_error, err)
                }
            }
        },
        Err(err_spec) => {
            Err(err_spec)
        }
    }
}



//this function is for debugging only (obviously)
pub(crate) fn not_implemented<O>() -> ResultBacktrace<O> {
    return leaf_err("Not implemented yet.");
}