
// Mobile doesn't need this since it's only used for the socket command dispatcher, and the mobile
// version lacks the ability to execute commands in socket mode entirely.
#[cfg(not(feature = "mobile"))]
pub(crate) mod blocking_queue;
