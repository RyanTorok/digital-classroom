use crossbeam::queue::{ArrayQueue, SegQueue};
use std::sync::{Condvar, Mutex, Arc, WaitTimeoutResult, MutexGuard};
use std::ops::Deref;
use std::time::{Duration, Instant};

/// An mpmc queue that blocks on removing on an empty queue until another thread adds a value
pub(crate) trait BlockingQueue<T> {

    // These functions can take an immutable self-reference, because the underlying ArrayQueue
    // deals with the synchronization.

    /// Removes an element from the queue. Blocks until an element is available.
    fn poll(&self) -> T;

    /// Adds an element to the queue. Blocks until the queue is not full.
    fn push(&self, t: T);

    /// Removes an element from the queue. Blocks until an element is available.
    fn poll_timeout(&self, timeout: Duration) -> Result<T, Timeout>;

    /// Returns true iff the queue is empty
    fn is_empty(&self) -> bool;

    /// Returns the number of elements in the queue
    fn len(&self) -> usize;
}

pub(crate) struct ArrayBlockingQueue<T> {
    queue: ArrayQueue<T>,
    empty_cv: Arc<(Mutex<()>, Condvar)>,
    full_cv: Arc<(Mutex<()>, Condvar)>,
    poll_lock: Mutex<()>,
    push_lock: Mutex<()>,
}

impl <T> ArrayBlockingQueue<T> {
    pub fn new(size: usize) -> ArrayBlockingQueue<T> {
        ArrayBlockingQueue { queue: ArrayQueue::new(size),
            empty_cv: Arc::new((Mutex::new(()), Condvar::new())),
            full_cv: Arc::new((Mutex::new(()), Condvar::new())),
            poll_lock: Mutex::new(()),
            push_lock: Mutex::new(())
        }
    }

    /// Returns true if the queue is full, false otherwise.
    pub fn is_full(&self) -> bool {
        self.queue.is_full()
    }

    fn push_timeout(&self, t: T, timeout: Duration) -> Result<(), Timeout> {
        match self.push_inner(t, Some(timeout)) {
            Ok(_) => Ok(()),
            Err(_) => Err(Timeout::default()),
        }
    }

    fn push_inner(&self, t: T, opt_timeout: Option<Duration>) -> Result<(), Timeout> {
        let multi_push_guard = self.push_lock.lock().unwrap();
        while self.queue.is_full() {
            let (mtx, cvar) = &*self.full_cv;
            let mut guard = mtx.lock().unwrap();
            match opt_timeout {
                Some(timeout) => {
                    let g = cvar.wait_timeout(guard, timeout).unwrap();
                    if g.1.timed_out() {
                        return Err(Timeout::default())
                    }
                    guard = g.0;
                },
                None => {
                    guard = cvar.wait(guard).unwrap();
                }
            }
        }
        self.queue.push(t);
        drop(multi_push_guard);
        // Notify a blocked poller that the queue is no longer empty
        let arc = self.empty_cv.clone();
        let (mtx, cvar) = &*arc;
        // assignment makes this live long enough
        let _lock = mtx.lock().unwrap();
        // Tell one thread they can add their object to the queue.
        cvar.notify_one();
        Ok(())
    }

    fn poll_inner(&self, opt_timeout: Option<Duration>) -> Result<T, Timeout> {
        let _multi_poll_guard = self.poll_lock.lock().unwrap();
        // Block until the queue is not empty
        while self.queue.is_empty() {
            let (mtx, cvar) = &*self.empty_cv;
            let mut guard = mtx.lock().unwrap();
            match opt_timeout {
                Some(timeout) => {
                    let g = cvar.wait_timeout(guard, timeout).unwrap();
                    if g.1.timed_out() {
                        return Err(Timeout::default())
                    }
                    guard = g.0;
                },
                None => {
                    guard = cvar.wait(guard).unwrap();
                }
            }
        }
        // Now we know the queue isn't empty so we don't have to check.
        let removed = self.queue.pop().unwrap();
        // Notify a blocked pusher that the queue is no longer full
        let arc = self.full_cv.clone();
        let (mtx, cvar) = &*arc;
        // assignment makes this live long enough
        let lock = mtx.lock().unwrap();
        // Tell one thread they can add their object to the queue.
        cvar.notify_one();
        Ok(removed)
    }
}

impl <T> BlockingQueue<T> for ArrayBlockingQueue<T> {

    fn poll(&self) -> T {
        self.poll_inner(None).unwrap()
    }

    fn push(&self, t: T) {
        self.push_inner(t, None).unwrap();
    }

    fn poll_timeout(&self, timeout: Duration) -> Result<T, Timeout> {
        match self.poll_inner(Some(timeout)) {
            Ok(val) => { Ok(val) },
            Err(_) => { Err(Timeout::default()) },
        }
    }

    fn is_empty(&self) -> bool {
        self.queue.is_empty()
    }

    fn len(&self) -> usize { self.queue.len() }

}

pub(crate) struct LinkedBlockingQueue<T> {
    queue: SegQueue<T>,
    empty_cv: Arc<(Mutex<()>, Condvar)>,
    poll_lock: Mutex<()>
}

impl <T> LinkedBlockingQueue<T> {
    pub fn new() -> LinkedBlockingQueue<T> {
        LinkedBlockingQueue {
            queue: SegQueue::new(),
            empty_cv: Arc::new((Mutex::new(()), Condvar::new())),
            poll_lock: Mutex::new(())
        }
    }

    fn poll_inner(&self, opt_timeout: Option<Duration>) -> Result<T, Timeout> {
        // Block until the queue is not empty
        let _multi_poll_guard = self.poll_lock.lock().unwrap();
        let (mtx, cvar) = &*self.empty_cv;
        let mut guard = mtx.lock().unwrap();
        while self.queue.is_empty() {
            match opt_timeout {
                Some(timeout) => {
                    let g = cvar.wait_timeout(guard, timeout).unwrap();
                    if g.1.timed_out() {
                        return Err(Timeout::default())
                    }
                    guard = g.0;
                },
                None => {
                    guard = cvar.wait(guard).unwrap();
                }
            }
        }
        // Now we know the queue isn't empty so we don't have to check.
        Ok(self.queue.pop().unwrap())
    }
}

impl <T> BlockingQueue<T> for LinkedBlockingQueue<T> {
    fn poll(&self) -> T {
        self.poll_inner(None).unwrap()
    }

    fn push(&self, t: T) {
        self.queue.push(t);
        // Notify a blocked poller that the queue is no longer empty
        let arc = self.empty_cv.clone();
        let (mtx, cvar) = &*arc;
        // assignment makes this live long enough
        let _lock = mtx.lock().unwrap();
        // Tell one thread they can add their object to the queue.
        cvar.notify_one();
    }

    fn poll_timeout(&self, timeout: Duration) -> Result<T, ()> {
        match self.poll_inner(Some(timeout)) {
            Ok(val) => { Ok(val) },
            Err(_) => { Err(Timeout::default()) },
        }
    }

    fn is_empty(&self) -> bool {
        self.queue.is_empty()
    }

    fn len(&self) -> usize { self.queue.len() }
}

pub(crate) type Timeout = ();