pub(crate) fn u16_to_bytes_le(num: u16)-> [u8; 2] {
    let mut bytes = [0u8; 2];
    bytes[0] = (num & 0xFF) as u8;
    bytes[1] = ((num >> 8) & 0xFF) as u8;
    bytes
}

pub(crate) fn u16_to_bytes_be(num: u16)-> [u8; 2] {
    let mut bytes = [0u8; 2];
    bytes[0] = ((num >> 8) & 0xFF) as u8;
    bytes[1] = (num & 0xFF) as u8;
    bytes
}

pub(crate) fn u32_to_bytes_le(num: u32)-> [u8; 4] {
    let mut bytes = [0u8; 4];
    bytes[0] = (num & 0xFF) as u8;
    bytes[1] = ((num >> 8) & 0xFF) as u8;
    bytes[2] = ((num >> 16) & 0xFF) as u8;
    bytes[3] = ((num >> 24) & 0xFF) as u8;
    bytes
}

pub(crate) fn u32_to_bytes_be(num: u32)-> [u8; 4] {
    let mut bytes = [0u8; 4];
    bytes[0] = ((num >> 24) & 0xFF) as u8;
    bytes[1] = ((num >> 16) & 0xFF) as u8;
    bytes[2] = ((num >> 8) & 0xFF) as u8;
    bytes[3] = (num & 0xFF) as u8;
    bytes
}

pub(crate) fn u64_to_bytes_le(num: u64)-> [u8; 8] {
    let mut bytes = [0u8; 8];
    bytes[0] = (num & 0xFF) as u8;
    bytes[1] = ((num >> 8) & 0xFF) as u8;
    bytes[2] = ((num >> 16) & 0xFF) as u8;
    bytes[3] = ((num >> 24) & 0xFF) as u8;
    bytes[4] = ((num >> 32) & 0xFF) as u8;
    bytes[5] = ((num >> 40) & 0xFF) as u8;
    bytes[6] = ((num >> 48) & 0xFF) as u8;
    bytes[7] = ((num >> 56) & 0xFF) as u8;
    bytes
}

pub(crate) fn u64_to_bytes_be(num: u64)-> [u8; 8] {
    let mut bytes = [0u8; 8];
    bytes[0] = ((num >> 56) & 0xFF) as u8;
    bytes[1] = ((num >> 48) & 0xFF) as u8;
    bytes[2] = ((num >> 40) & 0xFF) as u8;
    bytes[3] = ((num >> 32) & 0xFF) as u8;
    bytes[4] = ((num >> 24) & 0xFF) as u8;
    bytes[5] = ((num >> 16) & 0xFF) as u8;
    bytes[6] = ((num >> 8) & 0xFF) as u8;
    bytes[7] = (num & 0xFF) as u8;
    bytes
}

pub(crate) fn u128_to_bytes_le(num: u128)-> [u8; 8] {
    let mut bytes = [0u8; 8];
    bytes[0] = (num & 0xFF) as u8;
    bytes[1] = ((num >> 8) & 0xFF) as u8;
    bytes[2] = ((num >> 16) & 0xFF) as u8;
    bytes[3] = ((num >> 24) & 0xFF) as u8;
    bytes[4] = ((num >> 32) & 0xFF) as u8;
    bytes[5] = ((num >> 40) & 0xFF) as u8;
    bytes[6] = ((num >> 48) & 0xFF) as u8;
    bytes[7] = ((num >> 56) & 0xFF) as u8;
    bytes
}

pub(crate) fn u128_to_bytes_be(num: u128)-> [u8; 8] {
    let mut bytes = [0u8; 8];
    bytes[0] = ((num >> 56) & 0xFF) as u8;
    bytes[1] = ((num >> 48) & 0xFF) as u8;
    bytes[2] = ((num >> 40) & 0xFF) as u8;
    bytes[3] = ((num >> 32) & 0xFF) as u8;
    bytes[4] = ((num >> 24) & 0xFF) as u8;
    bytes[5] = ((num >> 16) & 0xFF) as u8;
    bytes[6] = ((num >> 8) & 0xFF) as u8;
    bytes[7] = (num & 0xFF) as u8;
    bytes
}

#[derive(Clone)]
pub(crate) enum ByteWidth {
    OneByte,
    TwoByte,
    FourByte,
    EightByte,
    SixteenByte
}

#[derive(Clone)]
pub(crate) enum SignMode {
    Unsigned,
    Signed
}
