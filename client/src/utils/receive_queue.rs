// FILE: receive_queue.rs
// 
// DESCRIPTION: An abstraction that converts a message-based Read API to a byte-buffered Read output.
// 
// CHANGE LOG:
// 24 Jan 2021 -- Initial generation

use std::{
    io::{Read, Write},
    sync::mpsc::Receiver
};
use crate::utils::socket_utils::ShutdownSocket;
use crate::utils::err_handle::ResultBacktrace;
use std::ops::Index;
use std::sync::mpsc::channel;
use std::sync::{Arc, Mutex};
use wasm_bindgen::__rt::std::net::Shutdown;

pub(crate) trait SendMsg {
    fn send(&mut self, msg: &[u8]) -> ResultBacktrace<()>;
}

pub(crate) trait RecvMsg<M: Index<usize, Output = u8>> {
    fn receive(&mut self) -> std::io::Result<(M, bool)>;
}

pub(crate) trait Length {
    fn length(&self) -> usize;
}

impl Length for Vec<u8> {
    fn length(&self) -> usize {
        self.len()
    }
}

impl Length for [u8] {
    fn length(&self) -> usize {
       self.len()
    }
}

impl <'a> Length for &'a [u8] {
    fn length(&self) -> usize {
        self.len()
    // FILE: socket_utils.rs
//
// DESCRIPTION: Contains usefult traits and functions related to socket communication
//
// CHANGE LOG:
// 27 Jan 2021 -- Initial generation; moved ShutdownSocket trait over here from ipc to maintain
// compatibility with mobile targets.
}
}


pub(crate) struct ReceiveQueue<T : SendMsg + RecvMsg<M> + ShutdownSocket, M: Index<usize, Output = u8> + Length> {
    socket: T,
    current: Option<M>,
    off: usize
}

impl <T: SendMsg + RecvMsg<M> + ShutdownSocket, M: Index<usize, Output = u8> + Length> ReceiveQueue<T, M> {
    pub(crate) fn new(socket: T) -> ReceiveQueue<T, M> {
        ReceiveQueue {socket, current: None, off: 0 }
    }
}

impl <T: SendMsg + RecvMsg<M> + ShutdownSocket, M: Index<usize, Output = u8> + Length> Read for ReceiveQueue<T, M> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        // Keep reading from current until we run out of bytes, then read the next one.
        let mut read_bytes: usize = 0;
        let len = buf.len();
        loop {
            if let Some(current) = &self.current {
                let to_read: usize = std::cmp::min(buf.len() - read_bytes, current.length() as usize - self.off);
                if self.off + to_read > 0xFFFFFFFF {
                    return Err(std::io::Error::new(std::io::ErrorKind::InvalidData,
                                                   "Received message length exceeded maximum size of JavaScript array"))
                }
                for i in 0..to_read {
                    buf[read_bytes + i] = current[self.off + i];
                }
                read_bytes += to_read;
                self.off += to_read;
            }
            if read_bytes < len {
                match self.socket.receive() {
                    Ok((array, should_close)) => {
                        if should_close {
                            return Ok(read_bytes);
                        } else {
                            self.current = Some(array);
                            self.off = 0;
                        }
                    },
                    Err(e) => {
                        return Err(e);
                    }
                }
            } else {
                return Ok(len);
            }
        }
    }
}

impl <T: SendMsg + RecvMsg<M> + ShutdownSocket, M: Index<usize, Output = u8> + Length> Write for ReceiveQueue<T, M> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        match self.socket.send(buf) {
            Ok(_) => Ok(buf.len()),
            Err(e) => Err(std::io::Error::new(std::io::ErrorKind::Other, e))
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        // No code here, since send() sends immediately.
        Ok(())
    }
}

impl <T: SendMsg + RecvMsg<M> + ShutdownSocket, M: Index<usize, Output = u8> + Length> ShutdownSocket for ReceiveQueue<T, M> {
    fn shutdown_socket(&mut self, which: Shutdown) {
        self.socket.shutdown_socket(which)
    }
}
