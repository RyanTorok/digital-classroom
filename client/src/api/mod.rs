/// FILE: api/mod.rs
///
/// DESCRIPTION: This file acts as a header for the api submodule.
///
/// CHANGE LOG:
/// 10 Oct 2020 -- Initial generation

pub mod api;
pub mod ffi;
pub mod tuple;

#[cfg(not(feature = "mobile"))]
pub mod send_socket_command;
#[cfg(not(feature = "mobile"))]
pub mod encoding;
