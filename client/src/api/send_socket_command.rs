// FILE: api/send_socket_command.rs
//
// DESCRIPTION: This file contains the send_socket_command function, which the API functions call to
// indicate a command should be executed in socket mode.
//
// CHANGE LOG:
// 12 Oct 2020 -- Initial generation
// 23 Jan 2021 -- Downgrade lifetime of arguments to allow WASM integration (required some unsafe code).

use crate::command_utils::params::{Argument, ReturnValue};
use crate::api::encoding::{encode_command, read_command_response};
use crate::gui_socket::gui_client::{enqueue_command, empty_queue, no_sockets};
use std::sync::{Condvar, Mutex, Arc};
use crate::ipc::IPCConnection;
use std::io::Write;
use std::rc::Rc;
use crate::command_utils::err_codes::{ERR_NOBACKEND, ERR_RTNCOUNT};
use std::ops::DerefMut;
use futures::SinkExt;

/// Submits a command to be executed in socket mode, and returns the response. This function will
/// block until the response is received.

// The static lifetime in `arguments` looks worrying, but it's actually what we want. The Rust compiler
// can't verify that a Vec of any shorter-lived arguments will be valid when the closure below that
// writes the command to the socket is eventually polled from the queue, because the queue is static
// itself. However, luckily for us, the type(s) that use a reference inside the `Arguments` enum
// reference memory that wasn't allocated by Rust, but by the calling program. As long as we make sure
// send_socket_command can't return until the arguments are written to the socket (which we do by
// blocking until the command is finished executing and returns, let alone finished writing its
// arguments), the argument's reference will be valid, and then the resource it points to should
// be successfully deallocated by the calling program that allocated it in the first place after the
// command returns.
//
// Long story short: This code doesn't cause a memory leak even though we need a static lifetime.

//TODO remove type parameter (requires changes to API generation script)
pub(crate) fn send_socket_command<R>(opcode: u16, arguments: Vec<Argument>, expected_return_size: u16) -> Result<Vec<Option<ReturnValue>>, u16> {
    if no_sockets() {
        return Err(ERR_NOBACKEND);
    }
    let mut return_values: Vec<Option<ReturnValue>> = vec![];
    let owned_rv = &return_values as *const Vec<Option<ReturnValue>>;
    let encoding = encode_command(opcode, &arguments);
    let arc = Arc::new((Mutex::new(&return_values as *const Vec<Option<ReturnValue>> as usize), Condvar::new()));
    let clone = arc.clone();

    let closure = Box::new(move |socket: &mut IPCConnection| {
        std::io::stdout().flush();
        socket.write(encoding.as_slice());
        std::io::stdout().flush();
        // Put string contents
        for arg in &arguments {
            match arg {
                Argument::StringSlice(x) => {
                    socket.write(x.as_bytes());
                }
                Argument::BinarySlice(x) => {
                    socket.write(x);
                }
                _ => {}
            }
        }
        std::io::stdout().flush();
        socket.flush();
        let (mtx, cvar) = &*clone;
        let mut guard = mtx.lock().unwrap();
        let ptr_return_values = guard.deref_mut();
        let ptr_return_values = *ptr_return_values as *mut Vec<Option<ReturnValue>>;
        let is_internal = opcode >= 0xE000 && opcode < 0xF000;

        // This is safe because:
        // Since the command queue is static, so is this closure, which means the compiler can't
        // make any guarantees about when a reference to the return values list would go out of
        // scope if we passed it as &mut return_values in the mutex of `arc` above. Because the
        // Condvar stops send_socket_command() from returning (or reading `return_values`) until
        // this closure is done modifying `return_values`, we can safely return our owned copy of
        // `return_values` safe in the knowledge that it won't be modified while it's being read.
        // The mutex also prevents the Condvar from waking up when the length is > 0, but isn't 
        // done being written to by `read_command_response` yet.
        unsafe {
            let ref_return_values = &mut *ptr_return_values;
            read_command_response(socket, ref_return_values, is_internal);
        }
        cvar.notify_all();
    }) as Box<dyn Fn(&mut IPCConnection) + Send + Sync>;
    enqueue_command((&closure as *const Box<dyn Fn(&mut IPCConnection) + Send + Sync>) as usize);
    let (mtx, cvar) = &*arc;
    let mut guard = mtx.lock().unwrap();
    while return_values.len() == 0 {
        // mutex prevents len > 0 while the list isn't done yet
        guard = cvar.wait(guard).unwrap();
    }
    if return_values.len() == expected_return_size as usize {
        Ok(return_values)
    } else {
        println!("bad rtncount -- expected = {} actual = {}", expected_return_size, return_values.len());
        Err(ERR_RTNCOUNT)
    }
}

#[cfg(test)]
mod tests {
    use crate::api::send_socket_command::send_socket_command;
    use crate::command_utils::params::{Argument, ReturnValue};
    use crate::command_utils::err_codes::OK;
    use crate::commands::cmd_start_backend_socket::start_backend_socket;
    use crate::commands::opcodes::OPCODE_TEST_SOCKET_COMMAND;
    use crate::command_utils::params::ReturnValue::StringReference;
    use std::ffi::CStr;

    #[test]
    fn test() {
        start_backend_socket(8);
        let response = send_socket_command::<(u16, String)>(OPCODE_TEST_SOCKET_COMMAND, vec![Argument::StringSlice("Test message")], 2);
        match response {
            Ok(response) => {
                assert_eq!(2, response.len());
                assert_eq!(OK, match &response[0] {
                    ReturnValue::U16(v) => {*v}
                    _ => {
                        panic!("Return type bad.");
                    }
                });
                unsafe {
                    assert_eq!("Received message: Test message", match &response[1] {
                        ReturnValue::StringReference(s) => { CStr::from_ptr(*s).to_str().unwrap().to_owned() }
                        _ => { panic!("Return type bad.") }
                    });
                }
            }
            Err(e) => {
                println!("{}", e);
                panic!("Command execution failed.");
            },
        }
    }
}
