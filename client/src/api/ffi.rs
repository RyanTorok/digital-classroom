use std::str::Utf8Error;
use std::ffi::{CStr, CString};
use std::rc::Rc;
use crate::command_utils::err_codes::{ERR_NONUTF8, ERR_NULLPTR, ERR_RTNTYPE};
use crate::command_utils::params::ReturnValue;
use uuid::Uuid;
use core::slice;

#[cfg(feature = "web")]
pub(crate) type StringArgType = String;

#[cfg(not(feature = "web"))]
pub(crate) type StringArgType = *const libc::c_char;

#[cfg(feature = "web")]
pub(crate) type StringReturnType = js_sys::JsString;

#[cfg(not(feature = "web"))]
pub(crate) type StringReturnType = *const libc::c_char;

#[cfg(feature = "web")]
pub(crate) type BinaryArgType = Vec<u8>;

#[cfg(not(feature = "web"))]
pub(crate) type BinaryArgType = *const CSlice;

#[cfg(feature = "web")]
pub(crate) type BinaryReturnType = js_sys::Uint8Array;

#[cfg(not(feature = "web"))]
pub(crate) type BinaryReturnType = *const CSlice;

pub(crate) type StringInternalArgType<'a> = &'a str;

pub(crate) type StringInternalReturnType = String;

pub(crate) type BinaryInternalArgType<'a> = &'a [u8];

pub(crate) type BinaryInternalReturnType = Vec<u8>;

#[repr(C)]
#[derive(Clone)]
pub struct CSlice {
    ptr: *const u8,
    len: usize
}

impl CSlice {
    pub(crate) fn new(ptr: *const u8, len: usize) -> CSlice {
        CSlice {ptr, len}
    }
}

#[cfg(feature = "web")]
pub(crate) fn unwrap_string_reference<'a>(string: &'a String) -> Result<&'a str, u16> {
    let x: &'a str = &string;
    Ok(x)
}

#[cfg(not(feature = "web"))]
pub(crate) fn unwrap_string_reference(ptr: &*const libc::c_char) -> Result<&str, u16> {
    let ptr: *const libc::c_char = *ptr;
    if ptr == std::ptr::null() {
        return Err(ERR_NULLPTR)
    }
    let c_str: &CStr = unsafe { CStr::from_ptr(ptr) };
    match c_str.to_str() {
        Ok(c_str) => Ok(c_str),
        Err(e) => Err(ERR_NONUTF8)
    }
}

#[cfg(feature = "web")]
pub(crate) fn wrap_string_reference(string: String) -> js_sys::JsString {
    string.into()
}

#[cfg(not(feature = "web"))]
pub(crate) fn wrap_string_reference(string: String) -> *const libc::c_char {
    // We don't have to return an error code here because we can trust ourselves to never
    // have a command return a string with a null byte. The Result<> object here doesn't depend
    // on any externally generated variable.
    let c_string = CString::new(string.as_bytes()).unwrap();
    c_string.into_raw()
}

#[cfg(feature = "web")]
pub(crate) fn unwrap_binary_reference<'a>(array: &'a Vec<u8>) -> Result<&'a [u8], u16> {
    let x: &'a [u8] = &array;
    Ok(x)
}


// Converts a the ffi-provided CSlice pointer to a Rust byte slice
//
// Safety: The pointer provided must be a valid reference to a valid CSlice, where the range from
// c_slice.ptr and c_slice.ptr + len refers to valid memory.
//
// Note: this function should be unsafe, but we don't mark it as such to avoid compiler warnings
// on other targets (the WASM equivalent of this function is not unsafe)
#[cfg(not(feature = "web"))]
pub(crate) fn unwrap_binary_reference(ptr: &*const CSlice) -> Result<&[u8], u16> {
    unsafe {
        let ptr: *const CSlice = *ptr;
        if ptr == std::ptr::null() {
            return Err(ERR_NULLPTR)
        }
        let c_slice: &CSlice = &*ptr;
        Ok(slice::from_raw_parts(c_slice.ptr, c_slice.len))
    }
}

#[cfg(feature = "web")]
pub(crate) fn wrap_binary_reference(vec: Vec<u8>) -> js_sys::Uint8Array {
    if vec.len() > 0xFFFFFFFF {
        panic!("Cannot return array of length that exceeds a u32.");
    }
    let js_array = js_sys::Uint8Array::new_with_length(vec.len() as u32);
    for i in 0..vec.len() {
        js_array.set_index(i as u32, vec[i]);
    }
    js_array
}

#[cfg(not(feature = "web"))]
pub(crate) fn wrap_binary_reference(vec: Vec<u8>) -> *const CSlice {
    let c_slice = Box::new(CSlice { ptr: vec.as_ptr(), len: vec.len() });
    std::mem::forget(vec);
    Box::into_raw(c_slice)
}

pub(crate) unsafe fn unwrap_uuid(val: *const u128) -> uuid::Uuid {
    uuid::Uuid::from_u128(*val)
}

pub(crate) fn wrap_uuid(val: uuid::Uuid) -> u128 {
    val.as_u128()
}

pub(crate) fn ret_uint8(r: ReturnValue) -> Result<u8, u16> {
    match r {
        ReturnValue::U8(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_uint16(r: ReturnValue) -> Result<u16, u16> {
    match r {
        ReturnValue::U16(n) => Ok(n),
        _ => {
            Err(ERR_RTNTYPE)
        }
    }
}

pub(crate) fn ret_uint32(r: ReturnValue) -> Result<u32, u16> {
    match r {
        ReturnValue::U32(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_uint64(r: ReturnValue) -> Result<u64, u16> {
    match r {
        ReturnValue::U64(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_uint128(r: ReturnValue) -> Result<u128, u16> {
    match r {
        ReturnValue::U128(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_int8(r: ReturnValue) -> Result<i8, u16> {
    match r {
        ReturnValue::I8(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_int16(r: ReturnValue) -> Result<i16, u16> {
    match r {
        ReturnValue::I16(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_int32(r: ReturnValue) -> Result<i32, u16> {
    match r {
        ReturnValue::I32(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}


pub(crate) fn ret_int64(r: ReturnValue) -> Result<i64, u16> {
    match r {
        ReturnValue::I64(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_int128(r: ReturnValue) -> Result<i128, u16> {
    match r {
        ReturnValue::I128(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_float32(r: ReturnValue) -> Result<f32, u16> {
    match r {
        ReturnValue::F32(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_float64(r: ReturnValue) -> Result<f64, u16> {
    match r {
        ReturnValue::F64(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_uuid(r: ReturnValue) -> Result<Uuid, u16> {
    match r {
        ReturnValue::Uuid(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }
}
#[cfg(feature = "web")]
pub(crate) fn ret_string(r: ReturnValue) -> Result<js_sys::JsString, u16> {
    match r {
        ReturnValue::StringReference(string) => Ok(js_sys::JsString::from(string.as_str())),
        _ => Err(ERR_RTNTYPE)
    }
}

#[cfg(not(feature = "web"))]
pub(crate) fn ret_string(r: ReturnValue) -> Result<*const libc::c_char, u16> {
    match r {
        ReturnValue::StringReference(n) => Ok(n.into_raw()),
        _ => Err(ERR_RTNTYPE)
    }
}

pub(crate) fn ret_internal_string(r: ReturnValue) -> Result<String, u16> {
    match r {
        ReturnValue::String(n) => Ok(n),
        _ => Err(ERR_RTNTYPE)
    }

}


#[cfg(feature = "web")]
pub(crate) fn ret_binary(r: ReturnValue) -> Result<js_sys::Uint8Array, u16> {
    match r {
        ReturnValue::Binary(vec) => Ok(js_sys::Uint8Array::from(vec.as_slice())),
        _ => Err(ERR_RTNTYPE)
    }
}

#[cfg(not(feature = "web"))]
pub(crate) fn ret_binary(r: ReturnValue) -> Result<*const CSlice, u16> {
    match r {
        ReturnValue::Binary(vec) => {
            //TODO Use Vec::into_raw_parts instead when it is stabilized
            let c_slice = CSlice::new(vec.as_ptr(), vec.len());
            std::mem::forget(vec);
            let box_c_slice = Box::new(c_slice);
            Ok(Box::into_raw(box_c_slice))
        }
        _ => {
            Err(ERR_RTNTYPE)
        }
    }
}

#[cfg(feature = "web")]
pub(crate) fn ret_internal_binary(r: ReturnValue) -> Result<BinaryInternalReturnType, u16> {
    match r {
        ReturnValue::Binary(vec) => Ok(vec),
        _ => Err(ERR_RTNTYPE)
    }
}

#[cfg(feature = "web")]
#[inline]
pub(crate) fn string_default() -> StringReturnType {
    js_sys::JsString::from("")
}

#[cfg(not(feature = "web"))]
#[inline]
pub(crate) fn string_default() -> StringReturnType {
    0 as *const libc::c_char
}
#[cfg(feature = "web")]
#[inline]
pub(crate) fn binary_default() -> BinaryReturnType {
    js_sys::Uint8Array::new_with_length(0)
}

#[cfg(not(feature = "web"))]
#[inline]
pub(crate) fn binary_default() -> BinaryReturnType {
    0 as *const CSlice
}

#[inline]
pub(crate) fn internal_string_default() -> StringInternalReturnType {
    String::new()
}

#[inline]
pub(crate) fn internal_binary_default() -> BinaryInternalReturnType {
    Vec::new()
}


