// FILE: api/encoding.rs
//
// DESCRIPTION: This file is the counterpart to command_executor/encoding.rs, which decodes commands
// sent over the socket. This file is responsible for taking a command and producing that encoding
// to send over the socket.
//
// CHANGE LOG:
// 25 Oct 2020 -- Initial generation


use crate::command_utils::params::{Argument, ReturnValue};
use crate::utils::byte_utils::*;
use crate::command_utils::type_codes::*;
use crate::ipc::IPCConnection;
use crate::command_utils::err_codes::ERR_DECODE;
use std::io::Read;
use std::sync::Mutex;
use byteorder::{LittleEndian, ByteOrder};
use crate::utils::err_handle::{ResultBacktrace, unwrap_or_wrap_std_err, leaf_err};
use std::string::FromUtf8Error;
use std::ffi::CString;
use futures::AsyncReadExt;
use std::rc::Rc;
#[cfg(not(feature = "web"))]
use crate::api::ffi::CSlice;

/// Encodes a command into socket form. Returns a list of bytes representing the command encoding.
/// The string contents are not added automatically to avoid extra copying. The logic to write the
/// encoding to the socket should parse the argument array for strings to write to the socket after
/// the bytes returned by this function.

pub(crate) fn encode_command(opcode: u16, arguments: &[Argument]) -> Vec<u8> {
    // We add the space for the type codes so we can write in the type codes and the values in
    // a single pass without any copying.
    let mut bytes: Vec<u8> = vec![0u8; 4 + arguments.len()];

    // push opcode and argument count
    let opcode_bytes = opcode.to_le_bytes();
    bytes[0] = opcode_bytes[0];
    bytes[1] = opcode_bytes[1];

    let arg_count_bytes = (arguments.len() as u16).to_le_bytes();
    bytes[2] = arg_count_bytes[0];
    bytes[3] = arg_count_bytes[1];

    // Push argument types and values (strings store the length here)
    let mut arg_index = 4;
    for arg in arguments {
        match arg {
            Argument::U8(x) => {
                bytes[arg_index] = ENCODING_U8;
                bytes.push(*x);
            },
            Argument::U16(x) => {
                bytes[arg_index] = ENCODING_U16;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            }
            Argument::U32(x) => {
                bytes[arg_index] = ENCODING_U32;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::U64(x) => {
                bytes[arg_index] = ENCODING_U64;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::U128(x) => {
                bytes[arg_index] = ENCODING_U128;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::I8(x) => {
                bytes[arg_index] = ENCODING_I8;
                bytes.push(*x as u8);
            },
            Argument::I16(x) => {
                bytes[arg_index] = ENCODING_I16;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            }
            Argument::I32(x) => {
                bytes[arg_index] = ENCODING_I32;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::I64(x) => {
                bytes[arg_index] = ENCODING_I64;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::I128(x) => {
                bytes[arg_index] = ENCODING_I128;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::F32(x) => {
                bytes[arg_index] = ENCODING_F32;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::F64(x) => {
                bytes[arg_index] = ENCODING_F64;
                (*x).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::Uuid(x) => {
                bytes[arg_index] = ENCODING_UUID;
                x.as_u128().to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::StringSlice(x) => {
                bytes[arg_index] = ENCODING_STRING;
                (x.len() as u32).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            Argument::BinarySlice(x) => {
                bytes[arg_index] = ENCODING_BINARY;
                (x.len() as u32).to_le_bytes().iter().for_each(|byte| bytes.push(*byte))
            },
            _ => {
                unreachable!("Received an argument type that was not API visible and cannot result from a command.")
            }
        }
    }
    // Unlike the analogous decode function in command_executor/encoding.rs, we don't add the
    // string contents here because we can write them directly to the socket, which saves a round
    // of copying.
    bytes
}

/// Counterpart to the client backend's encode_response() in command_executor/encoding.rs.
/// Decodes a command response received from the socket. Returns a tuple of the return values
/// (including the exit code), and the execution time in milliseconds, or 0xFFFF if the time >=
/// 65535 ms. For more details about the response encoding, see the documentation for
/// encode_response() in command_executor/encoding.rs.
///
/// If a decode error occurs, this function reserves the right to stop reading from the socket, and
/// it is expected the socket be reset before the next command in this case to prevent extraneous
/// bytes that weren't read being treated as part of the next command's result.

pub(crate) fn read_command_response(stream: &mut IPCConnection, return_values: &mut Vec<Option<ReturnValue>>, internal: bool) {
    let mut res_return = || -> Result<(), std::io::Error> {
        let mut buffer = [0u8; 16];

        // read time and return value count
        stream.read_exact(&mut buffer[0..4])?;

        let time: u16 = LittleEndian::read_u16(&buffer[0..2]);
        let return_count = LittleEndian::read_u16(&buffer[2..4]);

        let return_count = return_count as usize;
        let mut string_lengths = vec![0u32; return_count];

        let return_types_buf: &mut [u8];
        let mut vec;
        if return_count <= 8 {
            // We have <= 16 bytes of return types, so we can reuse the buffer we already have on the stack.
            return_types_buf = &mut buffer[0..return_count];
        } else {
            // Otherwise we need a new vector on the heap that can fit enough return types.
            vec = vec![0u8; return_count];
            return_types_buf = vec.as_mut_slice();
        }
        // Read return types into buffer
        stream.read_exact(return_types_buf)?;

        // Find out the total size of the return values so we only have to invoke the receive syscall once.
        let mut value_size: usize = 0;
        for i in 0..return_count {
            match return_types_buf[i] {
                ENCODING_U8 | ENCODING_I8 => {
                    value_size += 1;
                }
                ENCODING_U16 | ENCODING_I16 => {
                    value_size += 2;
                }
                // String is here because the length is stored as the value as a u32.
                ENCODING_U32 | ENCODING_I32 | ENCODING_F32 | ENCODING_STRING | ENCODING_BINARY => {
                    value_size += 4;
                }
                ENCODING_U64 | ENCODING_I64 | ENCODING_F64 => {
                    value_size += 8;
                }
                ENCODING_U128 | ENCODING_I128 | ENCODING_UUID => {
                    value_size += 16;
                }
                _ => {
                    return Ok(return_decode_error(return_values, format!("Invalid argument type code for argument {}: {}", i + 1, return_types_buf[i])));
                }
            };
        }

        // Read the return values
        let mut value_bytes = vec![0u8; value_size];
        stream.read_exact(&mut value_bytes)?;
        let mut values_offset = 0;
        return_values.extend(std::iter::repeat(Option::from(ReturnValue::U8(0))).take(return_count));
        for i in 0..return_count {
            match return_types_buf[i] {
                ENCODING_U8 => {
                    return_values[i] = Option::from(ReturnValue::U8(value_bytes[values_offset]));
                    values_offset += 1;
                }
                ENCODING_U16 => {
                    return_values[i] = Option::from(ReturnValue::U16(LittleEndian::read_u16(&value_bytes[values_offset..values_offset + 2])));
                    values_offset += 2;
                }
                ENCODING_U32 => {
                    return_values[i] = Option::from(ReturnValue::U32(LittleEndian::read_u32(&value_bytes[values_offset..values_offset + 4])));
                    values_offset += 4;
                }
                ENCODING_U64 => {
                    return_values[i] = Option::from(ReturnValue::U64(LittleEndian::read_u64(&value_bytes[values_offset..values_offset + 8])));
                    values_offset += 8;
                }
                ENCODING_U128 => {
                    return_values[i] = Option::from(ReturnValue::U128(LittleEndian::read_u128(&value_bytes[values_offset..values_offset + 16])));
                    values_offset += 16;
                }
                ENCODING_I8 => {
                    return_values[i] = Option::from(ReturnValue::I8(value_bytes[values_offset] as i8));
                    values_offset += 1;
                }
                ENCODING_I16 => {
                    return_values[i] = Option::from(ReturnValue::I16(LittleEndian::read_i16(&value_bytes[values_offset..values_offset + 2])));
                    values_offset += 2;
                }
                ENCODING_I32 => {
                    return_values[i] = Option::from(ReturnValue::I32(LittleEndian::read_i32(&value_bytes[values_offset..values_offset + 4])));
                    values_offset += 4;
                }
                ENCODING_I64 => {
                    return_values[i] = Option::from(ReturnValue::I64(LittleEndian::read_i64(&value_bytes[values_offset..values_offset + 8])));
                    values_offset += 8;
                }
                ENCODING_I128 => {
                    return_values[i] = Option::from(ReturnValue::I128(LittleEndian::read_i128(&value_bytes[values_offset..values_offset + 16])));
                    values_offset += 16;
                }
                ENCODING_F32 => {
                    return_values[i] = Option::from(ReturnValue::F32(LittleEndian::read_f32(&value_bytes[values_offset..values_offset + 4])));
                    values_offset += 4;
                }
                ENCODING_F64 => {
                    return_values[i] = Option::from(ReturnValue::F64(LittleEndian::read_f64(&value_bytes[values_offset..values_offset + 8])));
                    values_offset += 8;
                }
                ENCODING_UUID => {
                    return_values[i] = Option::from(ReturnValue::Uuid(uuid::Uuid::from_u128(LittleEndian::read_u128(&value_bytes[values_offset..values_offset + 16]))));
                    values_offset += 16;
                }
                ENCODING_STRING => {
                    string_lengths[i] = LittleEndian::read_u32(&value_bytes[values_offset..values_offset + 4]);
                    values_offset += 4;
                }
                ENCODING_BINARY => {
                    string_lengths[i] = LittleEndian::read_u32(&value_bytes[values_offset..values_offset + 4]);
                    values_offset += 4;
                }
                _ => {
                    unreachable!("Should have caught an invalid type code in the previous loop.")
                }
            };
        }
        //Add string contents
        for i in 0..return_count {
            match return_types_buf[i] {
                ENCODING_STRING => {
                    let length = string_lengths[i] as usize;
                    let mut string_buf = vec![0u8; length];
                    stream.read_exact(&mut string_buf);
                    // Internal strings are passed as reference-counted to avoid copying,
                    // whereas API-visible commands need to return a hard value, so they can
                    // be intentionally memory-leaked later, again without copying.
                    if internal {
                        match wrap_internal_return_string(string_buf, i) {
                            Ok(s) => {
                                return_values[i] = Option::from(ReturnValue::String(s))
                            },
                            Err(e) => return_decode_error(return_values, e.to_string())
                        }
                    } else {
                        match wrap_return_string(string_buf, i) {
                            Ok(s) => {
                                return_values[i] = Option::from(ReturnValue::StringReference(s))
                            },
                            Err(e) => return_decode_error(return_values, e.to_string())
                        };
                    }
                }
                ENCODING_BINARY => {
                    let length = string_lengths[i] as usize;
                    let mut binary_buf = vec![0u8; length];
                    stream.read_exact(&mut binary_buf);
                    return_values[i] = Option::from(ReturnValue::Binary(binary_buf))
                }
                _ => {}
            };
        }
        Ok(())
    };
    let result = res_return();
    match result {
        Ok(return_values) => {}
        Err(e) => {
            return_decode_error(return_values,format!("Not enough bytes received from socket on command return: {}", e.to_string()));
        }
    }
}

fn return_decode_error(list: &mut Vec<Option<ReturnValue>>, message: String) {
    while list.len() < 2 {
        list.push(Option::from(ReturnValue::U8(0)));
    }
    list[0] = Option::from(ReturnValue::U16(ERR_DECODE));
    list[1] = Option::from(ReturnValue::String(message));
}

#[cfg(feature = "web")]
fn wrap_return_string(vec: Vec<u8>, i: usize) -> ResultBacktrace<String> {
    wrap_internal_return_string(vec, i)
}

#[cfg(not(feature = "web"))]
fn wrap_return_string(string_buf: Vec<u8>, i: usize) -> ResultBacktrace<CString> {
    match CString::new(string_buf) {
        Ok(string) => {
            Ok(string)
        },
        Err(e) => {
            leaf_err(&format!("Got non-UTF-8 string in return value index: {}", i))
        }
    }
}

fn wrap_internal_return_string(vec: Vec<u8>, i: usize) -> ResultBacktrace<String> {
    match String::from_utf8(vec) {
        Ok(string) => Ok(string),
        Err(err) => {
            leaf_err(&format!("Got non-UTF-8 string in return value index: {}", i))
        },
    }
}

