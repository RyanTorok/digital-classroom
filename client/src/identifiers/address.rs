#[allow(dead_code)]

use crate::identifiers::address::Suffix::{CUSTOM, PLACE, ROUTE};
use std::fmt::format;
use crate::identifiers::address::Include::{Long, Short};
use std::ops::{RangeBounds, Range, Deref};
use crate::identifiers::address::State::*;
use crate::identifiers::address::Country::{USA, Other};
use crate::identifiers::address::Direction::{North, Northeast, Northwest, East, Southeast, South, Southwest, West};
use crate::identifiers::address::Street::{NoPrefix, StateHighway, FarmToMarket, RanchToMarket, UsHighway, InterstateHighway, Route};

const NULL: String = String::new();

fn add_space(s: &mut String) {
    if s.len() != 0 && !s.ends_with(' ') {
        s.push(' ');
    }
}

fn add_comma_space(s: &mut String) {
    if (s.len() == 0) {
        return;
    }
    if !s.ends_with(' ') {
        s.push_str(", ");
    } else {
        let change_index = s.trim_end().len();
        s.replace_range(change_index.., ", ")
    }
}

pub(crate) struct Address {
    number: u32,
    direction: Direction,
    street: Street,
    suffix: Suffix,
    apartment: Apartment,
    city: String,
    state: State,
    zip: u32,
    country: Country,
}

pub(crate) const NULL_ADDRESS: Address = Address {
    number: 0,
    direction: Direction::None,
    street: Street::NoPrefix(NULL),
    suffix: CUSTOM(NULL),
    apartment: Apartment::None,
    city: NULL,
    state: State::None,
    zip: 0,
    country: Country::USA,
};

impl Address {
    /*/// Constructs an Address object from a string reference of one of the following forms:
    /// "<number> <street> <suffix> <city>, <state> <zip>"
    /// "<number> <street> <suffix> <apartment> <city>, <state> <zip>"
    /// "<number> <street> <suffix> <city>, <state> <zip>, <country>"
    /// "<number> <street> <suffix> <apartment> <city>, <state> <zip>, <country>"
    ///
    /// returns: Address object on success, one of the following error codes on failure:
    /// 1: No word ended in a comma to use as a city name
    /// 2: First word ending in a comma came first or directly after the street number, leaving no space for the street name.
    pub(crate) fn from(address: &str) -> Result<Address, u8> {
        //address object parameters
        let mut number: u32 = 0;
        let mut direction: Direction;
        let mut street: Street;
        let mut suffix: Suffix;
        let mut apartment: Apartment;
        let mut


        let mut street_name_start: u32 = 0;
        let mut street_name_end: u32 = 0;
        let words = address.split_whitespace().collect::<Vec<&str>>();
        let mut i = 0;

        //step 1: try to parse the first word, which should be the address number
        let result = words[0].parse::<u32>();
        if !result.is_err() {
            number = result.unwrap();
            street_name_start = i + 1;
        } else {
            street_name_start = i;
        }

        i += 1;

        // Approach: there might be an adversarial street name like "Boulevard Lane St", where we can't use the words to determine where the street name ends.
        // Instead, we treat the first word that ends in a comma as the city, the one before it as the suffix, and all before that as the street name.

        //step 2: find the first word that ends in a comma. That's the city name.
        let mut found = false;
        let mut first_comma_word = street_name_start;
        for i in street_name_start..(words.len() as u32) {
            if (words.get(i) as &str).ends_with(',') {
                first_comma_word = i;
                found = true;
                break;
            }
        }
        if !found {
            return Result::Err(1);
        }

        //step 3: the words in between the street number and the first comma word are "<street> <suffix>" or "<street name> <suffix> <apartment>". We'll call this section the 'body' from now on.
        //we can detect whether there is an apartment there by seeing if we get the word or abbreviation for apartment, unit, or room, followed by a word that has at least one digit in it.
        //e.g. 'Apt. 123' and 'Unit 89E' are both valid apartment signatures, but 'Apt. Blue' is not.
        let num_body_words = first_comma_word - street_name_start - 1;
        if num_body_words < 1 {
            return Result::Err(1);
        }

        match num_body_words {
            1 => {
                //if we only have one word, it must be the street name

            }
            _ => {}
        }

        return Result::Err(0);
    }*/


    //Constructs an Address object from the given parameters
    pub(crate) fn from(street_address: &str, city: &str, state: &str, zip: u32, country: &str) -> Result<Address, u8> {
        return Result::Err(1);
    }

    /// Produces a short String representation of an Address object. (Roughly what you would write on mail)
    pub(crate) fn to_short_string(&self) -> String {
        return self.to_string_explicit(SHORT_FORMAT);
    }

    /// Produces a long String representation of an Address object.
    pub(crate) fn to_long_string(&self) -> String {
        return self.to_string_explicit(LONG_FORMAT);
    }

    /// Produces the default String representation of an Address object.
    pub(crate) fn to_string(&self) -> String {
        return self.to_short_string();
    }

    /// Constructs a String representation of an Address based on the given AddressFormat
    pub(crate) fn to_string_explicit(&self, format: AddressFormat) -> String {
        let mut string = String::from("");

        //put the street number
        match format.number {
            Include::None => {}
            Short => { string.push_str(self.number.to_string().as_ref()) }
            Long => { string.push_str(self.number.to_string().as_ref()) }
        }

        //put the prefix direction, if applicable (e.g. the W in 'W Main St')
        add_space(&mut string);
        match format.direction {
            Include::None => {}
            Short => { string.push_str(self.direction.short_name().as_ref()) }
            Long => { string.push_str(self.direction.long_name().as_ref()) }
        }

        //put the street name
        add_space(&mut string);
        match format.street {
            Include::None => {}
            Short => { string.push_str(self.street.short_name().as_ref()) }
            Long => { string.push_str(self.street.long_name().as_ref()) }
        }

        //put the suffix
        add_space(&mut string);
        match format.suffix {
            Include::None => {}
            Short => { string.push_str(self.suffix.short_name().as_ref()) }
            Long => { string.push_str(self.suffix.long_name().as_ref()) }
        }

        //put the apartment number, if applicable
        add_space(&mut string);
        match format.apartment {
            Include::None => {}
            Short => { string.push_str(self.apartment.short_name().as_ref()) }
            Long => { string.push_str(self.apartment.long_name().as_ref()) }
        }

        //put the city name
        add_space(&mut string);
        match format.city {
            Include::None => {}
            Short => { string.push_str(self.city.as_ref()) }
            Long => { string.push_str(self.city.as_ref()) }
        }

        //put the state
        match format.state {
            Include::None => {}
            Short => {
                add_comma_space(&mut string);
                string.push_str(self.state.short_name().as_ref())
            }
            Long => {
                add_comma_space(&mut string);
                string.push_str(self.state.long_name().as_ref())
            }
        }

        //put the zip code
        add_space(&mut string);
        match format.zip {
            Include::None => {}
            Short => { string.push_str(self.zip.to_string().as_ref()) }
            Long => { string.push_str(self.zip.to_string().as_ref()) }
        }

        //put the country

        match format.country {
            Include::None => {}
            Short => {
                add_comma_space(&mut string);
                string.push_str(self.country.short_name().as_ref())
            }
            Long => {
                add_comma_space(&mut string);
                string.push_str(self.country.long_name().as_ref())
            }
        }

        //if one or more of the last fields weren't added, we could have a trailing space,
        // which we can truncate without losing ownership.
        if string.ends_with(' ') {
            string.truncate(string.len() - 1)
        }

        return string;
    }
}

pub(crate) struct AddressFormat {
    number: Include,
    direction: Include,
    street: Include,
    suffix: Include,
    apartment: Include,
    city: Include,
    state: Include,
    zip: Include,
    country: Include,
}

const SHORT_FORMAT: AddressFormat = AddressFormat {
    number: Short,
    direction: Short,
    street: Short,
    suffix: Short,
    apartment: Short,
    city: Short,
    state: Short,
    zip: Short,
    country: Short,
};

const LONG_FORMAT: AddressFormat = AddressFormat {
    number: Long,
    direction: Long,
    street: Long,
    suffix: Long,
    apartment: Long,
    city: Long,
    state: Long,
    zip: Long,
    country: Long,
};


pub(crate) enum Include {
    None = 0,
    Short = 1,
    Long = 2,
}

enum Direction {
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
    None,
}

impl Direction {
    fn short_name(&self) -> &str {
        return match self {
            Direction::North => { "N" }
            Direction::Northeast => { "NE" }
            Direction::East => { "E" }
            Direction::Southeast => { "SE" }
            Direction::South => { "S" }
            Direction::Southwest => { "SW" }
            Direction::West => { "W" }
            Direction::Northwest => { "NW" }
            Direction::None => { "" }
        };
    }

    fn long_name(&self) -> &str {
        return match self {
            Direction::North => { "North" }
            Direction::Northeast => { "Northeast" }
            Direction::East => { "East" }
            Direction::Southeast => { "Southeast" }
            Direction::South => { "South" }
            Direction::Southwest => { "Southwest" }
            Direction::West => { "West" }
            Direction::Northwest => { "Northwest" }
            Direction::None => { "" }
        };
    }

    fn from(name: &str) -> Direction {
        return match name.to_ascii_lowercase().as_ref() {
            "north" => { North }
            "n" => { North }
            "northeast" => { Northeast }
            "ne" => { Northeast }
            "east" => { East }
            "e" => { East }
            "southeast" => { Southeast }
            "se" => { Southeast }
            "south" => { Direction::South }
            "s" => { Direction::South }
            "southwest" => { Southwest }
            "sw" => { Southwest }
            "west" => { Direction::West }
            "w" => { Direction::West }
            "northwest" => { Northwest }
            "nw" => { Northwest }
            _ => { Direction::None }
        };
    }
}

enum Street {
    StateHighway(u32),
    UsHighway(u32),
    InterstateHighway(u32),
    Route(u32),
    FarmToMarket(u32),
    RanchToMarket(u32),
    NoPrefix(String),
}

impl Street {
    fn short_name(&self) -> &str {
        return match self {
            Street::StateHighway(num) => { "SH" }
            Street::UsHighway(num) => { "US" }
            Street::InterstateHighway(num) => { "IH" }
            Street::Route(num) => { "Route " }
            Street::FarmToMarket(num) => { "F.M." }
            Street::RanchToMarket(num) => { "R.M." }
            Street::NoPrefix(string) => { string }
        };
    }

    fn long_name(&self) -> &str {
        return match self {
            Street::StateHighway(num) => { "State Highway" }
            Street::UsHighway(num) => { "US" }
            Street::InterstateHighway(num) => { "Interstate" }
            Street::Route(num) => { "Route" }
            Street::FarmToMarket(num) => { "Farm to Market Road" }
            Street::RanchToMarket(num) => { "Ranch to Market Road" }
            Street::NoPrefix(string) => { string }
        };
    }

    fn from(name: &str) -> Street {
        let split = name.split_whitespace().collect::<Vec<&str>>();
        if split.len() == 0 {
                return Street::NoPrefix(String::new());
        }
        if split.len() == 2 {
            let maybe_integer = split[1].parse::<u32>();
            if maybe_integer.is_ok() {
                return match split[0].to_ascii_uppercase().as_ref() {
                    "SH" => { StateHighway(maybe_integer.unwrap()) }
                    "S.H." => { StateHighway(maybe_integer.unwrap()) }
                    "US" => { UsHighway(maybe_integer.unwrap()) }
                    "U.S." => { UsHighway(maybe_integer.unwrap()) }
                    "IH" => { InterstateHighway(maybe_integer.unwrap()) }
                    "I.H." => {InterstateHighway(maybe_integer.unwrap()) }
                    "Route" => { Route(maybe_integer.unwrap()) }
                    "RT" => { Route(maybe_integer.unwrap()) }
                    "FM" => { FarmToMarket(maybe_integer.unwrap()) }
                    "F.M." => { FarmToMarket(maybe_integer.unwrap()) }
                    "RM" => { RanchToMarket(maybe_integer.unwrap()) }
                    "R.M." => { RanchToMarket(maybe_integer.unwrap()) }
                    _ => {Street::NoPrefix(name.to_string())}
                }
            }
        }
        return Street::NoPrefix(name.to_string());
    }
}

enum Suffix {
    STREET,
    ROAD,
    DRIVE,
    BOULEVARD,
    LANE,
    AVENUE,
    CIRCLE,
    HIGHWAY,
    FREEWAY,
    EXPRESSWAY,
    TOLLWAY,
    PARKWAY,
    SPEEDWAY,
    WAY,
    ROUTE,
    WALK,
    PLACE,
    TERRACE,
    TRAIL,
    NoEnding,
    CUSTOM(String),
}

impl Suffix {
    fn long_name(&self) -> &str {
        return match self {
            Suffix::STREET => { "Street" }
            Suffix::ROAD => { "Road" }
            Suffix::DRIVE => { "Drive" }
            Suffix::BOULEVARD => { "Boulevard" }
            Suffix::LANE => { "Lane" }
            Suffix::AVENUE => { "Avenue" }
            Suffix::CIRCLE => { "Circle" }
            Suffix::HIGHWAY => { "Highway" }
            Suffix::FREEWAY => { "Freeway" }
            Suffix::EXPRESSWAY => { "Expressway" }
            Suffix::TOLLWAY => { "Tollway" }
            Suffix::PARKWAY => { "Parkway" }
            Suffix::SPEEDWAY => { "Speedway" }
            Suffix::WAY => { "Way" }
            Suffix::ROUTE => { "Route" }
            Suffix::WALK => { "Walk" }
            Suffix::PLACE => { "Place" }
            Suffix::TERRACE => { "Terrace" }
            Suffix::TRAIL => { "Trail" }
            Suffix::NoEnding => { "" }
            Suffix::CUSTOM(string) => { string }
        };
    }

    fn short_name(&self) -> &str {
        return match self {
            Suffix::STREET => { "St" }
            Suffix::ROAD => { "Rd" }
            Suffix::DRIVE => { "Dr" }
            Suffix::BOULEVARD => { "Blvd" }
            Suffix::LANE => { "Ln" }
            Suffix::AVENUE => { "Ave" }
            Suffix::CIRCLE => { "Cir" }
            Suffix::HIGHWAY => { "Hwy" }
            Suffix::FREEWAY => { "Fwy" }
            Suffix::EXPRESSWAY => { "Expy" }
            Suffix::TOLLWAY => { "Tollway" }
            Suffix::PARKWAY => { "Pkwy" }
            Suffix::SPEEDWAY => { "Spdwy" }
            Suffix::WAY => { "Way" }
            Suffix::ROUTE => { "Rt" }
            Suffix::WALK => { "Walk" }
            Suffix::PLACE => { "Pl" }
            Suffix::TERRACE => { "Terr" }
            Suffix::TRAIL => { "Tr" }
            Suffix::NoEnding => { "" }
            Suffix::CUSTOM(string) => { string }
        };
    }

    fn from(name: &str) -> Suffix {
        let string = name.to_ascii_lowercase();
        return match string.as_ref() {
            "street" => Suffix::STREET,
            "road" => Suffix::ROAD,
            "drive" => Suffix::DRIVE,
            "boulevard" => Suffix::BOULEVARD,
            "lane" => Suffix::LANE,
            "avenue" => Suffix::AVENUE,
            "circle" => Suffix::CIRCLE,
            "highway" => Suffix::HIGHWAY,
            "freeway" => Suffix::FREEWAY,
            "expressway" => Suffix::EXPRESSWAY,
            "tollway" => Suffix::TOLLWAY,
            "parkway" => Suffix::PARKWAY,
            "speedway" => Suffix::SPEEDWAY,
            "way" => Suffix::WAY,
            "route" => Suffix::ROUTE,
            "walk" => Suffix::WALK,
            "place" => Suffix::PLACE,
            "terrace" => Suffix::TERRACE,
            "trail" => Suffix::TRAIL,

            "st" => Suffix::STREET,
            "rd" => Suffix::ROAD,
            "dr" => Suffix::DRIVE,
            "blvd" => Suffix::BOULEVARD,
            "ln" => Suffix::LANE,
            "ave" => Suffix::AVENUE,
            "cir" => Suffix::CIRCLE,
            "hwy" => Suffix::HIGHWAY,
            "fwy" => Suffix::FREEWAY,
            "expy" => Suffix::EXPRESSWAY,
            "tlwy" => Suffix::TOLLWAY,
            "pkwy" => Suffix::PARKWAY,
            "spdwy" => Suffix::SPEEDWAY,
            //"way" => Suffix::WAY, // left in for visual symmetry, but not needed because they're the same as their long counterparts.
            "rt" => Suffix::ROUTE,
            //"walk" => Suffix::WALK,
            "pl" => Suffix::PLACE,
            "terr" => Suffix::TERRACE,
            "tr" => Suffix::TRAIL,

            "" => Suffix::NoEnding,
            _ => {Suffix::CUSTOM(string)}
        };
    }
}

enum Apartment {
    //The apartment/unit number might not be an integer, but if it is, we don't want to store a string
    // (e.g. 'Unit 12E' stores a string but 'Unit 12' doesn't)
    Apartment(u32),
    Unit(u32),
    Room(u32),
    ApartmentName(String),
    UnitName(String),
    RoomName(String),
    None,
}

impl Apartment {
    fn short_name(&self) -> String {
        return match self {
            Apartment::Apartment(num) => {
                let mut string: String = String::from("Apt. ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::Unit(num) => {
                let mut string = String::from("Unit ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::Room(num) => {
                let mut string = String::from("Rm. ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::ApartmentName(name) => {
                let mut string: String = String::from("Apt. ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::UnitName(name) => {
                let mut string = String::from("Unit ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::RoomName(name) => {
                let mut string = String::from("Rm. ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::None => NULL
        };
    }

    fn long_name(&self) -> String {
        return match self {
            Apartment::Apartment(num) => {
                let mut string: String = String::from("Apartment ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::Unit(num) => {
                let mut string = String::from("Unit ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::Room(num) => {
                let mut string = String::from("Room ");
                string.push_str(num.to_string().as_ref());
                string
            }
            Apartment::ApartmentName(name) => {
                let mut string: String = String::from("Apartment ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::UnitName(name) => {
                let mut string = String::from("Unit ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::RoomName(name) => {
                let mut string = String::from("Room ");
                string.push_str(name.as_ref());
                string
            }
            Apartment::None => NULL
        };
    }
}

enum State {
    Alabama,
    Alaska,
    Arizona,
    Arkansas,
    California,
    Colorado,
    Connecticut,
    Delaware,
    Florida,
    Georgia,
    Hawaii,
    Idaho,
    Illinois,
    Indiana,
    Iowa,
    Kansas,
    Kentucky,
    Louisiana,
    Maine,
    Maryland,
    Massachusetts,
    Michigan,
    Minnesota,
    Mississippi,
    Missouri,
    Montana,
    Nebraska,
    Nevada,
    NewHampshire,
    NewJersey,
    NewMexico,
    NewYork,
    NorthCarolina,
    NorthDakota,
    Ohio,
    Oklahoma,
    Oregon,
    Pennsylvania,
    RhodeIsland,
    SouthCarolina,
    SouthDakota,
    Tennessee,
    Texas,
    Utah,
    Vermont,
    Virginia,
    Washington,
    WestVirginia,
    Wisconsin,
    Wyoming,
    None,
}

//Rust is dumb and doesn't let us iterate through enums so we do this.
const STATES: [State; 50] = [
    Alabama,
    Alaska,
    Arizona,
    Arkansas,
    California,
    Colorado,
    Connecticut,
    Delaware,
    Florida,
    Georgia,
    Hawaii,
    Idaho,
    Illinois,
    Indiana,
    Iowa,
    Kansas,
    Kentucky,
    Louisiana,
    Maine,
    Maryland,
    Massachusetts,
    Michigan,
    Minnesota,
    Mississippi,
    Missouri,
    Montana,
    Nebraska,
    Nevada,
    NewHampshire,
    NewJersey,
    NewMexico,
    NewYork,
    NorthCarolina,
    NorthDakota,
    Ohio,
    Oklahoma,
    Oregon,
    Pennsylvania,
    RhodeIsland,
    SouthCarolina,
    SouthDakota,
    Tennessee,
    Texas,
    Utah,
    Vermont,
    Virginia,
    Washington,
    WestVirginia,
    Wisconsin,
    Wyoming];

impl State {
    fn short_name(&self) -> &str {
        return match self {
            State::Alabama => { "AL" }
            State::Alaska => { "AK" }
            State::Arizona => { "AZ" }
            State::Arkansas => { "AR" }
            State::California => { "CA" }
            State::Colorado => { "CO" }
            State::Connecticut => { "CT" }
            State::Delaware => { "DE" }
            State::Florida => { "FL" }
            State::Georgia => { "GA" }
            State::Hawaii => { "HI" }
            State::Idaho => { "ID" }
            State::Illinois => { "IL" }
            State::Indiana => { "IN" }
            State::Iowa => { "IA" }
            State::Kansas => { "KS" }
            State::Kentucky => { "KY" }
            State::Louisiana => { "LA" }
            State::Maine => { "ME" }
            State::Maryland => { "MD" }
            State::Massachusetts => { "MA" }
            State::Michigan => { "MI" }
            State::Minnesota => { "MN" }
            State::Mississippi => { "MS" }
            State::Missouri => { "MO" }
            State::Montana => { "MT" }
            State::Nebraska => { "NE" }
            State::Nevada => { "NV" }
            State::NewHampshire => { "NH" }
            State::NewJersey => { "NJ" }
            State::NewMexico => { "NM" }
            State::NewYork => { "NY" }
            State::NorthCarolina => { "NC" }
            State::NorthDakota => { "ND" }
            State::Ohio => { "OH" }
            State::Oklahoma => { "OK" }
            State::Oregon => { "OR" }
            State::Pennsylvania => { "PA" }
            State::RhodeIsland => { "RI" }
            State::SouthCarolina => { "SC" }
            State::SouthDakota => { "SD" }
            State::Tennessee => { "TN" }
            State::Texas => { "TX" }
            State::Utah => { "UT" }
            State::Vermont => { "VT" }
            State::Virginia => { "VA" }
            State::Washington => { "WA" }
            State::WestVirginia => { "WV" }
            State::Wisconsin => { "WI" }
            State::Wyoming => { "WY" }
            State::None => { "" }
        };
    }

    fn long_name(&self) -> &str {
        return match self {
            State::Alabama => { "Alabama" }
            State::Alaska => { "Alaska" }
            State::Arizona => { "Arizona" }
            State::Arkansas => { "Arkansas" }
            State::California => { "California" }
            State::Colorado => { "Colorado" }
            State::Connecticut => { "Connecticut" }
            State::Delaware => { "Delaware" }
            State::Florida => { "Florida" }
            State::Georgia => { "Georgia" }
            State::Hawaii => { "Hawaii" }
            State::Idaho => { "Idaho" }
            State::Illinois => { "Illinois" }
            State::Indiana => { "Indiana" }
            State::Iowa => { "Iowa" }
            State::Kansas => { "Kansas" }
            State::Kentucky => { "Kentucky" }
            State::Louisiana => { "Louisiana" }
            State::Maine => { "Maine" }
            State::Maryland => { "Maryland" }
            State::Massachusetts => { "Massachusetts" }
            State::Michigan => { "Michigan" }
            State::Minnesota => { "Minnesota" }
            State::Mississippi => { "Mississippi" }
            State::Missouri => { "Missouri" }
            State::Montana => { "Montana" }
            State::Nebraska => { "Nebraska" }
            State::Nevada => { "Nevada" }
            State::NewHampshire => { "New Hampshire" }
            State::NewJersey => { "New Jersey" }
            State::NewMexico => { "New Mexico" }
            State::NewYork => { "New York" }
            State::NorthCarolina => { "North Carolina" }
            State::NorthDakota => { "North Dakota" }
            State::Ohio => { "Ohio" }
            State::Oklahoma => { "Oklahoma" }
            State::Oregon => { "Oregon" }
            State::Pennsylvania => { "Pennsylvania" }
            State::RhodeIsland => { "Rhode Island" }
            State::SouthCarolina => { "South Carolina" }
            State::SouthDakota => { "South Dakota" }
            State::Tennessee => { "Tennessee" }
            State::Texas => { "Texas" }
            State::Utah => { "Utah" }
            State::Vermont => { "Vermont" }
            State::Virginia => { "Virginia" }
            State::Washington => { "Washington" }
            State::WestVirginia => { "West Virginia" }
            State::Wisconsin => { "Wisconsin" }
            State::Wyoming => { "Wyoming" }
            State::None => { "" }
        };
    }

    //Rust is dumb and doesn't just take an enum constant as it is, so we have this.
    pub(crate) fn dereference(me: &State) -> State {
        return match me {
            Alabama => { Alabama }
            Alaska => { Alaska }
            Arizona => { Arizona }
            Arkansas => { Arkansas }
            California => { California }
            Colorado => { Colorado }
            Connecticut => { Connecticut }
            Delaware => { Delaware }
            Florida => { Florida }
            Georgia => { Georgia }
            Hawaii => { Hawaii }
            Idaho => { Idaho }
            Illinois => { Illinois }
            Indiana => { Indiana }
            Iowa => { Iowa }
            Kansas => { Kansas }
            Kentucky => { Kentucky }
            Louisiana => { Louisiana }
            Maine => { Maine }
            Maryland => { Maryland }
            Massachusetts => { Massachusetts }
            Michigan => { Michigan }
            Minnesota => { Minnesota }
            Mississippi => { Mississippi }
            Missouri => { Missouri }
            Montana => { Montana }
            Nebraska => { Nebraska }
            Nevada => { Nevada }
            NewHampshire => { NewHampshire }
            NewJersey => { NewJersey }
            NewMexico => { NewMexico }
            NewYork => { NewYork }
            NorthCarolina => { NorthCarolina }
            NorthDakota => { NorthDakota }
            Ohio => { Ohio }
            Oklahoma => { Oklahoma }
            Oregon => { Oregon }
            Pennsylvania => { Pennsylvania }
            RhodeIsland => { RhodeIsland }
            SouthCarolina => { SouthCarolina }
            SouthDakota => { SouthDakota }
            Tennessee => { Tennessee }
            Texas => { Texas }
            Utah => { Utah }
            Vermont => { Vermont }
            Virginia => { Virginia }
            Washington => { Washington }
            WestVirginia => { WestVirginia }
            Wisconsin => { Wisconsin }
            Wyoming => { Wyoming }
            None => { None }
        };
    }

    fn from(name: &str) -> State {
        for i in 0..STATES.len() {
            let test = &STATES[i];
            if name.eq_ignore_ascii_case(test.short_name()) || name.eq_ignore_ascii_case(test.long_name()) {
                return State::dereference(test);
            }
        }
        return State::None;
    }
}


enum Country {
    USA,
    Other(String),
}

impl Country {
    fn short_name(&self) -> &str {
        return match self {
            Country::USA => { "USA" }
            Country::Other(name) => { name }
        };
    }

    fn long_name(&self) -> &str {
        return match self {
            Country::USA => { "United States of America" }
            Country::Other(name) => { name }
        };
    }

    fn from(name: &str) -> Country {
        match name.to_ascii_lowercase().as_ref() {
            "usa" => { USA }
            "united states" => { USA }
            "united states of america" => { USA }
            _ => { Other(name.to_string()) }
        }
    }
}

// TESTS

#[cfg(test)]
mod tests {
    use crate::identifiers::address::{Address, Direction, Street, Suffix, Apartment, State, Country, AddressFormat, Include};
    use std::convert::TryFrom;

    #[test]
    fn test_basic_address() {
        let address = Address {
            number: 2317,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Speedway")),
            suffix: Suffix::STREET,
            apartment: Apartment::None,
            city: "Austin".to_string(),
            state: State::Texas,
            zip: 78705,
            country: Country::USA,
        };
        assert_eq!(address.to_short_string(), "2317 Speedway St Austin, TX 78705, USA");
    }

    #[test]
    fn test_short_all_fields() {
        let address = Address {
            number: 1234,
            direction: Direction::Northeast,
            street: Street::NoPrefix(String::from("Main")),
            suffix: Suffix::STREET,
            apartment: Apartment::ApartmentName("17E".to_string()),
            city: "San Francisco".to_string(),
            state: State::California,
            zip: 98765,
            country: Country::USA,
        };
        assert_eq!(address.to_short_string(), "1234 NE Main St Apt. 17E San Francisco, CA 98765, USA");
    }

    #[test]
    fn test_long_all_fields() {
        let address = Address {
            number: 1234,
            direction: Direction::South,
            street: Street::NoPrefix(String::from("Park")),
            suffix: Suffix::PLACE,
            apartment: Apartment::Unit(789),
            city: "New York".to_string(),
            state: State::NewYork,
            zip: 13579,
            country: Country::USA,
        };
        assert_eq!(address.to_long_string(), "1234 South Park Place Unit 789 New York, New York 13579, United States of America");
    }

    #[test]
    fn test_custom_format_1() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: "Chicago".to_string(),
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::Short,
            direction: Include::Long, //won't show up because it's set as None above
            street: Include::Short,
            suffix: Include::None,
            apartment: Include::Short,
            city: Include::Short,
            state: Include::Long,
            zip: Include::None,
            country: Include::None,
        };
        assert_eq!(address.to_string_explicit(format), "5 Robin Hood Rm. 181 Chicago, Illinois");
    }

    #[test]
    fn test_custom_format_2() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: "Chicago".to_string(),
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::None,
            direction: Include::None,
            street: Include::None,
            suffix: Include::Long,
            apartment: Include::Long,
            city: Include::None,
            state: Include::Short,
            zip: Include::Short,
            country: Include::Short,
        };
        //This address looks really weird but we needed to test the None value in every field.
        assert_eq!(address.to_string_explicit(format), "Lane Room 181, IL 45678, USA");
    }

    #[test]
    fn test_custom_format_3() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: "Chicago".to_string(),
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::None,
            direction: Include::None,
            street: Include::None,
            suffix: Include::None,
            apartment: Include::None,
            city: Include::None,
            state: Include::Long,
            zip: Include::None,
            country: Include::Short,
        };
        assert_eq!(address.to_string_explicit(format), "Illinois, USA");
    }

    #[test]
    fn test_custom_format_4() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: "Chicago".to_string(),
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::Short,
            direction: Include::None,
            street: Include::None,
            suffix: Include::None,
            apartment: Include::None,
            city: Include::None,
            state: Include::Long,
            zip: Include::None,
            country: Include::Short,
        };
        assert_eq!(address.to_string_explicit(format), "5, Illinois, USA");
    }

    #[test]
    fn test_custom_format_5() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: " ".to_string(), // !!!
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::None,
            direction: Include::None,
            street: Include::None,
            suffix: Include::None,
            apartment: Include::None,
            city: Include::Short,
            state: Include::Long,
            zip: Include::None,
            country: Include::None,
        };
        address.to_string_explicit(format); //make sure we don't crash on some weird inputs
    }

    #[test]
    fn test_custom_format_6() {
        let address = Address {
            number: 5,
            direction: Direction::None,
            street: Street::NoPrefix(String::from("Robin Hood")),
            suffix: Suffix::LANE,
            apartment: Apartment::Room(181),
            city: "Chicago".to_string(),
            state: State::Illinois,
            zip: 45678,
            country: Country::USA,
        };
        let format = AddressFormat {
            number: Include::None,
            direction: Include::None,
            street: Include::None,
            suffix: Include::None,
            apartment: Include::None,
            city: Include::None,
            state: Include::None,
            zip: Include::None,
            country: Include::None,
        };
        assert_eq!(address.to_string_explicit(format), "");
    }
}
