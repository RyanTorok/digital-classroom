use clap::{App, ArgMatches, Arg, Values};
use std::io::{empty, Write};
use crate::command_executor::command_output_target::CommandOutputTarget::Console;
use crate::utils::err_handle::wrap_err_spec;
use crate::command_executor::executor;
use std::io;

#[cfg(feature = "desktop")]
use crate::gui_socket::gui_server::{listen_for_establish, start_listener};
use crate::command_executor::encoding::decode_plaintext_command;

//CONSTANTS

const APP_NAME: &str = "Digital Classroom";
const VERSION: &str = "0.1";
const AUTHOR: &str = "Ryan Torok <ryan.torok@utexas.edu>";
const DESCRIPTION: &str = "An advanced LMS for the 21st century";


//ARGUMENTS

const DEBUG_ARG: &str = "debug";
const DEBUG_ARG_SHORT: &str = "d";
const DEBUG_ARG_DESCRIPTION: &str = "Prints all debug information to the terminal.";

const NO_ESTAB_ARG: &str = "no-socket";
const NO_ESTAB_ARG_SHORT: &str = "n";
const NO_ESTAB_ARG_DESCRIPTION: &str = "Don't listen on /usr/share/digital-classroom/sockets/gui.sock for new GUI connections.";

const SERVER_ARG: &str = "server";
const SERVER_ARG_SHORT: &str = "S";
const SERVER_ARG_DESCRIPTION: &str = "Run in domain server mode. Must be running as 'digitalclassroom' user to run as server";

const SCRIPT_ARG: &str = "script";
const SCRIPT_ARG_SHORT: &str = "s";
const SCRIPT_ARG_DESCRIPTION: &str = "Executes a sequence of plaintext commands from the given script file";

const TERMINAL_ARG: &str = "terminal";
const TERMINAL_ARG_SHORT: &str = "t";
const TERMINAL_ARG_DESCRIPTION: &str = "Displays a terminal to execute plaintext commands";


pub(crate) struct ConsoleOptions {
    pub debug: bool,
    pub no_estab: bool,
    pub server: bool,
    pub script_files: Vec<String>,
    pub terminal: bool
}

pub(crate) fn parse_arguments() -> (ConsoleOptions, String) {
    let mut debug = false;
    let mut no_estab = false;
    let mut server = false;
    let mut script_files = vec![];
    let mut terminal = false;

    let matches = get_arguments();

    if matches.is_present(DEBUG_ARG) {
        debug = true;
    }
    let scripts = matches.values_of(SCRIPT_ARG);
    if scripts.is_some() {
        let scripts: Values = scripts.unwrap();
        for script in scripts {
            script_files.push(script.to_string());
        }
    }
    if matches.is_present(NO_ESTAB_ARG) {
        no_estab = true;
    }
    if matches.is_present(SERVER_ARG) {
        server = true;
    }
    if matches.is_present(TERMINAL_ARG) {
        terminal = true;
    }
    return (ConsoleOptions{debug, no_estab, server, script_files, terminal}, matches.usage().to_string());
}

fn get_arguments<'a>() -> ArgMatches<'a> {
    App::new(APP_NAME)
        .version(VERSION)
        .author(AUTHOR)
        .about(DESCRIPTION)

        .arg(Arg::with_name(DEBUG_ARG)
            .short(DEBUG_ARG_SHORT)
            .help(DEBUG_ARG_DESCRIPTION))

        .arg(Arg::with_name(NO_ESTAB_ARG)
            .short(NO_ESTAB_ARG_SHORT)
            .help(NO_ESTAB_ARG_DESCRIPTION))

        .arg(Arg::with_name(SERVER_ARG)
            .short(SERVER_ARG_SHORT)
            .help(SERVER_ARG_DESCRIPTION))

        .arg(Arg::with_name(SCRIPT_ARG)
            .short(SCRIPT_ARG_SHORT)
            .help(SCRIPT_ARG_DESCRIPTION)
            .value_name("FILE")
            .multiple(true))

        .arg(Arg::with_name(TERMINAL_ARG)
            .short(TERMINAL_ARG_SHORT)
            .help(TERMINAL_ARG_DESCRIPTION))

        .get_matches()
}

pub(crate) fn handle_arguments(properties: ConsoleOptions) {
    let ref_properties = &properties;

    //Execute the script file if the user specified one
    for script_file_name in ref_properties.script_files.as_slice() {
    }

    let ref_properties = &properties;

    //if the user added the -t flag, start the CLI.
    if ref_properties.terminal {
        println!("Welcome to Digital Classroom, Version {}", VERSION);
        println!("Connect any compliant GUI, or type commands directly to debug.");
        let reader = io::stdin();
        loop {
            print!(">>> ");
            let _result = io::stdout().flush();
            let mut buf = String::new();
            let _result = reader.read_line(&mut buf);
            match _result {
                Ok(_size) => {
                    let command = decode_plaintext_command(&buf);

                    let cmd_result = command.execute();
                    /*match cmd_result {
                        //Show command stats if we're in debug mode 
                        Ok(response) => {
                            if cfg!(feature = "global_debug") {
                                println!("\n*************************************************");
                                println!("* Command exited successfully with exit code {:2} *", &response.exit_code);
                                println!("* Execution time: {:.04} sec                    *", ((&response).time_millis as f64) / 1000.0 );
                                println!("*************************************************\n");
                            }
                        },
                        Err(err) => {
                            println!("* Command failed with error:\n {} *", err.to_string());
                        }
                    }*/
                },
                Err(err) => {
                    println!("An error occurred reading the input command:\n{}", err);
                }
            }
        }
    }

    //initialize the ESTAB listener unless the user specified the --no-estab argument.
    #[cfg(feature = "desktop")]
    if !ref_properties.no_estab {
        //start_listener() should never return unless there's an error.
        //let error = listen_for_establish();
        let error = start_listener();
        if error.is_ok() {
            panic!("Fatal error: listen_for_establish() should never return without an error code.");
        }
        let backtrace = wrap_err_spec("Fatal error: Failed to start ESTAB listener.", error.unwrap_err());
        panic!(backtrace.to_string());
    }
}