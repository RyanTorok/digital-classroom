use crate::utils::err_handle::wrap_std_err;
use uuid::*;
use std::net::TcpStream;
use std::time::Duration;
use std::io::{Write, Read, BufReader, BufWriter, BufRead};
use crate::utils::string_utils::{socket_escape, socket_unescape};
use crate::utils::err_handle::{ErrSpec, leaf_err, unwrap_or_wrap_err, wrap_err, unwrap_or_leaf_err, unwrap_or_wrap_err_msg, not_implemented, leaf_err_spec, ResultBacktrace};
use crate::network::domain_server_commands::DomainServerCommand;
use crate::network::response::ServerResponse;

const HOST: &str = "api.digitalclassroom.org";
const READ_TIMEOUT: Duration = Duration::from_secs(5);
const WRITE_TIMEOUT: Duration = Duration::from_secs(5);

struct ServerSession {
    server_id: Uuid,
    auth_key: Uuid,
    reader: BufReader<TcpStream>,
    writer: BufWriter<TcpStream>,
}

impl ServerSession {

    pub(crate) fn new() -> ResultBacktrace<ServerSession> {
        let socket= TcpStream::connect(HOST);
        match socket {
            Ok(socket) => {
                match socket.set_read_timeout(Option::from(READ_TIMEOUT)) {
                    Ok(()) => {},
                    Err(err) => {return wrap_std_err("Could not set socket read timeout", err);}
                }
                match socket.set_write_timeout(Option::from(WRITE_TIMEOUT)) {
                    Ok(()) => {},
                    Err(err) => {return wrap_std_err("Could not set socket write timeout", err);}
                }
                let clone = socket.try_clone();
                if clone.is_err() {
                    return leaf_err("Could not clone server session socket for writing.");
                }
                Ok(ServerSession { server_id: Uuid::nil(), auth_key: Uuid::nil(), reader: BufReader::new(socket), writer: BufWriter::new(clone.unwrap()) })
            }
            Err(err)=> {
                wrap_std_err(format!("Host '{}' refused to connect.", HOST).as_ref(), err)
            }
        }
    }

    #[inline]
    fn send_string(&mut self, message: String) -> ResultBacktrace<usize> {
        self.send_bytes(socket_escape(message.as_ref()).as_bytes())
    }

    #[inline]
    fn send_bytes(&mut self, message: &[u8]) -> ResultBacktrace<usize>{
        unwrap_or_wrap_err_msg(self.writer.write(message), "Server session socket write failed.")
    }

    fn receive_bytes(&mut self, num_bytes: usize) -> ResultBacktrace<Vec<u8>> {
        let mut buf: Vec<u8> = vec![0; num_bytes];
        let result = self.reader.read(buf.as_mut_slice());
        if result.is_err() {
            return wrap_err("Server session socket read failed.", leaf_err_spec(result.unwrap_err()));
        } else {
            //assert_eq!(buf.len(), result.unwrap());
            return Ok(buf);
        }
    }

    fn receive_string(&mut self) -> ResultBacktrace<String> {
        let mut buf = String::new();
        let result = self.reader.read_line(&mut buf);
        if result.is_err() {
            return wrap_err("Server session socket read failed.", leaf_err_spec(result.unwrap_err()));
        } else {
            //assert_eq!(buf.len(), result.unwrap());
            return Ok(socket_unescape(buf.as_ref()));
        }
    }
}

pub(crate) fn domain_server_send_cmd(command: DomainServerCommand) -> ResultBacktrace<ServerResponse> {
    not_implemented()
}
