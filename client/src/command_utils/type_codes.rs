// FILE: command_utils/type_codes.rs
//
// DESCRIPTION: This file stores the type codes used for encoding arguments and return values
// in commands executed in socket mode.
//
// CHANGE LOG:
// 25 Oct 2020 -- Initial generation

pub(crate) const ENCODING_U8: u8 = 0x0;
pub(crate) const ENCODING_U16: u8 = 0x1;
pub(crate) const ENCODING_U32: u8 = 0x2;
pub(crate) const ENCODING_U64: u8 = 0x3;
pub(crate) const ENCODING_U128: u8 = 0x4;
pub(crate) const ENCODING_I8: u8 = 0x5;
pub(crate) const ENCODING_I16: u8 = 0x6;
pub(crate) const ENCODING_I32: u8 = 0x7;
pub(crate) const ENCODING_I64: u8 = 0x8;
pub(crate) const ENCODING_I128: u8 = 0x9;
pub(crate) const ENCODING_F32: u8 = 0xA;
pub(crate) const ENCODING_F64: u8 = 0xB;
pub(crate) const ENCODING_UUID: u8 = 0xC;
pub(crate) const ENCODING_STRING: u8 = 0xD;
pub(crate) const ENCODING_BINARY: u8 = 0xE;
