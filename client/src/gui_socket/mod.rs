use crate::ipc::ConnectionMode;

#[cfg(feature="backend")]
pub(crate) mod gui_server;

#[cfg(feature="libdcl")]
pub(crate) mod gui_client;

#[cfg(all(feature = "desktop", target_family = "unix"))]
const SOCKET_PATH: &str = "/usr/share/digital-classroom/sockets/gui.sock";

#[cfg(target_family = "unix")]
const ESTAB_PIPE_PATH: &str = "/usr/share/digital-classroom/pipes/estab";
#[cfg(target_family = "unix")]
const ESTAB_PIPE_DIR: &str = "/usr/share/digital-classroom/pipes/";


#[cfg(all(feature = "desktop", target_family = "unix"))]
const CONNECTION_PARAMS: ConnectionMode = ConnectionMode::UnixSocket(SOCKET_PATH);
#[cfg(all(feature = "desktop", target_family = "windows"))]
const CONNECTION_PARAMS: ConnectionMode = ConnectionMode::WindowsNamedPipe(SOCKET_PATH, SOCKET_FILE_NAME);
#[cfg(feature = "web")]
const CONNECTION_PARAMS: ConnectionMode = ConnectionMode::WebSocket("localhost", 8080);