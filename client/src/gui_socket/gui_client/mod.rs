use crate::ipc::{ConnectionMode, IPCConnection};
use crate::utils::err_handle::{ResultBacktrace, leaf_err, not_implemented, unwrap_or_wrap_std_err};
#[cfg(not(feature = "web"))]
use crate::gui_socket::SOCKET_PATH;
#[cfg(all(not(feature = "web"), target_family = "unix"))]
use std::os::unix::net::UnixStream;
use std::sync::{Mutex, Arc, RwLock, Once, RwLockReadGuard, MutexGuard, TryLockError, Condvar, Weak};
use std::ops::{Deref, DerefMut};
use std::thread::spawn;
use crate::utils::sync::blocking_queue::{LinkedBlockingQueue, BlockingQueue, Timeout};
use crate::command_utils::params::Argument;
use std::time::Duration;
#[cfg(feature = "web")]
use crate::ipc::wasm_stream::WasmClient;

const TIMEOUT: Duration = Duration::from_millis(50);

static THREAD_POOL_INIT: Once = Once::new();

lazy_static! {
    pub(crate) static ref THREAD_POOL_SIZE: RwLock<u16> = RwLock::new(0);
    // We don't need a lock on the outer list because the RwLock around THREAD_POOL_SIZE prevents more than one
    // call to start_backend_socket() at a time.
    pub(crate) static ref THREAD_KILL_ACKS: RwLock<Vec<Arc<(Mutex<bool>, Condvar)>>> = RwLock::new(vec![]);
    //static ref QUEUE: LinkedBlockingQueue<*const dyn Fn(&mut IPCConnection) + Send + Sync> = LinkedBlockingQueue::new();
    static ref QUEUE: LinkedBlockingQueue<usize> = LinkedBlockingQueue::new();
}

pub(crate) fn connect() -> ResultBacktrace<IPCConnection> {
    #[cfg(all(feature = "desktop", target_family = "unix"))] {
        unwrap_or_wrap_std_err(UnixStream::connect(SOCKET_PATH),
                               format!("Could not create socket to {}", SOCKET_PATH).as_str())
    } #[cfg(all(feature = "desktop", target_family = "windows"))] {
        return not_implemented();
    } #[cfg(feature = "web")] {
        WasmClient::new(8080, "digitalclassroom.org/client", 443)
    }
}

#[inline]
pub(crate) fn enqueue_command(f: usize) {
    QUEUE.push(f as usize);
}

#[inline]
pub(crate) fn dequeue_command() -> Result<usize, Timeout> {
    QUEUE.poll_timeout(TIMEOUT)
}

#[inline]
pub(crate) fn empty_queue() -> bool {
    QUEUE.is_empty()
}

#[inline]
pub(crate) fn queue_size() -> usize {
    QUEUE.len()
}

#[inline]
pub(crate) fn no_sockets() -> bool {
    *(THREAD_POOL_SIZE.read().unwrap()) == 0
}
/*pub(crate) fn allocate_connection() -> &'static mut IPCConnection {
    let mut guard: RwLockReadGuard<Vec<Mutex<IPCConnection>>> = CONNECTIONS.read().expect("Fatal error: The lock on \
	the IPC connections list was double-acquired within a single thread.");
    let connections: &Vec<Mutex<IPCConnection>> = guard.deref();
    for conn in connections {
        match conn.try_lock() {
            Ok(mut guard) => {
                return guard.deref_mut();
            },
            Err(e) => {
                // This socket is in use, let's try another
            },
        }
    }
    // Uh oh, all sockets are taken. We
    unimplemented!()
}
*/
