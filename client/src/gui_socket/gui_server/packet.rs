use libc::PIPE_BUF;
/**
    The OwnedPacket and SlicePacket structs represent data packets to be sent and received through
    the GUI socket. We must split messages longer than PIPE_BUF bytes into multiple packets, because
    POSIX only guarantees writes to named pipes up to PIPE_BUF bytes are atomic, otherwise the OS can
    interveave parts of different messages together. 

    The OnwedPacket is intended for reading data from the pipe, since the packet should own the payload 
    data, as there is no other reasonable place to store it.

    The SlicePacket is intened for writing data to the pipe, where the message is already stored in the 
    command response and we don't need to copy it to the packet in order to write the data to the pipe.
*/
pub(crate) struct OwnedPacket {
    pub header: PacketHeader,
    pub bytes: Vec<u8>
}

//Packed because we write a SlicePacket directly to the outbound pipe as raw bytes.
#[repr(C, packed)]
pub(crate) struct SlicePacket<'a> {
    pub header: PackedPacketHeader,
    pub bytes: &'a [u8]
}

pub(crate) const HEADER_SIZE: usize = 20;

/**
 * These structs represent a packet header. The unpacked version is intended for OwnedPacket 
 * so we can access the fields without unsafe {}. The packed version is intended for SlicePacket
 * so we can write the raw data without undefined spacing between the variables.
 */

pub(crate) struct PacketHeader {
    pub message_id: u64,
    pub index: u32,
    pub total: u32,
    pub size: u32,
}

#[repr(C, packed)]
pub(crate) struct PackedPacketHeader {
    pub message_id: u64,
    pub index: u32,
    pub total: u32,
    pub size: u32,
}



impl OwnedPacket {

    pub(crate) fn new(message_id: u64, index: u32, total: u32, size: u32, bytes: Vec<u8>) -> OwnedPacket {
        OwnedPacket {header: PacketHeader{message_id, index, total, size}, bytes}
    }


    /**
     * This function takes a list of packets and reassembles the message
     */
    pub(crate) fn assemble_message(packets: &[OwnedPacket]) -> Vec<u8> {
        let mut message: Vec<u8> = vec![];
        for packet in packets {
            for byte in &packet.bytes {
                message.push(*byte);
            }
        }
        message
    }

    pub(crate) fn as_slice_packet<'a>(&'a self) -> SlicePacket<'a> {
        SlicePacket {header: PackedPacketHeader { message_id: self.header.message_id, index: self.header.index, 
            total: self.header.total, size: self.header.size }, bytes: &self.bytes}    
    }
}

impl <'a> SlicePacket<'a> {

    /**
     * This function takes an arbitrarily long slice of bytes and splits it into packets of 
     * of length <= PIPE_BUF so they can be written concurrently. The packets only live as long
     * as the original message.
     */
    pub(crate) fn make_packets(message_id: u64, bytes: &'a [u8]) -> Vec<SlicePacket<'a>> {
        let mut packets = vec![];
        let payload_per_packet = PIPE_BUF - HEADER_SIZE;
        let packets_count = ((bytes.len() - 1) / payload_per_packet) + 1;
        let mut start_index = 0;
        let mut end_index = payload_per_packet.min(bytes.len());
        // start_index and end_index are defined like in substring(), i.e. if start_index = 0 
        // and end_index = 4000, we take bytes [0, 4000).
        for i in 0..packets_count {
            packets.push(SlicePacket{header: PackedPacketHeader{message_id: message_id, index: i as u32, total: packets_count as u32, 
                size: (end_index - start_index) as u32}, bytes: &bytes[start_index..end_index]});
            start_index += payload_per_packet;
            end_index = (start_index + payload_per_packet).min(bytes.len());
        }
        packets
    }
}