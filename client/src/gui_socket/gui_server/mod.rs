use crate::gui_socket::gui_server::named_pipe::{ESTAB_PREAMBLE_0, ESTAB_PREAMBLE_1, ESTAB_PREAMBLE_2, ESTAB_PREAMBLE_3};
use crate::gui_socket::gui_server::packet::{PacketHeader, SlicePacket};
use crate::gui_socket::gui_server::packet::OwnedPacket;
use crate::gui_socket::gui_server::named_pipe::ReadPipe;
use crate::gui_socket::gui_server::named_pipe::WritePipe;
use crate::utils::err_handle::{wrap_err, leaf_err, ResultBacktrace, wrap_std_err, ErrSpec, merge_mut, unwrap_or_wrap_std_err, leaf_err_spec, wrap_result_or_wrap_err, wrap_or_wrap_err, unwrap_or_wrap_err};

use std::path::Path;
use std::collections::VecDeque;
use std::error::Error;
use std::collections::HashMap;
use std::os::unix::net::{UnixListener, UnixStream};
use crate::command_executor::encoding::{execute_gui_command};
use crate::command_executor::executor::{COMMAND_RESPONSE_HEADER_BYTES};
use crate::command_executor::command_output_target::CommandOutputTarget;
use crate::system::linux_syscalls::{fork, execl};
use std::process::Command;
use std::thread;
use std::io::{BufReader, BufRead, Read, Write};
use crate::command_executor::executor;
use crate::command_executor::encoding::encode_response;
use crate::ipc::{IPCServer, IPCConnection, ConnectionMode, new_listener};
use crate::ipc::unix_socket::UnixSocketListener;
use crate::utils::socket_utils::ShutdownSocket;
use byteorder::{LittleEndian, ReadBytesExt, ByteOrder};
use std::sync::{Arc, Mutex, PoisonError, Weak, MutexGuard};
use std::sync::mpsc::{Sender, RecvError, Receiver, channel};
use std::time::{Duration, Instant};
use rand::{random, RngCore};
use std::borrow::{Borrow, BorrowMut};
use std::ops::{Deref, DerefMut};
use crate::gui_socket::{CONNECTION_PARAMS, ESTAB_PIPE_DIR, ESTAB_PIPE_PATH};
use crate::command_executor::timing::CommandTimingData;
use std::net::Shutdown;
use uuid::Uuid;

pub(crate) mod virtual_socket;
pub(crate) mod virtual_listener;
pub(crate) mod named_pipe;
pub(crate) mod packet;


// Starts the Unix Server Socket to receive requests from programs started by calling attach() below.
// This function should never return unless there is an error.
pub(crate) fn start_listener() -> ResultBacktrace<()> {
    let mut listener = unwrap_or_wrap_err(new_listener(CONNECTION_PARAMS),
                                          "Could not initialize virtual IPC server")?;
    /*thread::spawn(move || {
        loop {
            match input_out.recv() {
                Ok(msg) => {
                    let command = decode_gui_command(msg.data.as_slice());
                    let params = CommandParams { command, output_target:CommandOutputTarget::Socket(output_in) };
                    command.async_execute();
                },
                Err(err) => {
                    eprintln!("ERROR submitting command message to executor: {}", err.to_string())
                },
            }
        }
    });*/
    listener.handle_all(|conn| {
        handle_connection(conn);
    });
    eprintln!("WARNING: Virtual GUI listener unexpectedly terminated.");
    Ok(())
}

fn handle_connection(mut conn: impl Read + Write + ShutdownSocket) {
    /*//queue for decoded commands that we can fill from one thread and remove from another
    let (input_in, input_out): (std::sync::mpsc::Sender<PacketMessage>, std::sync::mpsc::Receiver<PacketMessage>) = channel();
    let weak_conn = Arc::downgrade(&arc_conn);
    thread::spawn(move || {
        println!("Started read thread");
        read_commands_from_socket(weak_conn, input_in);
    });*/
    loop {
        /*match input_out.recv() {
            Ok(incoming_packet) => {*/
                let submit_time = Instant::now();
               // thread::spawn(move || {
                    let start_time = Instant::now();
                    let response = execute_gui_command(&mut conn);
                    if response.1 {
                        // Opcode 0 -- need to close socket
                        //println!("closing socket...");
                        conn.shutdown_socket(Shutdown::Both);
                        break;
                    }
                    let end_time = Instant::now();
                    let calc_time = move || -> CommandTimingData {
                        let time_submit_start = start_time.duration_since(submit_time);
                        let time_start_end = end_time.duration_since(start_time);
                        CommandTimingData::new(submit_time, time_submit_start, time_start_end )
                    };
                    let timing_data = calc_time();
                    let mut outbound_buf = vec![];
                    let structure = encode_response(&mut outbound_buf, response.0, timing_data);
                    let off_types = 4 + structure.0;
                    let off_values = off_types + structure.1;

                    // Message 1: Time and number of return values
                    conn.write(&outbound_buf[0..4]);
                    // Message 2: Return types
                    conn.write(&outbound_buf[4..off_types]);
                    // Message 3: Return values (length for strings)
                    conn.write(&outbound_buf[off_types..off_values]);
                    if structure.2 {
                        // Message 4: String contents -- only sent if one or more return types is a string.
                        // An empty string return value will cause a zero-size message to be sent.
                        conn.write(&outbound_buf[off_values..]);
                    }
                    //println!("write complete.");
                    //set_packet_header(&mut outbound_buf, incoming_packet.id, incoming_packet.length, incoming_packet.index, incoming_packet.total);

                    /*match ref_arc_conn.deref().lock() {
                        Ok(mut guard_conn) => {
                            let conn = guard_conn.deref_mut();
                            conn.write(&outbound_buf);
                        },
                        Err(err) => {
                            eprintln!("ERROR acquiring lock for socket write: {}", err);
                        },
                    }*/
            //    });
            /*}
            Err(err) => {
                let err = wrap_std_err::<PacketMessage, RecvError>("Could not read command from GUI socket", err);
                eprintln!("GUI socket received exception: {}", err.err().unwrap().to_string());
                break;
            }
        }*/
    }
}
/*
struct PacketMessage {
    id: u128,
    length: u32,
    index: u16,
    total: u16,
    collected: u16,
    data: Vec<u8>,
}

///
///   read_commands() enqueues commands read, possibly as multiple packets, from the gui socket,
///   and dispatches complete commands to the command executor.
///
///    Command packet format:
///    Note: all multi-byte quantities should be expressed in little-endian format, and the byte
///    offsets listed here are 0-indexed.
///
///    [bytes 0-15]  Message ID
///    [bytes 16-19] Message payload length, excluding 32-byte header
///    [bytes 20-21] Packet index (0-indexed)
///    [bytes 22-23] Total number of packets in message (1-indexed -- e.g. if a message's packets
///                  are numbered 0-6, this value should be 7).
///    [bytes 24-27] Packet length of each packet in this message besides the final one (excluding 32 byte header)
///                  All packets should contain the same value, even if the final packet is shorter.
///    [bytes 28-31] Reserved for future use
///    [bytes 32-X]  Message payload data
///

const HEADER_SIZE: usize = 32;
const MAX_PARTIAL_COMMANDS: usize = 256;

fn read_commands_from_socket(weak_src: Weak<Mutex<impl Read>>, queue: std::sync::mpsc::Sender<PacketMessage>) {
    let mut partial_commands: HashMap<u128, PacketMessage> = HashMap::new();
    let mut read_packet = move || -> ResultBacktrace<()> {
        let mut header = [0u8; HEADER_SIZE];
        match weak_src.upgrade() {
            None => {
                return leaf_err("Could not read command from socket because the socket has been closed.");
            },
            Some(arc_src) => {
                match arc_src.lock() {
                    Ok(mut guard_src) => {
                        let src = guard_src.deref_mut();
                        let count = unwrap_or_wrap_std_err(src.read(&mut header), "Could not read from virtual GUI socket.")?;
                        //Make sure we read enough bytes
                        println!("received {} header bytes", count);
                        if count != HEADER_SIZE {
                            return leaf_err(format!("Did not read enough bytes for command header from virtual GUI socket: expected {}, received {}", HEADER_SIZE, count).as_str());
                        }
                        let id = (&header[0..16]).read_u128::<LittleEndian>().unwrap();
                        let index = (&header[20..22]).read_u16::<LittleEndian>().unwrap();
                        let total = (&header[22..24]).read_u16::<LittleEndian>().unwrap();
                        let packet_length = (&header[24..28]).read_u32::<LittleEndian>().unwrap();
                        let payload_length = (&header[16..20]).read_u32::<LittleEndian>().unwrap();
                        println!("index = {}", index);
                        let data_offset: usize = (index as u32 * packet_length) as usize;
                        let mut not_in_map = false;
                        let mut partial_command: PacketMessage = match partial_commands.remove(&id) {
                            None => {
                                //first packet we've seen for this message
                                let payload_buf = vec![0u8; payload_length as usize];
                                not_in_map = true;
                                PacketMessage {
                                    id,
                                    index,
                                    total,
                                    length: payload_length,
                                    collected: 0,
                                    data: payload_buf,
                                }
                            }
                            Some(partial_command) => { partial_command }
                        };
                        let write_length: usize = if index == total - 1 { payload_length % packet_length } else { packet_length } as usize;
                        let (start, end) = (data_offset, data_offset + write_length);
                        let count = unwrap_or_wrap_std_err(src.read(&mut partial_command.data[start..end]),
                                                           "Could not read command payload from virtual GUI socket.")?;
                        println!("received {} payload bytes", count);
                        if count != packet_length as usize {
                            return leaf_err(format!("Did not read enough bytes for command payload from virtual GUI socket: expected {}, received {}", payload_length, count).as_str());
                        }
                        //check params
                        if total <= index {
                            return leaf_err(format!("Discarded packet whose 'total' field was <= the packet index. Index = {}, Total = {}", index, total).as_str());
                        }
                        //if we only have one packet, just submit it. If there is more than one. Store it until we get the rest of the packets
                        //We don't have to worry about total == 0. It will be caught above.
                        partial_command.collected += 1;
                        if partial_command.collected == total {
                            queue.send(partial_command);
                        } else {
                            //if we have too many partial commands stored, pick one to evict
                            while partial_commands.len() >= MAX_PARTIAL_COMMANDS {
                                //this is a bit slow, but it's only a 'just in case'.
                                let victim_index: usize;
                                let victim;
                                {
                                    let mut keys = partial_commands.keys();
                                    let victim_index: usize = random_int(0, keys.len());
                                    victim = *(keys.nth(victim_index).expect("Random victim index was out of range"));
                                }
                                partial_commands.remove(&victim);
                            }
                            partial_commands.insert(id, partial_command);
                        }
                        Ok(())

                    },
                    Err(err) => {
                        return leaf_err("Could not acquire lock to read from GUI socket.");
                    },
                }
            },
        }
    };
    loop {
        let result = read_packet();
        println!("reading a packet");
        match result {
            Ok(_) => {},
            Err(err) => {
                eprintln!("ERROR reading packet from GUI socket: {}", err.to_string());
                break;
            },
        };
    };
}

///
/// set_packet_header() sets the header data for outbound write to GUI socket.
/// Important: the data array parameter must pre-allocate HEADER_BYTES bytes for the header data.
/// This constraint allows us to avoid copying the payload data to a new struct, since all implementations
/// of IPCConnection require writes to be of continuous bytes.
///
fn set_packet_header(buf: &mut [u8], message_id: u128, message_length: u32, packet_index: u16, total_packets: u16) {
    let len = buf.len();
    let mut header = &mut buf[0..HEADER_SIZE];
    LittleEndian::write_u128(&mut header[0..16], message_id);
    LittleEndian::write_u32(&mut header[16..20], message_length);
    LittleEndian::write_u16(&mut header[20..22], packet_index);
    LittleEndian::write_u16(&mut header[22..24], total_packets);
    LittleEndian::write_u32(&mut header[24..28], (len - HEADER_SIZE) as u32);
}
*/
// Runs the client program using fork-exec. The logic in start_listener() above will handle the
// socket requests the child program will presumably make.
pub(crate) fn attach(gui: &str) -> Result<(), ErrSpec> {

    // Start the GUI by fork/exec.
    let pid = fork();
    if pid == 0 {
        // Child - execute the parameter program
        let result = execl("/bin/sh", gui);
        if result.is_err() {
            return wrap_err("Failed to execute specified gui by /bin/sh ", result.unwrap_err());
        }
    } else {
        // Parent - move on like nothing even happened.
    }
    //Only the parent should return from this function
    Ok(())
}

/*fn run_gui_socket() -> ResultBacktrace<()> {
    let read_pipe_file = std::fs::File::open(format!("{}{}", PIPE_PATH, OUT));
    let write_pipe_file = std::fs::File::open(format!("{}{}", PIPE_PATH, IN_PIPE_FILENAME));
    match write_pipe_file {
        Ok(file) => {
            let mut write_pipe = WritePipe::new(file);
            loop {
                let mut command = String::new();
                let result = reader.read_line(&mut command);
                if result.is_err() {
                    eprintln!("error: failed to read instruction from gui");
                } else {
                    if command.is_empty() {
                        println!("Received empty string. GUI socket disconnecting.");
                        return Ok(());
                    }
                    println!("Received message: {}", command);
                    let instance_id: u64 = 0;
                    let params = CommandParams::from_gui_socket(command.as_bytes());
                    let response: CommandResponse = executor::execute(params).unwrap(); //TODO handle err
                    let response_raw = &response as *const _ as *const u8;
                    /*
                        This is safe because:
                        We're converting a CommandResponse struct (which is byte-packed) to a raw pointer to return it as bytes.
                        The number of bytes in the struct is COMMAND_RESPONSE_HEADER_BYTES plus the number of bytes in the return message.
                        Since we read exactly that number of bytes starting at the address of the CommandResponse struct, we read the 
                        struct and only the struct as raw bytes and don't introduce a memory issue.
                    */
                    let response_byte_slice = unsafe { std::slice::from_raw_parts(response_raw, COMMAND_RESPONSE_HEADER_BYTES + response.message.as_bytes().len()) };
                    let packets = SlicePacket::make_packets(instance_id, response_byte_slice);
                    let mut write_success: ResultBacktrace<()> = Ok(());
                    let mut i = 1;
                    let num_packets = &packets.len();
                    for packet in packets {
                        write_success = merge_mut(write_success, || {write_pipe.write_packet(&packet)}, 
                        &format!("Could not write packet to outbound socket (Message ID: {}, Packet {} of {})", instance_id, i, num_packets));
                        i += 1;
                    }
                    /*
                        let out_val: CommandResponse = futures::executor::block_on(executor::execute(params));
                        let out_msg = out_val.gui_socket_encode();
                        stream.write(out_msg.as_bytes());
                        */
                }
            }
        },
        Err(err) => {
            wrap_std_err("Could not open outbound pipe", err)
        }
    }
    
}

// Helper functions to set up the sockets and run commands received by the GUI

fn handle_gui_client(mut stream: UnixStream) -> ResultBacktrace<()> {
    let result = stream.try_clone();
    if result.is_err() {
        return leaf_err("Could not clone unix socket for writing.");
    }
    let mut reader = BufReader::new(result.unwrap());
    //stream.write_all("Can you hear me GUI?\n".as_bytes());
    let write_pipe_file = std::fs::File::open(PIPE_PATH);
    match write_pipe_file {
        Ok(file) => {
            let mut write_pipe = WritePipe::new(file);
            loop {
                let mut command = String::new();
                let result = reader.read_line(&mut command);
                if result.is_err() {
                    eprintln!("error: failed to read instruction from gui");
                } else {
                    if command.is_empty() {
                        println!("Received empty string. GUI socket disconnecting.");
                        return Ok(());
                    }
                    println!("Received message: {}", command);
                    let instance_id: u64 = 0;
                    let params = CommandParams::from_gui_socket(command.as_bytes());
                    let response: CommandResponse = executor::async_execute(params);
                    let response_raw = &response as *const _ as *const u8;
                    /*
                        This is safe because:
                        We're converting a CommandResponse struct (which is byte-packed) to a raw pointer to return it as bytes.
                        The number of bytes in the struct is COMMAND_RESPONSE_HEADER_BYTES plus the number of bytes in the return message.
                        Since we read exactly that number of bytes starting at the address of the CommandResponse struct, we read the 
                        struct and only the struct as raw bytes and don't introduce a memory issue.
                    */
                    let response_byte_slice = unsafe { std::slice::from_raw_parts(response_raw, COMMAND_RESPONSE_HEADER_BYTES + response.message.as_bytes().len()) };
                    let packets = SlicePacket::make_packets(instance_id, response_byte_slice);
                    let mut write_success: ResultBacktrace<()> = Ok(());
                    let mut i = 1;
                    let num_packets = &packets.len();
                    for packet in packets {
                        write_success = merge_mut(write_success, || {write_pipe.write_packet(&packet)}, 
                        &format!("Could not write packet to outbound socket (Message ID: {}, Packet {} of {})", instance_id, i, num_packets));
                        i += 1;
                    }
                    /*
                        let out_val: CommandResponse = futures::executor::block_on(executor::execute(params));
                        let out_msg = out_val.gui_socket_encode();
                        stream.write(out_msg.as_bytes());
                        */
                }
            }
        },
        Err(err) => {
            wrap_std_err("Could not open outbound pipe", err)
        }
    }
}

*/

//might not need this function if we can resuse the same socket file for all concurrently running GUIs.
/*fn allocate_socket(socket_id: i32) -> Result<UnixListener, &'static str> {
    //try to create a socket of the form ./temp/sockets/dcl_socket_<pid>
    let path: &str = "./temp/sockets/";
    let dir = Path::new(path);
    if !dir.exists() {
        std::fs::create_dir(dir);
    }
    let path_str = format!("{}dcl_socket_{}", path, pid);
    let path = Path::new::<str>(path_str.as_ref());
    let option = path.to_str();
    if option.is_some() {
        let listener: Result<std::os::unix::net::UnixListener, Error> = UnixListener::bind(option.unwrap());
        if listener.is_err() {
            return Err("An error occurred when creating the socket listener.");
        } else {
            return Ok(listener.unwrap());
        }
    } else {
        return Err("An error occurred when constructing the socket path.")
    }

*/

/**
 * Listen for on the ESTAB pipe for requests to establish a GUI connection.
 * This function should never return unless there is an error establishing the 
 * pipe connection or reading from the pipe.
 */

const MAX_ESTAB_SESSIONS: usize = 64;
const STATUS_CODE_OK: u8 = 0;

enum EstabMessage {
    MessageOne,
    MessageThree,
}

struct EstabSequence {
    progress: u8,
    pipe_in: ReadPipe,
    pipe_out: WritePipe,
    client_nonce_1: [u8; 16],
    client_nonce_2: [u8; 16],
    system_nonce: [u8; 16],
}

const ACK: u8 = 0;

impl EstabSequence {
    /**
     * Construct the packet for message two. Although we usually use SlicePacket
     * for writing, since we have to construct the message from multiple 
     * places, we'll end up with owned byte sequence, and if we wanted to use
     * a slice packet, there would be no reasonable place to store the byte sequence.
     */
    pub(crate) fn make_message_two(&self) -> OwnedPacket {
        let mut packet_bytes = vec![0u8; 37];
        packet_bytes[0] = ACK;
        for i in 0..16 {
            packet_bytes[i + 1] = self.client_nonce_1[i];
        }
        for i in 0..16 {
            packet_bytes[i + 17] = self.system_nonce[i];
        }
        OwnedPacket {
            header: PacketHeader {
                message_id: ESTAB_PREAMBLE_0,
                index: ESTAB_PREAMBLE_1,
                total: ESTAB_PREAMBLE_2,
                size: ESTAB_PREAMBLE_3,
            },
            bytes: packet_bytes,
        }
    }
}

pub(crate) fn listen_for_establish() -> ResultBacktrace<()> {
    //make the pipe file if it doesn't exist yet
    let dir = Path::new(ESTAB_PIPE_DIR);
    if !dir.exists() {
        return leaf_err(&format!("Pipes directory not found: {}", ESTAB_PIPE_DIR));
    }
    let path = Path::new(ESTAB_PIPE_PATH);
    if !path.exists() {
        let create_result = ReadPipe::make_estab_pipe();
        match create_result {
            Ok(()) => {
                //we're ok
            }
            Err(err) => {
                return Err(err);
            }
        }
    }
    let result = std::fs::File::open(ESTAB_PIPE_PATH);
    match result {
        Ok(file) => {
            let mut estab_pipe = ReadPipe::new(file);
            let mut packet_byte_queue = VecDeque::new();
            let mut active_estab_sessions: VecDeque<EstabSequence> = VecDeque::with_capacity(MAX_ESTAB_SESSIONS);
            loop {
                let packet_result = estab_pipe.read_estab_packet(&mut packet_byte_queue);
                match packet_result {
                    Ok(packet) => {
                        let message_type = match packet.bytes[0] {
                            0x00u8 => {
                                EstabMessage::MessageOne
                            }
                            0x01u8 => {
                                EstabMessage::MessageThree
                            }
                            _ => {
                                return leaf_err("read_estab_packet() returned ESTAB packet with invalid message type number.");
                            }
                        };
                        match message_type {
                            EstabMessage::MessageOne => {
                                let nonce_1 = &packet.bytes[1..17];
                                let nonce_2 = &packet.bytes[17..33];
                                let in_filename_length = (packet.bytes[33] as u16 + ((packet.bytes[34] as u16) << 8)) as usize;
                                let out_filename_length = (packet.bytes[35] as u16 + ((packet.bytes[36] as u16) << 8)) as usize;
                                let in_filename = String::from_utf8_lossy(&packet.bytes[36..36 + in_filename_length]);
                                let out_filename = String::from_utf8_lossy(&packet.bytes[36 + in_filename_length..36 + in_filename_length + out_filename_length]);
                                let in_file_r = std::fs::File::open(Path::new(in_filename.as_ref()));
                                match in_file_r {
                                    Ok(pipe_in) => {
                                        let out_file_r = std::fs::File::open(Path::new(out_filename.as_ref()));
                                        match out_file_r {
                                            Ok(pipe_out) => {
                                                //copy nonce bytes to owned arrays
                                                let mut client_nonce_1 = [0u8; 16];
                                                for i in 0..16 {
                                                    client_nonce_1[i] = nonce_1[i];
                                                }
                                                let mut client_nonce_2 = [0u8; 16];
                                                for i in 0..16 {
                                                    client_nonce_2[i] = nonce_2[i];
                                                }
                                                let mut seq = EstabSequence {
                                                    progress: 1,
                                                    pipe_in: ReadPipe::new(pipe_in),
                                                    pipe_out: WritePipe::new(pipe_out),
                                                    client_nonce_1: client_nonce_1,
                                                    client_nonce_2: client_nonce_2,
                                                    system_nonce: [0u8; 16],
                                                };
                                                rand::thread_rng().fill_bytes(&mut seq.system_nonce);
                                                seq.pipe_out.write_packet(&seq.make_message_two().as_slice_packet());
                                                /*
                                                    If we maxed out our number of sessions, abandon the oldest one. 
                                                    This could require a start-over for the owner of that session,
                                                    but if we instead ignored the newest one, a malicious GUI client could 
                                                    institute a denial-of-service attack by filling up all the session
                                                    slots and then never sending message three for any of them.
                                                */
                                                if active_estab_sessions.len() == MAX_ESTAB_SESSIONS {
                                                    active_estab_sessions.pop_front();
                                                }
                                                active_estab_sessions.push_back(seq);
                                            }
                                            Err(err) => {
                                                if cfg!(feature = "global_debug") {
                                                    println!("Could not create ESTAB chain with out_file = {}:\n{}", out_filename, err);
                                                }
                                                continue;
                                            }
                                        }
                                    }
                                    Err(err) => {
                                        if cfg!(feature = "global_debug") {
                                            println!("Could not create ESTAB chain with in_file = {}:\n{}", in_filename, err.to_string());
                                        }
                                        continue;
                                    }
                                }
                            }
                            EstabMessage::MessageThree => {
                                let submitted_client_nonce_2 = &packet.bytes[1..17];
                                let submitted_system_nonce = &packet.bytes[17..33];
                                let status_code = packet.bytes[33];
                                //check nonces
                                for i in 0..active_estab_sessions.len() {
                                    if byte_slice_equals(&active_estab_sessions[i].client_nonce_2, submitted_client_nonce_2)
                                        && byte_slice_equals(&active_estab_sessions[i].system_nonce, submitted_system_nonce) {
                                        match status_code {
                                            STATUS_CODE_OK => {
                                                //remove the matching session from the active sessions list
                                                active_estab_sessions.remove(i);

                                                //start the GUI command listener on the provided in/out pipes
                                            }
                                            _ => {
                                                //invalid status code, ignore.
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Err(err) => {
                        return wrap_err("Received an error reading packets from ESTAB pipe.", err);
                    }
                };
            }
        }
        Err(err) => {
            wrap_std_err("Could not set up listener on estab pipe.", err)
        }
    }
}

fn byte_slice_equals(b1: &[u8], b2: &[u8]) -> bool {
    if b1.len() != b2.len() {
        return false;
    }
    for i in 0..b1.len() {
        if b1[i] != b2[i] {
            return false;
        }
    }
    true
}

const MAX_CONCURRENT_COMMANDS_PER_SESSION: usize = 256;

struct PipeGuiSession {
    in_pipe: ReadPipe,
    out_pipe: WritePipe,
}

struct PartialCommand {
    pub packets: Vec<OwnedPacket>
}

/**
 * This function performs the logic for reading commands and writing command responses
 * to an established pipe pair.
 */
fn pipe_gui_session(mut session: PipeGuiSession) {
    let mut partials: HashMap<u64, PartialCommand> = HashMap::with_capacity(MAX_CONCURRENT_COMMANDS_PER_SESSION);
    let out_pipe = session.out_pipe;
    loop {
        match out_pipe.try_clone() {
            Ok(out_pipe) => {
                let result_packet = session.in_pipe.read_packet();
                match result_packet {
                    Ok(packet) => {
                        if packet.header.index > 0 {
                            let opt_partial = partials.get_mut(&packet.header.message_id);
                            match opt_partial {
                                Some(partial) => {
                                    let last = packet.header.index == packet.header.total - 1;
                                    partial.packets.push(packet);
                                    //If we received the last packet, then we can assemble the message and submit it to the executor
                                    if last {
                                        let message = OwnedPacket::assemble_message(&partial.packets);
                                        //let response = execute_gui_command(message.as_slice());
                                    }
                                }
                                None => {
                                    //ERROR - nonexistent or expired partial
                                }
                            }
                        } else {
                            if partials.get(&packet.header.message_id).is_some() {
                                //ERROR - can't reuse message ID
                                //TODO
                            } else {
                                if packet.header.total > packet.header.index + 1 {
                                    //if the message is more than one packet, add it to the paritals.
                                    partials.insert(packet.header.message_id, PartialCommand { packets: vec![packet] });
                                } else {
                                    //owtherwise just convert it and run
                                    let singleton: [OwnedPacket; 1] = [packet];
                                    let message = OwnedPacket::assemble_message(&singleton);
                                }
                            }
                        }
                    }
                    Err(err) => {}
                }
            }
            Err(err) => {
                //TODO print warning for 'unable to clone outbound pipe'
            }
        }
    }
}