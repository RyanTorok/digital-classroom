use crate::gui_socket::gui_server::ESTAB_PIPE_PATH;
use crate::system::libc_wrappers::mkfifo;
use crate::utils::err_handle::wrap_sys_error;
use std::path::Path;
use std::sync::Mutex;
use crate::utils::err_handle::merge_mut;
use crate::utils::err_handle::merge_std;
use crate::utils::err_handle::merge_std_mut;
use crate::utils::err_handle::not_implemented;
use crate::gui_socket::gui_server::packet::SlicePacket;
use crate::utils::err_handle::wrap_std_err;
use crate::utils::err_handle::wrap_err;
use crate::gui_socket::gui_server::packet::PacketHeader;
use crate::gui_socket::gui_server::packet::OwnedPacket;
use std::sync::MutexGuard;
use std::fs::File;
use crate::utils::err_handle::leaf_err;
use crate::utils::err_handle::ResultBacktrace;
use std::sync::Arc;
use std::io::Read;
use std::io::BufReader;
use crate::utils::err_handle::wrap_result_or_wrap_leaf_err;
use crate::utils::err_handle::mut_wrap_result_or_wrap_leaf_err;
use crate::gui_socket::gui_server::packet::HEADER_SIZE;
use crate::system::linux_syscalls::write;
use std::os::unix::io::AsRawFd;
use std::collections::VecDeque;

pub(crate) const ESTAB_PREAMBLE_0: u64 = 0x0011223344556677;
pub(crate) const ESTAB_PREAMBLE_1: u32 = 0x8899AABB;
pub(crate) const ESTAB_PREAMBLE_2: u32 = 0xCCDDEEFF;
pub(crate) const ESTAB_PREAMBLE_3: u32 = 0x00000000;

#[cfg(target_family = "unix")]
pub(crate) struct ReadPipe {
    file: File
}

#[cfg(target_family = "windows")]
pub(crate) struct ReadPipe {
    //TODO
}

impl ReadPipe {

    pub(crate) fn new(file: File) -> ReadPipe {
        ReadPipe {file}
    }

    /**
     * Returns a single packet read from the 'submit' named pipe. 
     * Blocks until another process writes to the pipe.
     * 
     * Note: this struct is not thread safe. Multiple threads should not attempt to read
     * packets concurrently.
     */
    pub(crate) fn read_packet(&mut self) -> ResultBacktrace<OwnedPacket> {
            let mut buf_header: [u8; HEADER_SIZE] = [0; HEADER_SIZE];
            let read_result = self.file.read_exact(&mut buf_header);
            mut_wrap_result_or_wrap_leaf_err(read_result, |()| -> ResultBacktrace<OwnedPacket> {
                //from_le_bytes() doesn't let us pass a slice because it doesn't want to handle 
                // the case of not enough data, so we do this.
                let mut buf_msg_id: [u8; 8] = [0; 8];
                let mut buf_index: [u8; 4] = [0; 4];
                let mut buf_total: [u8; 4] = [0; 4];
                let mut buf_size: [u8; 4] = [0; 4];
                for i in 0..8 {
                    buf_msg_id[i] = buf_header[i];
                }
                let message_id = u64::from_le_bytes(buf_msg_id);
                drop(buf_msg_id);
                for i in 0..4 {
                    buf_index[i] = buf_header[8 + i];
                }
                let index = u32::from_le_bytes(buf_index);
                drop(buf_index);
                for i in 0..4 {
                    buf_total[i] = buf_header[12 + i];
                }
                let total = u32::from_le_bytes(buf_total);
                drop(buf_total);
                for i in 0..4 {
                    buf_size[i] = buf_header[16 + i];
                }
                let size = u32::from_le_bytes(buf_size);
                drop(buf_size);
                drop(buf_header);
                let mut target = OwnedPacket{header: PacketHeader{message_id, index, total, size}, bytes: vec![0u8; size as usize]};
                let read_payload_result = self.file.read_exact((target).bytes.as_mut_slice());
                // For some reason rust complains about moving the packet if we use the wrap functions.
                // TODO check back on this when the wrap functions don't copy anymore.
                match read_payload_result {
                    Ok(()) => {
                        Ok(target)
                    },
                    Err(err) => {
                        wrap_std_err("An error occurred reading the packet payload.", err)
                    }
                }
            }, "An error occurred reading the packet header")
    }

    /**
     * Construct the ESTAB pipe to accept ESTAB connections
     */
    #[cfg(target_family = "unix")]
    pub(crate) fn make_estab_pipe() -> ResultBacktrace<()> {
        //Unix permissions: pr---w--w-
        match mkfifo(ESTAB_PIPE_PATH, 0o422) {
            0 => Ok(()),
            -1 => wrap_sys_error("Could not create ESTAB pipe."),
            _=> panic!("mkfifo returned value other than 0 or -1.")
        }
    }

    #[cfg(target_family = "windows")]
    pub(crate) fn make_estab_pipe(path: &Path) -> ResultBacktrace<()> {
        //TODO
    }


    /**
     * ESTAB packets are special. The 20 bytes of the packet header contan 
     * 0x00112233445566778899AABBCCDDEEFF00000000, as a preamble, and then the packet 
     * contains the following data, depending on the protocol stage:
     * 
     * Protocol message 1: (client -> service)
     * 0x00 (1 byte) 
     * Client-generated nonce #1 (16 bytes)
     * Client-generated nonce #2 (16 bytes)
     * IN filename length (2 bytes)
     * OUT filename length (2 bytes)
     * IN filename
     * OUT filename
     * 0x00000000 (4 bytes)
     * 
     * Protocol message 2: (service -> client OUT file)
     * Status code (1 byte)
     * Client-generated nonce #1 from message 1 (16 bytes)
     * Service-generated nonce (16 bytes)
     * 0x00000000 (4 bytes)
     * 
     * Protocol message 3:
     * 0x01 (1 byte)
     * Client-generated nonce #2 from message 1 (16 bytes)
     * The system-generated nonce from message 2 (16 bytes)
     * Status code (1 byte)
     * 0x00000000 (4 bytes)
     * 
     * The preamble and suffix practice prevents attackers from disrupting the packet stream 
     * with writes of length other than the packet size. We need to protect against it 
     * because the ESTAB pipe is universally writable, unlike the GUI socket pipes that the 
     * ESTAB packets are intended to set up.
     * 
     * An attacker can attempt to send malicious packets by writing the preamble bytes, but
     * the packets are designed so that the attacker cannot use them to receive output 
     * for another user or convince the system they are a different user.
     * 
     * Writing the preamble bytes also does not allow the attacker to prevent legitimate packets
     * from getting through, as any stream that begins with the above preamble will be treated 
     * as a packet.
     * 
     * The last four bytes of the preamble are zeroes, and this is deliberate. They prevent a
     * side-channel attack where the attacker could carefully lie about the length of its own packet's
     * file names, and thus we might read in the bytes in the next honest packet as part of the attacker's
     * supposed pipe filenames, and try to open the file. Based on the result of this system attempting
     * to open the filename, this could leak the honest user's nonces. To prevent this, the preamble
     * ends with 4 bytes of zeroes, which is the same as the suffix. As we point out below, this allows
     * the system to know to stop reading (since no legitimate file name has null bytes), and thus
     * we won't try to open a file name that depends on the honest user's nonces.
     */
    pub(crate) fn read_estab_packet(&mut self, byte_queue: &mut VecDeque<u8>) -> ResultBacktrace<OwnedPacket> {
            //will return from function and automatically unlock the file when we find a valid ESTAB packet
            loop {
                //read enough bytes to get the preamble
                if byte_queue.len() < HEADER_SIZE {
                    let mut remain = vec![0u8; HEADER_SIZE - byte_queue.len()];
                    let result = self.file.read_exact(remain.as_mut_slice());
                    if result.is_err() {
                        return wrap_std_err("Could not read enough bytes from ESTAB pipe for preamble.", result.unwrap_err());
                    }
                    for i in 0..remain.len() {
                        byte_queue.push_back(remain[i]);
                    }
                }
                
                if ReadPipe::is_preamble(&byte_queue) {
                    let read_packet_result: ResultBacktrace<()> = Ok(());
                    let mut success = false;
                    let mut packet = OwnedPacket {header: PacketHeader{message_id: ESTAB_PREAMBLE_0, 
                        index: ESTAB_PREAMBLE_1, total: ESTAB_PREAMBLE_2, size: ESTAB_PREAMBLE_3}, bytes: vec![]};
                    
                    //read the type code and the two nonces
                    let mut buf = vec![0xFFu8; 33 - (byte_queue.len() - HEADER_SIZE)];
                    let mut read_packet_result = merge_std_mut(read_packet_result, || {self.file.read_exact(&mut buf)}, "Could not read message type and nonces from ESTAB pipe.");
                    
                    //handle the rest of the message
                    read_packet_result = merge_mut(read_packet_result, || {
                        for i in 0..buf.len() {
                            byte_queue.push_back(buf[i]);
                        }
                        match buf[0] {
                            0x00u8 => {
                                //message 1
                                
                                /*
                                    Read bytes until we get 4 bytes of zeroes

                                    We can't just trust the filename length parameters, because
                                    a malicious packet could perform a denial-of-service attack by setting both filename 
                                    parameters to 65535 and then not write those bytes. If we tried to read that number
                                    of bytes, we would infinitely block and could not accept future honest packets
                                    (or at least until 2^17 bytes worth of ESTAB packets are written to the pipe)

                                    Reading until we hit the packet suffix is okay, though, because the attacker can't infinitely
                                    write nonzero bytes to ESTAB, because of the PIPE_BUF limit, and we'll stop when we hit the 
                                    end of the first honest packet. Of course, the malformed malicious packet will be rejected,
                                    but the point is we don't infinitely block.
                                */
                                let result_data = ReadPipe::read_until_u32_zero(byte_queue, &mut self.file);
                                /*
                                    data[] contains all packet data after the message number (1 or 3) and the two nonces,
                                    including the 4 bytes of zeroes at the end of the packet.
                                */
                                match result_data {
                                    Ok(data) => {
                                        /*
                                            At a miniumum, we need the two filename lengths, at least one byte each for 
                                            each filename, and 4 bytes of zeroes for the suffix.
                                        */
                                        if data.len() >= 10  {
                                            let in_length = data[0] as u16 + ((data[1] as u16) << 8);
                                            let out_length = data[2] as u16 + ((data[3] as u16) << 8);
                                            //Check filenames are correct length
                                            if data.len() as u32 == 4u32 + in_length as u32 + out_length as u32 + 4u32 {
                                                //write the bytes to the packet
                                                packet.bytes = vec![0u8; 33 + data.len()];
                                                //we could also take the values from byte_queue here, they have the same data.
                                                for i in 0..33 {
                                                    packet.bytes[i] = buf[i];
                                                }
                                                for i in 0..data.len() {
                                                    packet.bytes[33 + i] = data[i];
                                                }
                                                success = true;
                                            }
                                        }
                                    },
                                    Err(err) => {
                                        return wrap_err("Could not read complete packet", err);
                                    }
                                }
                            },
                            0x01u8 => {
                                //message 3  

                                /*
                                    We don't need the 'read until we see 4 bytes of zeroes' trick from message 1 because
                                    message 3 is fixed size. Even if the attacker submits a malicious packet that lacks enough
                                    bytes to fill the fixed size, they'll be filled by the begninning of the next packet and we
                                    won't infinitely block.
                                */
                                
                                //If we don't have enough bytes in the queue, read the rest from the pipe.
                                if byte_queue.len() < HEADER_SIZE + 38 {
                                    let mut data_buf = vec![(HEADER_SIZE + 38 - byte_queue.len()) as u8];
                                    let read_data_result = self.file.read_exact(data_buf.as_mut_slice());
                                    match read_data_result {
                                        Ok(()) => {
                                            for i in 0..data_buf.len() {
                                                byte_queue.push_back(data_buf[i]);
                                            }
                                        },
                                        Err(err) => {
                                            return wrap_std_err("Could not read complete packet", err);
                                        }
                                    }
                                }
                                let first_suffix_index = HEADER_SIZE + 34;
                                let mut incorrect_suffix = false;
                                for i in first_suffix_index..first_suffix_index + 4 {
                                    incorrect_suffix |= *(byte_queue.get(i).unwrap()) != 0;
                                }
                                if !incorrect_suffix {
                                    packet.bytes = vec![0u8; 38];
                                    for i in 0..38 {
                                        packet.bytes[i] = *byte_queue.get(i).unwrap();
                                    }
                                    success = true;
                                }
                            },
                            _ => {
                                //invalid message, continue at next byte
                            }
                        };
                        Ok(())
                    }, 
                    "Could not read enough bytes from ESTAB pipe for status code and nonces");
                    byte_queue.pop_front();
                    if success {
                        return Ok(packet);
                    }
                } else {
                    byte_queue.pop_front();
                }
           }
    }

    fn read_until_u32_zero(byte_queue: &mut VecDeque<u8>, file: &mut File) -> ResultBacktrace<Vec<u8>> {
        let mut buf = [0u8; 1];
        let mut data = vec![];
        let mut zeroes = 0;
        let mut read_result: ResultBacktrace<()> = Ok(());
        let mut i = 33 as usize + HEADER_SIZE;
        while zeroes < 4 {
            if i >= byte_queue.len() {
                //read from the file
                read_result = merge_std_mut(read_result, || {
                    file.read_exact(&mut buf)
                }, &format!("Could not read data byte {} from ESTAB.", data.len()));
                //we can't read that byte again, so if the read was successful, add it to the queue.
                if read_result.is_ok() {
                    byte_queue.push_back(buf[0]);
                }
            } else {
                //take the queue byte
                buf[0] = *(byte_queue.get(i).unwrap());
            }
            if buf[0] == 0x00 {
                zeroes += 1;
            } else {
                zeroes = 0;
            }
            data.push(buf[0]);
            i += 1;
        }
        match read_result {
            Ok(()) => {
                Ok(data)
            },
            Err(err) => {
                wrap_err("Could not read bytes from ESTAB pipe for variable length data", err)
            }
        }
    }

    fn is_preamble(queue: &VecDeque<u8>) -> bool {
           *queue.get(0).unwrap() == 0x00
        && *queue.get(1).unwrap() == 0x11
        && *queue.get(2).unwrap() == 0x22
        && *queue.get(3).unwrap() == 0x33
        && *queue.get(4).unwrap() == 0x44
        && *queue.get(5).unwrap() == 0x55
        && *queue.get(6).unwrap() == 0x66
        && *queue.get(7).unwrap() == 0x77
        && *queue.get(8).unwrap() == 0x88
        && *queue.get(9).unwrap() == 0x99
        && *queue.get(10).unwrap() == 0xAA
        && *queue.get(11).unwrap() == 0xBB
        && *queue.get(12).unwrap() == 0xCC
        && *queue.get(13).unwrap() == 0xDD
        && *queue.get(14).unwrap() == 0xEE
        && *queue.get(15).unwrap() == 0xFF
        && *queue.get(16).unwrap() == 0x01
        && *queue.get(17).unwrap() == 0x23
        && *queue.get(18).unwrap() == 0x45
        && *queue.get(19).unwrap() == 0x67
    }
}

#[cfg(target_family = "unix")]
pub(crate) struct WritePipe {
    file: File
}

impl WritePipe {
    pub(crate) fn new(file: File) -> WritePipe {
        WritePipe { file }
    }

    #[cfg(target_family = "unix")]
    pub(crate) fn write_packet(&mut self, packet: &SlicePacket) -> ResultBacktrace<()> {

        // We have to invoke the Unix write syscall directly because we can't guarantee
        // Rust's file I/O system will always issue a single write, and we need it all in one 
        // write because that's required for the packets to remain contiguous.

        let raw_memory = &packet as *const _ as *const u8;

        // This setup will write the header in the system's endianness, so the client program reading the packet
        // must read it in that endianness.
        let raw_packet_size = HEADER_SIZE + packet.header.size as usize;
        let bytes_written: isize = write(self.file.as_raw_fd() as usize, raw_memory, raw_packet_size);
        match bytes_written {
            -1 => {
                leaf_err(&format!("Could not write packet to outbound pipe (Message ID: {}, Packet {} of {})", packet.header.message_id, packet.header.index + 1, packet.header.total))
            },
            _ => {
                if bytes_written as usize != raw_packet_size {
                    leaf_err(&format!("Partial packet written to outbound pipe (Message ID: {}, Packet {} of {}, wrote {} out of {} bytes)", packet.header.message_id, packet.header.index + 1, packet.header.total, bytes_written, packet.header.total))
                } else {
                    Ok(())
                }
            }
        }
    }

    pub(crate) fn try_clone(&self) -> ResultBacktrace<Self> {
        match self.file.try_clone() {
            Ok(file) => {
                Ok(Self {file})
            },
            Err(err) => {
                wrap_std_err("Could not clone WritePipe for command response output.", err)
            }
        }
    }
}
