use crate::utils::err_handle::unwrap_or_wrap_std_err;
use crate::utils::string_utils::socket_escape;
use crate::utils::err_handle::{ResultBacktrace, leaf_err, not_implemented, unwrap_or_wrap_err};
use std::env::consts::OS;
use std::io::{BufReader, BufWriter, Write};
use std::os::unix::net::{UnixStream, UnixListener};


pub(crate) struct VirtualSocket {
    unix_socket: UnixRW
}

impl VirtualSocket {
    pub(crate) fn from_unix_socket(stream: UnixStream) -> ResultBacktrace<VirtualSocket> {
        //TODO reimplement with conditional compilation
        match OS {
            "linux" | "macos" | "freebsd" | "solaris" => {
                let result = stream.try_clone();
                if result.is_err() {
                    return leaf_err("Could not clone unix socket for writing.");
                }
                return Ok(VirtualSocket { unix_socket: UnixRW {reader: result.unwrap(), writer: stream } });
            }
            "windows" => {
                return not_implemented();
            }
            _ => {leaf_err(format!("Unsupported OS: '{}'", OS).as_str())}
        }
    }

    pub(crate) fn from_windows_pipe(pipe: Pipe) -> ResultBacktrace<VirtualSocket> {
        not_implemented()
    }

    pub(crate) fn send_line(&mut self, msg: String) {
        let mut msg = socket_escape(msg.as_ref());
        msg.push('\n');
        self.send(msg.as_bytes());
    }

    fn send(&mut self, bytes: &[u8]) -> ResultBacktrace<usize> {
        unwrap_or_wrap_std_err(self.unix_socket.writer.write(bytes), "Hello!")
    }

    pub(crate) fn receive_line(&mut self) {

    }
}

struct UnixRW {
    reader: UnixStream,
    writer: UnixStream
}


//TODO for debug only
pub(crate) struct Pipe {
    
}