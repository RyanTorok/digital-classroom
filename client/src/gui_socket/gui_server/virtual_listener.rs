use crate::utils::err_handle::wrap_result_or_wrap_leaf_err;
use std::os::unix::net::SocketAddr;
use std::os::unix::net::UnixStream;
use std::io::Error;
use crate::utils::err_handle::wrap_or_wrap_leaf_err;
use crate::utils::err_handle::wrap_or_wrap_err;
use std::os::unix::net::UnixListener;
use crate::gui_socket::gui_server::virtual_socket::VirtualSocket;
use std::env::consts::OS;
use crate::utils::err_handle::{ResultBacktrace, leaf_err, unwrap_or_wrap_err};


#[cfg(target_family = "windows")]
struct WindowsListener {

}

#[cfg(target_family = "windows")]
impl WindowsListener {
    pub(crate) fn accept() -> Pipe {
        Pipe {}
    }
}

struct VirtualListener {
    #[cfg(target_family = "unix")]
    unix_listener: UnixListener
}

impl VirtualListener {
    pub(crate) fn new(filename: &str) {

    }

    #[cfg(target_family = "unix")]
    pub(crate) fn accept(&self) -> ResultBacktrace<VirtualSocket> {
        let res: std::io::Result<(UnixStream, SocketAddr)> = self.unix_listener.accept();
        wrap_result_or_wrap_leaf_err(res, |s| {VirtualSocket::from_unix_socket(s.0)}, "Could not wrap Unix socket as virtual socket.")
    }

    #[cfg(target_family = "windows")]
    pub(crate) fn accept(&self) -> ResultBacktrace<VirtualSocket> {
        VirtualSocket::from_unix_socket(self.unix_listener.unwrap().accept().0);
    }
}