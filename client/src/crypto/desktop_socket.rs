use crate::crypto::CryptoOperations;
use crate::utils::err_handle::{ResultBacktrace, leaf_err, unwrap_or_wrap_err, wrap_err};
use crate::api::api::{socket_password_hash_stage_1, socket_libsodium_salt_bytes, socket_symmetric_decrypt, socket_symmetric_encrypt};
use crate::api::tuple::unwrap_c_tuple_2;
use crate::command_utils::err_codes::{OK, ERR_NOBACKEND, ERR_NULLPTR};
use std::rc::Rc;
use js_sys::{Uint8Array, WebAssembly};
use wasm_bindgen::prelude::*;
use web_sys::{Document, Element, HtmlElement, Window};
use sha2::{
    Digest,
    digest::generic_array::GenericArray
};

pub(crate) struct DesktopSocketCrypto {}

/*fn secure_random_bytes(length: u32) -> Rc<Vec<u8>> {
    let c_tuple = socket_secure_random(length);
    let exit_code: u16 = c_tuple.a;
    let random_bytes: Rc<Vec<u8>> = c_tuple.b;
    match exit_code {
        OK => Ok(random_bytes),

        // Handle error codes
        ERR_NOBACKEND => leaf_err("Could not perform password hash because the client backend \
                was not available on the local machine, and libsodium was not shipped with the WASM binary."),
        ERR_NULLPTR => leaf_err("Somehow an internal command call created a null pointer."),
        _ => leaf_err(&format!("The socket_password_hash_stage_1 command produced an error message it did not declare: {}", exit_code))
    }
}*/

impl CryptoOperations for DesktopSocketCrypto {

    fn sodium_init() {
        // Nothing here, because the backend we talk to initializes it automatically.
    }

    fn password_hash_stage_1(&self, plaintext: &str, salt: &[u8]) -> ResultBacktrace<Vec<u8>> {
        let c_tuple = socket_password_hash_stage_1(plaintext, salt);
        let exit_code: u16 = c_tuple.a;
        let ciphertext = c_tuple.b;
        match exit_code {
            OK => Ok(ciphertext),
            // Handle error codes
            ERR_NOBACKEND => leaf_err("Could not perform password hash because the client backend \
                was not available on the local machine, and libsodium was not shipped with the WASM binary."),
            ERR_NULLPTR => leaf_err("Somehow an internal command call created a null pointer."),
            _ => leaf_err(&format!("The socket_password_hash_stage_1 command produced an error message it did not declare: {}", exit_code))
        }
    }

    /*fn password_hash_stage_2(&self, stage_1_ciphertext: &[u8]) -> ResultBacktrace<Vec<u8>> {
        unimplemented!()
    }*/

    fn salt_bytes(&self) -> ResultBacktrace<usize> {
        let c_tuple = socket_libsodium_salt_bytes();
        let exit_code: u16 = c_tuple.a;
        let plaintext: u32 = c_tuple.b;
        match c_tuple.a {
            OK => Ok(plaintext as usize),

            // Handle error codes
            ERR_NOBACKEND => leaf_err("Could not perform password hash because the client backend \
                was not available on the local machine, and libsodium was not shipped with the WASM binary."),
            ERR_NULLPTR => leaf_err("Somehow an internal command call created a null pointer."),
            _ => leaf_err(&format!("The socket_password_hash_stage_1 command produced an error message it did not declare: {}", exit_code))
        }

    }

    fn symmetric_encrypt(&self, plaintext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let c_tuple = socket_symmetric_encrypt(plaintext, key, iv);
        let exit_code: u16 = c_tuple.a;
        let ciphertext = c_tuple.b;
        match exit_code {
            OK => Ok(ciphertext),

            // Handle error codes
            ERR_NOBACKEND => leaf_err("Could not perform password hash because the client backend \
                was not available on the local machine, and libsodium was not shipped with the WASM binary."),
            ERR_NULLPTR => leaf_err("Somehow an internal command call created a null pointer."),
            _ => leaf_err(&format!("The socket_password_hash_stage_1 command produced an error message it did not declare: {}", exit_code))
        }
    }

    fn symmetric_decrypt(&self, ciphertext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let c_tuple = socket_symmetric_decrypt(ciphertext, key, iv);
        let exit_code: u16 = c_tuple.a;
        let plaintext = c_tuple.b;
        match exit_code {
            OK => Ok(plaintext),

            // Handle error codes
            ERR_NOBACKEND => leaf_err("Could not perform password hash because the client backend \
                was not available on the local machine, and libsodium was not shipped with the WASM binary."),
            ERR_NULLPTR => leaf_err("Somehow an internal command call created a null pointer."),
            _ => leaf_err(&format!("The socket_password_hash_stage_1 command produced an error message it did not declare: {}", exit_code))
        }
    }

    /*fn local_password_encryption_key(&self, pin: Option<String>) -> ResultBacktrace<Vec<u8>> {
        let mut sha512 = sha2::Sha512::default();

        //get this system's hardware key
        let seed = derive_hardware_key();
        if seed.is_err() {
            return wrap_err("Failed to generate hardware key as seed.", seed.unwrap_err());
        }
        let mut seed = seed.unwrap_or_default();

        //throw in the user's pin if there is one
        if let Some(pin) = pin {
            seed.extend_from_slice(pin.as_bytes())
        }

        //hash with SHA-512
        sha512.input(seed);
        let hash1 = sha512.result();

        //hash with Argon2id
        let hash2 = argon_keygen(&Vec::from(hash1.as_slice()));
        unwrap_or_wrap_err(hash2, "Failed to hash SHA-512-hashed LPEK with Argon2.")
    }*/
}

