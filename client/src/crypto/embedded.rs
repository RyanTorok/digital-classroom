use crate::{
    crypto::{CryptoOperations, HashType},
    utils::err_handle::ResultBacktrace,
    utils::os::OS,
    utils::string_utils::{push_byte_vec},
    utils::err_handle::{ErrSpec, wrap_err, leaf_err, unwrap_or_wrap_err}
};
use libc::{
    c_longlong,
    c_uchar,
    c_schar
};
use sha2::{
    Digest,
    digest::generic_array::GenericArray
};
use libsodium_sys::{crypto_pwhash_OPSLIMIT_MAX, crypto_pwhash_STRBYTES, crypto_pwhash_SALTBYTES, crypto_pwhash_ALG_ARGON2I13, crypto_pwhash_OPSLIMIT_INTERACTIVE, crypto_pwhash_MEMLIMIT_INTERACTIVE, self, crypto_pwhash_MEMLIMIT_MODERATE, crypto_pwhash_OPSLIMIT_MODERATE, crypto_pwhash_OPSLIMIT_SENSITIVE, crypto_pwhash_ALG_ARGON2ID13, crypto_pwhash_saltbytes, crypto_box_SEEDBYTES, crypto_aead_xchacha20poly1305_ietf_encrypt, crypto_aead_xchacha20poly1305_ietf_ABYTES, crypto_aead_xchacha20poly1305_ietf_decrypt, crypto_box_keypair, sodium_init};
use std::ffi::{CString, CStr};
use std::os::*;
use std::process::Command;
use std::ptr::null;
use rand::RngCore;
use std::borrow::Cow;

pub const SALT_LENGTH: u32 = crypto_pwhash_SALTBYTES;
const IV_BYTES: usize = 192;

pub(crate) struct EmbeddedCrypto {}

impl CryptoOperations for EmbeddedCrypto {

    fn sodium_init() {
        // Call to external function
        unsafe { libsodium_sys::sodium_init(); }
    }

    // Performs a hash intended to be used as an intermediate form before the final hash,
    // where we can't use a random salt, because asking the server for the salt is insecure
    // (it allows the attacker to verify usernames without knowing the passwords).
    #[inline]
    fn password_hash_stage_1(&self, plaintext: &str, salt: &[u8]) -> ResultBacktrace<Vec<u8>> {
        hash_and_salt(plaintext.as_bytes(), salt, HashType::Password)
    }

    /*fn password_hash_stage_2(&self, stage_1_ciphertext: &[u8]) -> ResultBacktrace<Vec<u8>> {
        let outlen: u64 = (crypto_pwhash_STRBYTES) as u64;
        let mut buf: Vec<u8> = vec![0 ; outlen as usize];
        let ciphertext = buf.as_mut_ptr() as *mut libc::c_char;
        let len_plaintext = stage_1_ciphertext.len();
        let c_plaintext = stage_1_ciphertext.as_ptr();

        let success = unsafe {
            libsodium_sys::crypto_pwhash_argon2id_str(
                ciphertext,
                c_plaintext as *const libc::c_char,
                len_plaintext as u64,
                (crypto_pwhash_OPSLIMIT_INTERACTIVE) as u64,
                (crypto_pwhash_MEMLIMIT_INTERACTIVE) as usize,
            )
        };
        if success == 0 {
            return Ok(buf);

        } else {
            return leaf_err("Auto-salted string hashing failed.");
        }
    }*/

    #[inline]
    fn salt_bytes(&self) -> ResultBacktrace<usize> {
        Ok(SALT_LENGTH as usize)
    }

    fn symmetric_encrypt(&self, plaintext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let mut ciphertext: Vec<u8> = vec![0; plaintext.len() + crypto_aead_xchacha20poly1305_ietf_ABYTES as usize];
        let mut outlen: u64 = 0;
        let nullptr: *const u8 = 0 as *const u8;
        unsafe { crypto_aead_xchacha20poly1305_ietf_encrypt(
            ciphertext.as_mut_ptr(),
            &mut outlen as *mut u64,
            plaintext.as_ptr(),
            plaintext.len() as u64,
            nullptr ,
            0,
            nullptr,
            iv.as_ptr(),
            key.as_ptr()
        ); }
        ciphertext.truncate(outlen as usize);
        Ok(ciphertext)
    }

    fn symmetric_decrypt(&self, ciphertext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let mut plaintext: Vec<u8> = vec![0 ; ciphertext.len()];
        let mut outlen: u64 = 0;
        let nullptr: *mut u8 = 0 as *mut u8;
        unsafe { crypto_aead_xchacha20poly1305_ietf_decrypt(
            plaintext.as_mut_ptr(),
            &mut outlen as *mut u64,
            nullptr,
            ciphertext.as_ptr(),
            ciphertext.len() as u64,
            nullptr,
            0,
            iv.as_ptr(),
            key.as_ptr()
        ); }
        Ok(plaintext)
    }

}

impl EmbeddedCrypto {

    #[inline]
    pub fn new() -> EmbeddedCrypto {
        EmbeddedCrypto {}
    }

    pub fn local_password_encryption_key(&self, opt_pin: Option<String>) -> ResultBacktrace<Vec<u8>> {
        let mut sha512 = sha2::Sha512::default();

        //get this system's hardware key
        let seed = derive_hardware_key();
        if seed.is_err() {
            return wrap_err("Failed to generate hardware key as seed.", seed.unwrap_err());
        }
        let mut seed = seed.unwrap_or_default();

        //throw in the user's pin if there is one
        if let Some(pin) = opt_pin {
            seed.extend_from_slice(pin.as_bytes())
        }

        //hash with SHA-512
        sha512.input(seed);
        let hash1 = sha512.result();

        //hash with Argon2id
        let hash2: ResultBacktrace<Vec<u8>> = argon_keygen(&hash1);
        unwrap_or_wrap_err(hash2, "Failed to hash SHA-512-hashed LPEK with Argon2.")
    }
}

fn derive_hardware_key() -> Result<Vec<u8>, ErrSpec> {
    let mut key: Vec<u8> = vec![];
    key.extend_from_slice(OS.to_str().as_bytes());
    //TODO change to conditional compilation
    //version
    match OS {
        OS::WIN32 => {},
        OS::WIN64 => {},
        OS::MAC => {},
        OS::LINUX => {
            let distribution = Command::new("lsb_release").arg("-ds").output();
            if distribution.is_err() {
                return leaf_err("Failed to execute: lsb_release");
            }
            push_byte_vec(&mut key, distribution.unwrap().stdout);
            let cpu_data = Command::new("lscpu").arg("").output();
            if cpu_data.is_err() {
                return leaf_err("Failed to execute: lscpu");
            }
            push_byte_vec(&mut key, cpu_data.unwrap().stdout);

            //TODO needs more robust solution. Doesn't work in int'l implementations
            let mac_address = Command::new("ifconfig eth0 | awk '/HWaddr/ {print $NF}'").arg("").output();
            if mac_address.is_err() {
                return leaf_err("Failed to execute: ifconfig eth0 | awk '/HWaddr/ {print $NF}'");
            }
            push_byte_vec(&mut key, mac_address.unwrap().stdout);
        },
        OS::FREEBSD => {},
    };

    return Ok(key);
}

#[inline]
fn argon_keygen(seed: &[u8]) -> Result<Vec<u8>, ErrSpec> {
    hash_and_salt(seed, &deterministic_salt(), HashType::Key)
}

fn hash_and_salt(plaintext: &[u8], salt: &[u8], hash_type: HashType) -> ResultBacktrace<Vec<u8>> {

    let out_len: u64 = match hash_type {
        HashType::Password => {crypto_pwhash_STRBYTES},
        HashType::Key => {crypto_box_SEEDBYTES},
    } as u64;

    let mut buf: Vec<u8> = vec![0; out_len as usize];
    let ciphertext = buf.as_mut_ptr() as *mut c_uchar;
    let len_plaintext = plaintext.len();
    let c_plaintext = plaintext.as_ptr();
    let c_salt = salt.as_ptr();

    let success = unsafe {
        libsodium_sys::crypto_pwhash(
            ciphertext,
            out_len,
            c_plaintext as *const libc::c_char,
            len_plaintext as u64,
            c_salt,
            (crypto_pwhash_OPSLIMIT_INTERACTIVE) as u64,
            (crypto_pwhash_MEMLIMIT_INTERACTIVE) as usize,
            crypto_pwhash_ALG_ARGON2ID13 as i32
        )
    };
    if success == 0 {
        Ok(buf)
    } else {
        leaf_err("Specified-salt string hashing failed.")
    }
}

// Returns a salt that is always the same for a given length.
// SHOULD NOT be used for hashing passwords, only for encryption keys.
// Overkill maybe, but Argon2 always needs a salt, and all zeroes is just too easy.
fn deterministic_salt() -> Vec<u8> {
    let mut vec: Vec<u8> = vec![];
    for i in 0..crypto_pwhash_SALTBYTES {
        vec.push((i % 256) as u8);
    }
    vec
}
