use crate::crypto::{CryptoOperations, HashType};
use crate::utils::err_handle::{ResultBacktrace, unwrap_or_wrap_err, leaf_err, wrap_err};
use js_sys::{Uint8Array, WebAssembly};
use wasm_bindgen::prelude::*;
use sha2::{
    Digest,
    digest::generic_array::GenericArray
};
use std::sync::{Condvar, Mutex, Arc, RwLock};
use std::rc::Rc;

extern crate wasm_bindgen;

#[wasm_bindgen]
extern "C" {

    type Sodium;
    type Window;

    static WINDOW: Window;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_pwhash_STRBYTES(this: &Sodium) -> u32;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_pwhash_SALTBYTES(this: &Sodium) -> u32;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_pwhash_OPSLIMIT_INTERACTIVE(this: &Sodium) -> u32;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_box_SEEDBYTES(this: &Sodium) -> u32;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_pwhash_ALG_ARGON2ID13(this: &Sodium) -> i32;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_pwhash_MEMLIMIT_INTERACTIVE(this: &Sodium) -> u32;

    #[wasm_bindgen(structural, method)]
    fn crypto_pwhash_argon2id_str(this: &Sodium, plaintext: &[u8], opslimit: u32, memlimit: u32) -> Uint8Array;

    #[wasm_bindgen(structural, method)]
    fn randombytes_buf(this: &Sodium, n: u32) -> Uint8Array;

    #[wasm_bindgen(structural, method, getter)]
    fn crypto_aead_xchacha20poly1305_ietf_ABYTES(this: &Sodium);

    #[wasm_bindgen(structural, method)]
    fn crypto_aead_xchacha20poly1305_ietf_encrypt(this: &Sodium, plaintext: &[u8], additional_data: JsValue, null: JsValue, nonce: &[u8], key: &[u8]) -> Uint8Array;

    #[wasm_bindgen(structural, method)]
    fn crypto_aead_xchacha20poly1305_ietf_decrypt(this: &Sodium, ciphertext: &[u8], additional_data: JsValue, null: JsValue, nonce: &[u8], key: &[u8]) -> Uint8Array;

    #[wasm_bindgen(structural, method)]
    fn crypto_pwhash(this: &Sodium, plaintext: &[u8], salt: &[u8], ops_limit: u32, mem_limit: u32, algorithm: i32) -> Vec<u8>;

    #[wasm_bindgen(method, getter, js_name = sodium)]
    fn get_sodium(this: &Window) -> Sodium;
}

lazy_static! {
    static ref SODIUM_INITIALIZED_TX: Arc<(Mutex<Option<()>>, Condvar)> = Arc::new((Mutex::new(None), Condvar::new()));
    static ref SODIUM_INITIALIZED_RX: Arc<(Mutex<Option<()>>, Condvar)> = SODIUM_INITIALIZED_TX.clone();
}

#[wasm_bindgen]
pub fn notify_sodium_init() {
    *(SODIUM_INITIALIZED_TX.0.lock().unwrap()) = Some(());
    SODIUM_INITIALIZED_TX.1.notify_all();
}

// This function waits for JS to initialize libsodium and return us the JS object we can use to
// invoke the crypto functions. `notify_sodium_init` receives the sodium JsValue from JS, and
// completes the Condvar that unblocks this function so we can return the JS object.
fn sodium() -> Sodium {
    let mut init_state = SODIUM_INITIALIZED_RX.0.lock().unwrap();
    while (*init_state).is_none() {
        init_state = SODIUM_INITIALIZED_RX.1.wait(init_state).unwrap();
    }
    // Now we know that init_state.is_some() is true.

    // Some IDEs flag this as unsafe, since wasm-bindgen brings it in as an extern static, but
    // it does not require an unsafe block.
    WINDOW.get_sodium()
}

fn secure_random_bytes(length: u32) -> Vec<u8> {
    sodium().randombytes_buf(length).to_vec()
}

pub(crate) struct WasmLinkedCrypto {
}

impl WasmLinkedCrypto {
    pub fn new() -> WasmLinkedCrypto {
        WasmLinkedCrypto {}
    }
}

impl CryptoOperations for WasmLinkedCrypto {

    fn sodium_init() {
        // Nothing here yet, because JS initializes libsodium for us. This function is only here to
        // satisfy the interface.
    }

    // Performs a hash intended to be used as an intermediate form before the final hash,
    // where we can't use a random salt, because asking the server for the salt is insecure
    // (it allows the attacker to verify usernames without knowing the passwords).
    #[inline]
    fn password_hash_stage_1(&self, plaintext: &str, salt: &[u8]) -> ResultBacktrace<Vec<u8>> {
        hash_and_salt(plaintext.as_bytes(), salt, HashType::Password)
    }

    fn salt_bytes(&self) -> ResultBacktrace<usize> {
        Ok(sodium().crypto_pwhash_SALTBYTES() as usize)
    }

    /*fn password_hash_stage_2(&self, stage_1_ciphertext: &[u8]) -> ResultBacktrace<Vec<u8>> {
        let sodium = &sodium();
        let ciphertext: Vec<u8> = unsafe {
            crypto_pwhash_argon2id_str(
                sodium(&WINDOW()),
                Uint8Array::from(stage_1_ciphertext),
                crypto_pwhash_OPSLIMIT_INTERACTIVE(sodium),
                crypto_pwhash_MEMLIMIT_INTERACTIVE(sodium),
            );
        }.to_vec();
        Ok(ciphertext)
    }*/


    fn symmetric_encrypt(&self, plaintext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let sodium = sodium();
        let null = JsValue::null();
        let null2 = JsValue::null();
        let ciphertext: Vec<u8> = sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(
            plaintext,
            null,
            null2,
            iv,
            key
        ).to_vec();
        Ok(ciphertext)
    }

    fn symmetric_decrypt(&self, ciphertext: &[u8], key: &[u8], iv: &[u8; 192]) -> ResultBacktrace<Vec<u8>> {
        let sodium = sodium();
        let null = JsValue::null();
        let null2 = JsValue::null();
        let plaintext: Vec<u8> = sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(
            ciphertext,
            null,
            null2,
            iv,
            key
        ).to_vec();
        Ok(plaintext)
    }
}


fn hash_and_salt(plaintext: &[u8], salt: &[u8], hash_type: HashType) -> ResultBacktrace<Vec<u8>> {
    let sodium = sodium();
    let ciphertext = sodium.crypto_pwhash(
        plaintext,
        salt,
        sodium.crypto_pwhash_OPSLIMIT_INTERACTIVE(),
        sodium.crypto_pwhash_MEMLIMIT_INTERACTIVE(),
        sodium.crypto_pwhash_ALG_ARGON2ID13()
    );
    Ok(ciphertext)
}
