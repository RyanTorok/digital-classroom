// FILE: crypto/mod.rs
//
// DESCRIPTION: This file declares a wrapper API around the libsodium cryptography library. The API
// is implemented for each platform inside this module.
//
// CHANGE LOG:
// 24 Jan 2021 -- Added documentation header

use crate::utils::err_handle::ResultBacktrace;
use rand::RngCore;
use std::rc::Rc;

#[cfg(any(feature = "desktop", feature = "mobile"))]
pub(crate) mod embedded;

#[cfg(all(feature = "web", feature = "no_crypto"))]
pub(crate) mod desktop_socket;

#[cfg(all(feature = "web", not(feature = "no_crypto")))]
pub(crate) mod wasm_linked;

pub(crate) const IV_BYTES: usize = 192;

pub(crate) enum HashType {
    Password,
    Key
}


pub(crate) trait CryptoOperations {
    fn sodium_init();
    fn password_hash_stage_1(&self, plaintext: &str, salt: &[u8]) -> ResultBacktrace<Vec<u8>>;
//    fn password_hash_stage_2(&self, stage_1_ciphertext: &[u8]) -> ResultBacktrace<Vec<u8>>; // Probably don't need this, since it's done on the server
    fn salt_bytes(&self) -> ResultBacktrace<usize>;
    fn symmetric_encrypt(&self, plaintext: &[u8], key: &[u8], iv: &[u8 ; IV_BYTES]) -> ResultBacktrace<Vec<u8>>;
    fn symmetric_decrypt(&self, ciphertext: &[u8], key: &[u8], iv: &[u8 ; IV_BYTES]) -> ResultBacktrace<Vec<u8>>;
//    fn local_password_encryption_key(&self, pin: Option<String>) -> ResultBacktrace<Vec<u8>>;
}

pub(crate) fn secure_random_bytes(n: usize) -> Vec<u8> {
    let mut buf = vec![0u8; n];
    rand::thread_rng().fill_bytes(&mut buf);
    buf
}

pub(crate) fn random_salt(crypto: &impl CryptoOperations) -> ResultBacktrace<Vec<u8>> {
    Ok(secure_random_bytes(crypto.salt_bytes()?))
}

#[cfg(any(feature = "desktop", feature = "mobile"))]
pub(crate) type Crypto = embedded::EmbeddedCrypto;

#[cfg(all(feature = "web", feature = "no_crypto"))]
pub(crate) type Crypto = desktop_socket::DesktopSocketCrypto;

#[cfg(all(feature = "web", not(feature = "no_crypto")))]
pub(crate) type Crypto = wasm_linked::WasmLinkedCrypto;
