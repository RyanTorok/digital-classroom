#[cfg(feature = "libdcl")]
mod api;

mod commands;

mod user;
mod classes;
mod command_utils;
mod crypto;
mod identifiers;
mod network;
mod search_engine;
mod system;
mod tools;
mod utils;

#[cfg(feature = "test")]
mod test;

#[cfg(any(feature = "desktop", feature = "web"))]
mod gui_socket;
#[cfg(any(feature = "desktop", feature = "web"))]
mod ipc;

#[cfg(feature = "backend")]
mod filesystem;
#[cfg(feature = "backend")]
mod console;
#[cfg(feature = "backend")]
mod command_executor;

#[cfg(feature = "backend")]
use console::console_options::handle_arguments;
#[cfg(feature = "backend")]
use console::console_options::parse_arguments;
#[cfg(feature = "backend")]
use std::process::exit;
#[cfg(feature = "backend")]
use crate::crypto::embedded::EmbeddedCrypto;
#[cfg(feature = "backend")]
use crate::crypto::CryptoOperations;


#[cfg(all(feature = "libdcl", any(feature = "desktop", feature = "web")))]
#[macro_use]
extern crate lazy_static;

#[cfg(feature = "backend")]
fn main() {
    let properties = parse_arguments();
    init_libraries();
    handle_arguments(properties.0);
    println!("{}\n\nTry 'digital-classroom --help' for more information.", properties.1);
    exit(0);
}

//initialize all imported libraries
#[cfg(feature = "backend")]
#[inline]
fn init_libraries() {
    unsafe { EmbeddedCrypto::sodium_init() }
}