#include <cstdarg>
#include <cstdint>
#include <cstdlib>
#include <ostream>
#include <new>

/// Command: test_socket_command
///
/// Tests to make sure the command socket works correctly. Used for testing purposes only.
///
/// Arguments:
/// message (string) A test input message
///
/// Returns:
/// (string) A message indicating the command received the argument string
///
/// Throws:
/// ERR_NONUTF8 -- If the parameter string is not valid UTF-8
/// ERR_NOBACKEND -- If called in socket mode and the backend could not be reached
static const uint16_t OPCODE_TEST_SOCKET_COMMAND = 0;

/// Command: test_linked_command
///
/// Tests to make sure the linked command API works correctly. Used for testing purposes only.
///
/// Arguments:
/// message (string) A test input message
///
/// Returns:
/// (string) A message indicating the command received the argument string
///
/// Throws:
/// None
static const uint16_t OPCODE_TEST_LINKED_COMMAND = 1;

/// Command: test_types_tuples_socket
///
/// Tests to make sure the command socket can receive commands of all specified types, and receive and return
/// multiple values.
///
/// Arguments:
/// int8 (int8) an 8-bit integer
/// int16 (int16) a 16-bit integer
/// int32 (int32) a 32-bit integer
/// int64 (int64) a 64-bit integer
/// int128 (int128) a 128-bit integer
/// uint8 (uint8) an 8-bit unsigned integer
/// uint16 (uint16) a 16-bit unsigned integer
/// uint32 (uint32) a 32-bit unsigned integer
/// uint64 (uint64) a 64-bit unsigned integer
/// uint128 (uint128) a 128-bit unsigned integer
/// uuid (uuid) a 128-bit universally unique identifier (uuid)
/// string (string) a string of text
///
/// Returns:
/// (int8) an 8-bit integer
/// (int16) a 16-bit integer
/// (int32) a 32-bit integer
/// (int64) a 64-bit integer
/// (int128) a 128-bit integer
/// (uint8) an 8-bit unsigned integer
/// (uint16) a 16-bit unsigned integer
/// (uint32) a 32-bit unsigned integer
/// (uint64) a 64-bit unsigned integer
/// (uint128) a 128-bit unsigned integer
/// (uuid) a 128-bit universally unique identifier (uuid)
/// (string) a string of text
///
/// Throws:
/// None
static const uint16_t OPCODE_TEST_TYPES_TUPLES_SOCKET = 2;

/// Command: test_types_tuples_linked
///
/// Tests to make sure the linked command API can receive commands of all specified types, and receive and
/// return multiple values.
///
/// Arguments:
/// int8 (int8) an 8-bit integer
/// int16 (int16) a 16-bit integer
/// int32 (int32) a 32-bit integer
/// int64 (int64) a 64-bit integer
/// int128 (int128) a 128-bit integer
/// uint8 (uint8) an 8-bit unsigned integer
/// uint16 (uint16) a 16-bit unsigned integer
/// uint32 (uint32) a 32-bit unsigned integer
/// uint64 (uint64) a 64-bit unsigned integer
/// uint128 (uint128) a 128-bit unsigned integer
/// uuid (uuid) a 128-bit universally unique identifier (uuid)
/// string (string) a string of text
///
/// Returns:
/// (int8) an 8-bit integer
/// (int16) a 16-bit integer
/// (int32) a 32-bit integer
/// (int64) a 64-bit integer
/// (int128) a 128-bit integer
/// (uint8) an 8-bit unsigned integer
/// (uint16) a 16-bit unsigned integer
/// (uint32) a 32-bit unsigned integer
/// (uint64) a 64-bit unsigned integer
/// (uint128) a 128-bit unsigned integer
/// (uuid) a 128-bit universally unique identifier (uuid)
/// (string) a string of text
///
/// Throws:
/// None
static const uint16_t OPCODE_TEST_TYPES_TUPLES_LINKED = 3;

/// Command: free_ffi_string
///
/// Returns a string resource output by a previous command to the FFI to be properly deallocated from memory.
///
/// Arguments:
/// resource (string) The string resource to be deallocated
///
/// Returns:
/// None
///
/// Throws:
/// ERR_NONUTF8 -- If the argument string is not valid UTF-8
static const uint16_t OPCODE_FREE_FFI_STRING = 5;

static const uint16_t OPCODE_DECODE_ERROR = 65535;

/// OK: Returned when a command succeeds
static const uint16_t OK = 0;

/// ERR_NONUTF8: Returned when a command receives a string parameter which contains non-UTF-8 characters.
static const uint16_t ERR_NONUTF8 = 1;

/// ERR_NOBACKEND: Returned when the API cannot communicate with the client backend over the socket to issue
/// the command.
static const uint16_t ERR_NOBACKEND = 2;

/// ERR_NONETWORK: Returned when the client cannot connect to the server due to network connectivity failure.
static const uint16_t ERR_NONETWORK = 3;

/// ERR_UNSUPPORTED: Returned when a command is called that is not supported on the current platform.
static const uint16_t ERR_UNSUPPORTED = 4;

/// ERR_NULLPTR: Thrown when a command expecting a string resource receives a null pointer instead
static const uint16_t ERR_NULLPTR = 5;

/// ERR_DECODE: Thrown when a command received on the socket could not be decoded due to incorrect format.
static const uint16_t ERR_DECODE = 6;

static const uint8_t COMMAND_NOT_FOUND = 0;

static const uint8_t TOO_FEW_ARGUMENTS = 1;

static const uint8_t TOO_MANY_ARGUMENTS = 2;

static const uint8_t INCORRECT_TYPE = 3;

static const uint8_t CODE_U8 = 0;

static const uint8_t CODE_U16 = 1;

static const uint8_t CODE_U32 = 2;

static const uint8_t CODE_U64 = 3;

static const uint8_t CODE_U128 = 4;

static const uint8_t CODE_I8 = 5;

static const uint8_t CODE_I16 = 6;

static const uint8_t CODE_I32 = 7;

static const uint8_t CODE_I64 = 8;

static const uint8_t CODE_I128 = 9;

static const uint8_t CODE_UUID = 10;

static const uint8_t CODE_STRING = 11;

/// CTuple2: An FFI-readable struct representing a tuple of 2 values.
template<typename A, typename B>
struct CTuple2 {
  A a;
  B b;
};

/// CTuple13: An FFI-readable struct representing a tuple of 13 values.
template<typename A, typename B, typename C, typename D, typename E, typename F, typename G, typename H, typename I, typename J, typename K, typename L, typename M>
struct CTuple13 {
  A a;
  B b;
  C c;
  D d;
  E e;
  F f;
  G g;
  H h;
  I i;
  J j;
  K k;
  L l;
  M m;
};

extern "C" {

CTuple2<uint16_t, const char*> test_socket_command(const char *message);

CTuple2<uint16_t, const char*> test_linked_command(const char *message);

CTuple13<uint16_t, int8_t, int16_t, int32_t, int64_t, i128, uint8_t, uint16_t, uint32_t, uint64_t, u128, u128, const char*> test_types_tuples_socket(int8_t int8,
                                                                                                                                                     int16_t int16,
                                                                                                                                                     int32_t int32,
                                                                                                                                                     int64_t int64,
                                                                                                                                                     i128 int128,
                                                                                                                                                     uint8_t uint8,
                                                                                                                                                     uint16_t uint16,
                                                                                                                                                     uint32_t uint32,
                                                                                                                                                     uint64_t uint64,
                                                                                                                                                     u128 uint128,
                                                                                                                                                     u128 uuid,
                                                                                                                                                     const char *string);

CTuple13<uint16_t, int8_t, int16_t, int32_t, int64_t, i128, uint8_t, uint16_t, uint32_t, uint64_t, u128, u128, const char*> test_types_tuples_linked(int8_t int8,
                                                                                                                                                     int16_t int16,
                                                                                                                                                     int32_t int32,
                                                                                                                                                     int64_t int64,
                                                                                                                                                     i128 int128,
                                                                                                                                                     uint8_t uint8,
                                                                                                                                                     uint16_t uint16,
                                                                                                                                                     uint32_t uint32,
                                                                                                                                                     uint64_t uint64,
                                                                                                                                                     u128 uint128,
                                                                                                                                                     u128 uuid,
                                                                                                                                                     const char *string);

uint16_t free_ffi_string(const char *resource);

} // extern "C"
