import 'package:test/test.dart';
import 'package:ui/api/api.dart';
import 'package:ui/api/exit_code.dart';
import 'package:ui/api/ffi_types.dart';

void main() {
    test('Test linked command data types and multiple arguments/return values', () {
        var response = test_types_tuples_linked(
            0x12,
            0x3456,
            0x78901234,
            0x5678901234567890,
            Int128(0x1111111111111111, 0x2222222222222222),
            0x98,
            0x7654,
            0x32109876,
            0x5432109876543210,
            Uint128(0x3333333333333333, 0x4444444444444444),
            Uuid(0x5555555555555555, 0x6666666666666666),
            "I am a string."
        );
        expect(response.exitCode, OK);
        expect(response.a, 0x13);
        expect(response.b, 0x3457);
        expect(response.c, 0x78901235);
        expect(response.d, 0x5678901234567891);
        expect(response.e, Int128.fromU64LE(0x1111111111111112, 0x2222222222222222));
        expect(response.f, 0x99);
        expect(response.g, 0x7655);
        expect(response.h, 0x32109877);
        expect(response.i, 0x5432109876543211);
        expect(response.j, Uint128.fromU64LE(0x3333333333333334, 0x4444444444444444));
        expect(response.k, Uint128.fromU64LE(0x5555555555555556, 0x6666666666666666));
        expect(response.l.toString() ,"Received string: I am a string.");
    });
}
