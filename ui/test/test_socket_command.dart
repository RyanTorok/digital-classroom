import 'package:test/test.dart';
import 'package:ui/api/api.dart';
import 'package:ui/api/exit_code.dart';

void main() {
    test('Test socket command send/receive', () {
        var response = test_socket_command("This is a test.");
        expect(response.exitCode, ERR_NOBACKEND);
        var success = start_backend_socket(8);
        expect(success, 0);
        response = test_socket_command("This is a test.");
        expect(response.exitCode, OK);
        expect(response.a.toString(), "Received message: This is a test.");
    });
}