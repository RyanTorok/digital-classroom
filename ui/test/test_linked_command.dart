import 'package:test/test.dart';
import 'package:ui/api/api.dart';
import 'package:ui/api/exit_code.dart';

void main() {
    test('Test linked command send/receive', () {
        var response = test_linked_command("This is a test.");
        expect(response.exitCode, OK);
        expect("Received message: This is a test.", response.a.toString());
    });
}
