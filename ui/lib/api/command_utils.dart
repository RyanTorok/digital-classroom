import 'dart:ffi';
import 'dart:io';

import 'package:ffi/ffi.dart';
import 'gui_socket.dart';

GuiSocket socket;
final DynamicLibrary lib = Platform.isAndroid ? DynamicLibrary.open("libdcl.so")
                         : Platform.isLinux ? DynamicLibrary.open("linux/lib/libdcl.so")
                         : DynamicLibrary.process();

T unwrapType<T>(dynamic val) {
    if (val.runtimeType != T) {
        throw new Exception("Returned value was wrong type. Expected type = " + val.runtimeType.toString() + ", Actual type = " + T.toString());
    }
    return val as T;
}