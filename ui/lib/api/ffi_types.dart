import 'dart:io';
import 'dart:typed_data';
import 'package:ffi/ffi.dart';
import 'dart:ffi' as ffi;
import 'api.dart';

class Uint128 {
  Uint64List list = Uint64List(2);

  Uint128(int bytes_0_7, int bytes_8_15) {
    // Uint128 objects are always stored little endian, regardless of platform.
    if (Endian.host == Endian.little) {
      list[0] = bytes_0_7;
      list[1] = bytes_8_15;
    } else {
      list[1] = bytes_0_7;
      list[0] = bytes_8_15;
    }
  }

  static Uint128 fromU64LE(int lsb, int msb) {
    if (Endian.host == Endian.little) {
      return Uint128(lsb, msb);
    } else {
      return Uint128(msb, lsb);
    }
  }

  ffi.Pointer<ffi.Uint64> asPointer() {
    var ptr = allocate<ffi.Uint64>(count: 2);
    ptr[0] = this.list[0];
    ptr[1] = this.list[1];
    return ptr;
  }

  operator < (Uint128 other) {
    return list[1] < other.list[1] || list[1] == other.list[1] && list[0] < other.list[0];
  }

  operator > (Uint128 other) {
    return list[1] > other.list[1] || list[1] == other.list[1] && list[0] > other.list[0];
  }

  operator <= (Uint128 other) {
    return list[1] < other.list[1] || list[1] == other.list[1] && list[0] <= other.list[0];
  }

  operator >= (Uint128 other) {
    return list[1] > other.list[1] || list[1] == other.list[1] && list[0] >= other.list[0];
  }

  operator == (Object other) {
    if (other is Uint128) {
      return list[1] == other.list[1] && list[0] == other.list[0];
    } else if (other is Int128) {
      return list[1] == other.list[1] && list[1] < 0x8000000000000000 && list[0] == other.list[0];
    } else if (other is int) {
      return list[1] == 0 && list[0] == other;
    } else if (other is double) {
      var msw = other / 0xFFFFFFFFFFFFFFFF;
      var lsw = other - msw;
      return list[0] == lsw && list[1] == msw;
    } else {
      return false;
    }
  }

  @override
  // TODO: implement hashCode
  int get hashCode => list[0] ^ list[1];



}

class Int128 {
  Uint64List list = Uint64List(2);

  Int128(int bytes_0_7, int bytes_8_15) {
    // Int128 objects are always stored little endian, regardless of platform.
    if (Endian.host == Endian.little) {
      list[0] = bytes_0_7;
      list[1] = bytes_8_15;
    } else {
      list[1] = bytes_0_7;
      list[0] = bytes_8_15;
    }
  }

  static Int128 fromU64LE(int lsb, int msb) {
    if (Endian.host == Endian.little) {
      return Int128(lsb, msb);
    } else {
      return Int128(msb, lsb);
    }
  }

  ffi.Pointer<ffi.Uint64> asPointer() {
    var ptr = allocate<ffi.Uint64>(count: 2);
    ptr[0] = this.list[0];
    ptr[1] = this.list[1];
    return ptr;
  }

  @override
  bool operator ==(Object other) {
    if (other is Int128) {
      return list[1] == other.list[1] && list[0] == other.list[0];
    } else if (other is Uint128) {
      return list[1] == other.list[1] && list[1] < 0x8000000000000000 && list[0] == other.list[0];
    } else if (other is int) {
      return list[1] == 0 && list[0] == other;
    } else if (other is double) {
      var msw = other / 0xFFFFFFFFFFFFFFFF;
      var lsw = other - msw;
      return list[0] == lsw && list[1] == msw;
    } else {
      return false;
    }
  }

  @override
  // TODO: implement hashCode
  int get hashCode => list[0] ^ list[1];

}

ffi.Pointer<Utf8> wrap_string(String s) {
  throw Exception("not implemented");
}

/*
  The U8Slice class represents a section of memory allocated by the FFI for a
  command return value. The memory must be sent back to the FFI to be deallocated
  when the UI is finished with the value or copies it to an owned String.
 */
class Utf8Slice {
  ffi.Pointer<Utf8> _str;
  bool alive;

  Utf8Slice(ffi.Pointer<Utf8> str) {
    this._str = str;
  }

  String toString() {
    var copy = Utf8.fromUtf8(_str);
    deallocate();
    return copy;
  }

  void deallocate() {
    free_ffi_string(this._str.cast<ffi.Uint8>());
    _str = null;
  }

  ffi.Pointer<Utf8> asRaw() {
    return _str;
  }

  int elementAt(int index) {
    if (_str == null) {
      throw Exception("Attempted to index into a U8Slice which has already been deallocated");
    }
    return _str.cast<ffi.Uint8>()[index];
  }

  void setElementAt(int index, int value) {
    if (_str == null) {
      throw Exception("Attempted to index into a U8Slice which has already been deallocated");
    }
    if (value < 0 || value > 255) {
      throw Exception("Attempted to set a byte of a U8Slice to an out of bounds value: " + value.toString());
    }
    _str.cast<ffi.Uint8>()[index] = value;
  }

  operator [](int index) {
    return elementAt(index);
  }

  operator []=(int index, int value) {
    setElementAt(index, value);
  }
}

class Uuid extends Uint128 {
  Uuid(int leastSignificantBits, int mostSignificantBits) : super(leastSignificantBits, mostSignificantBits);
}

ffi.Pointer<ffi.Uint64> wrap_uuid(Uuid uuid) {
  return uuid.asPointer();
}

