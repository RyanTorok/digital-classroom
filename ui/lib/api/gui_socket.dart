//TYPE ENCODINGS
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:ui/utils/result.dart';
import 'package:uuid/uuid.dart';

import 'package:flutter/cupertino.dart';

const int PACKET_HEADER_SIZE = 32;

const int ENCODING_U8 = 0x1;
const int ENCODING_U16 = 0x2;
const int ENCODING_U32 = 0x3;
const int ENCODING_U64 = 0x4;
const int ENCODING_U128 = 0x5;
const int ENCODING_I8 = 0x6;
const int ENCODING_I16 = 0x7;
const int ENCODING_I32 = 0x8;
const int ENCODING_I64 = 0x9;
const int ENCODING_I128 = 0xA;
const int ENCODING_F32 = 0xB;
const int ENCODING_F64 = 0xC;
const int ENCODING_STR = 0xD;

enum ArgumentType {
  U8, U16, U32, U64, U128, I8, I16, I32, I64, I128, F32, F64, String
}

const String socket_file = "/usr/share/digital-classroom/sockets/gui.sock";
const int port = 1111;

class GuiSocket {

  Socket client;
  HashMap<String, PacketMessage> partialResponses;
  HashMap<String, Completer<Result<List<dynamic>>>> activeCommands;

  GuiSocket(Socket client) {
    this.client = client;
    partialResponses = new HashMap();
    activeCommands = new HashMap();
  }

  static Future<GuiSocket> connect() async {
    final host = InternetAddress(socket_file, type: InternetAddressType.unix);
    final client = await Socket.connect(host, port);
    var guiSocket = new GuiSocket(client);
    client.listen(
            (var data) => guiSocket._receiveData(data),
        onDone: () { print('Done'); client.close(); },
        onError: (e) { print('Got error $e'); client.close(); });
    print("Connection success.");
    return guiSocket;
  }

  Future<Result<List<dynamic>>> _waitForResponse(String messageId) async {
    return await activeCommands[messageId].future;
  }

  void _receiveData(dynamic data) {
    var result = _readPacket(data).ifOk((packet) => {
      _decodeResponse(packet).ifOk((returnVals) {
        if (returnVals != null) {
          var completer = activeCommands[packet.id];
          if (completer == null) {
            leafErr("Received packet for command not sent -- ID = " + packet.id);
          } else {
            completer.complete(returnVals as Result<List<dynamic>>);
            Ok(0);
          }
          true;
        } else {
          false;
        }
      })
    });
  }

  Result<PacketMessage> _readPacket(dynamic dynData) {
    //Remove packet header
    if (!(dynData is Uint8List)) {
      return leafErr("Data was not Uint8List");
    }
    var data = dynData as Uint8List;
    if (data.length < PACKET_HEADER_SIZE) {
      return leafErr("Not enough bytes for packet header in received data.");
    }
    var messageId = Uuid().unparse(data.sublist(0, 16));
    var remainingHeader = ByteData.sublistView(data, 16, 32);
    var payloadLength = remainingHeader.getUint16(0, Endian.little);
    var packetIndex = remainingHeader.getUint16(2, Endian.little);
    var packetLength = remainingHeader.getUint32(4, Endian.little);
    
    return Ok(PacketMessage(messageId, payloadLength, packetIndex, payloadLength, data));
  }

  Result<List<dynamic>> _decodeResponse(Uint8List data) {
    // Command response packet is identical to outbound command packet, except the
    // Opcode field is elided.

    //Return value count
    try {
      var returnValCount = ByteData.sublistView(
          data, PACKET_HEADER_SIZE, PACKET_HEADER_SIZE + 2).getUint16(
          0, Endian.little);
      List<ArgumentType> returnValTypes = List(returnValCount);
      int valuesOffset = PACKET_HEADER_SIZE + 2 + returnValCount;
      List<dynamic> returnVals = List(returnValCount);

      //Return values
      for (int i = 0; i < returnValCount; i++) {
        var typeCode = data[PACKET_HEADER_SIZE + 2 + i];
        switch (typeCode) {
          case ENCODING_U8:
            returnVals[i] = data[valuesOffset];
            valuesOffset++;
            break;
          case ENCODING_U16:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 2)
                    .getUint16(0);
            valuesOffset += 2;
            break;
          case ENCODING_U32:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 4)
                    .getUint32(0);
            valuesOffset += 4;
            break;
          case ENCODING_U64:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 8)
                    .getUint64(0);
            valuesOffset += 8;
            break;
          case ENCODING_U128:
            returnVals[i] =
                Uuid().unparse(data.sublist(valuesOffset, valuesOffset + 16));
            valuesOffset += 16;
            break;
          case ENCODING_I8:
            returnVals[i] = data[valuesOffset];
            valuesOffset++;
            break;
          case ENCODING_I16:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 2)
                    .getInt16(0);
            valuesOffset += 2;
            break;
          case ENCODING_I32:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 4)
                    .getInt32(0);
            valuesOffset += 4;
            break;
          case ENCODING_I64:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 8)
                    .getInt64(0);
            valuesOffset += 8;
            break;
          case ENCODING_I128:
            returnVals[i] =
                Uuid().unparse(data.sublist(valuesOffset, valuesOffset + 16));
            valuesOffset += 16;
            break;
          case ENCODING_F32:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 4)
                    .getFloat32(0);
            valuesOffset += 4;
            break;
          case ENCODING_F64:
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 4)
                    .getFloat32(0);
            valuesOffset += 4;
            break;
          case ENCODING_STR:
          //Temporarily store the string length, we'll replace it with the contents in the next section.
            returnVals[i] =
                ByteData.sublistView(data, valuesOffset, valuesOffset + 4)
                    .getUint32(0);
            valuesOffset += 4;
            break;
        }
      }

      for (int i = 0; i < returnValCount; i++) {
        var typeCode = data[PACKET_HEADER_SIZE + 2 + i];
        if (typeCode == ENCODING_STR) {
          //replace the length with the actual string contents
          returnVals[i] = utf8.decode(
              data.sublist(valuesOffset, valuesOffset + returnVals[i] as int));
        }
      }
      return Ok(returnVals);
    } catch (e) {
      return leafErr("Received error decoding command response: " + e.toString());
    }
  }

  //Sends a command to the GUI socket, and returns the message ID.
  String _sendToSocket(Command command, [String messageId]) {
    if (messageId == null) {
      messageId = Uuid().v4();
    }
    var bytes = _singlePacketEncode(command, messageId);
    print(bytes);
    client.add(bytes.sublist(0, 32));
    client.writeln();
    client.add(bytes.sublist(32));
    client.writeln();
    print("Length = " + bytes.length.toString());
    return messageId;
  }

  Future<Result<List<dynamic>>> sendCommand(Command command) async {
    var id = Uuid().v4();
    activeCommands[id] = new Completer();
    _sendToSocket(command, id);
    var response = await this._waitForResponse(id);
  }
}

class PacketMessage {
  String id;
  int length;
  int total;
  int received;
  Uint8List data;
  
  PacketMessage(String id, int length, int total, int received, Uint8List data) {
    this.id = id;
    this.length = length;
    this.total = total;
    this.received = 1;
    this.data = data;
  }
}

class Command {
  int opcode;
  List<ArgumentType> argument_types;
  List<dynamic> argument_values;

  Command(int opcode, List<ArgumentType> argument_types, List<dynamic> argument_values) {
    this.opcode = opcode;
    this.argument_types = argument_types;
    this.argument_values = argument_values;
  }
}


///
///    singlePacketEncode() returns a byte representation of a command to write to the GUI socket.
///    This encoding assumes a single packet. If multiple packets are needed, this representation
///    can be modified to fit the needs of the particular GUI socket implementation.
///
///    Command packet format:
///    Note: all multi-byte quantities should be expressed in little-endian format, and the byte
///    offsets listed here are 0-indexed.
///
///    [bytes 0-15]  Message ID
///    [bytes 16-19] Message payload length, excluding 32-byte header
///    [bytes 20-21] Packet index (0-indexed)
///    [bytes 22-23] Total number of packets in message (1-indexed -- e.g. if a message's packets
///                  are numbered 0-6, this value should be 7).
///    [bytes 24-27] Packet length of all packets in this message besides the final one (excluding 32 byte header)
///                  All packets should contain the same value, even if the final packet is shorter.
///    [bytes 28-31] Reserved for future use
///    [bytes 32-X]  Message payload data
///
///    Payload data contents (beginning at byte 32 in the packet):
///
///    Command Opcode  [2 bytes]
///    Argument Count  [2 bytes]
///    Argument Types  [1 byte per argument]
///    Argument Values [Size given by argument types] -- String arguments store their lengths here (4 bytes)
///    String contents [Size given by argument values]
///
List<int> _singlePacketEncode(Command command, String messageId) {
  //param checks
  if (command.argument_types.length != command.argument_values.length) {
    throw new Exception("Command encode error: Argument types and values arrays are not the same length -- " +
        "argument_types has length " + command.argument_types.length.toString() +
        ", but argument_values has length " + command.argument_values.length.toString() + ".");
  }

  var command_payload = List<int>();

  //Message ID [16 bytes]
  command_payload.addAll(utf8.encode(messageId));

  // Placeholder for rest of command header [16 bytes].
  // We don't know the values until we encode the command.
  for (int i = 16; i < PACKET_HEADER_SIZE; i++) {
    command_payload.add(0);
  }

  //Command opcode [2 bytes]
  command_payload.add(command.opcode & 0xFF);
  command_payload.add((command.opcode >> 8) & 0xFF);

  //Argument count [2 bytes]
  command_payload.add(command.argument_types.length & 0xFF);
  command_payload.add((command.argument_types.length >> 8) & 0xFF);

  //Argument types [1 byte each]
  for (final type in command.argument_types) {
    switch (type) {
      case ArgumentType.U8:
        command_payload.add(ENCODING_U8);
        break;
      case ArgumentType.U16:
        command_payload.add(ENCODING_U16);

        break;
      case ArgumentType.U32:
        command_payload.add(ENCODING_U32);
        break;
      case ArgumentType.U64:
        command_payload.add(ENCODING_U64);
        break;
      case ArgumentType.U128:
        command_payload.add(ENCODING_U128);
        break;
      case ArgumentType.I8:
        command_payload.add(ENCODING_I8);
        break;
      case ArgumentType.I16:
        command_payload.add(ENCODING_I16);
        break;
      case ArgumentType.I32:
        command_payload.add(ENCODING_I32);
        break;
      case ArgumentType.I64:
        command_payload.add(ENCODING_I64);
        break;
      case ArgumentType.I128:
        command_payload.add(ENCODING_I128);
        break;
      case ArgumentType.F32:
        command_payload.add(ENCODING_F32);
        break;
      case ArgumentType.F64:
        command_payload.add(ENCODING_F64);
        break;
      case ArgumentType.String:
        command_payload.add(ENCODING_STR);
        break;
    }
  }
  //Argument values
  int i = 0;
  for (final type in command.argument_types) {
    switch (type) {
      case ArgumentType.U8:
        command_payload.add((command.argument_values[i] & 0xFF) as int);
        break;
      case ArgumentType.U16:
        var buf = Uint8List(2);
        buf..buffer.asByteData().setUint16(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.U32:
        var buf = Uint8List(4);
        buf..buffer.asByteData().setUint32(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.U64:
        var buf = Uint8List(8);
        buf..buffer.asByteData().setUint64(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.U128:
        command_payload.add(ENCODING_U128);
        var uuid = command.argument_values[i] as List<int>;
        command_payload.addAll(uuid);
        break;
      case ArgumentType.I8:
        command_payload.add((command.argument_values[i] & 0xFF) as int);
        break;
      case ArgumentType.I16:
        var buf = Uint8List(2);
        buf..buffer.asByteData().setInt16(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.I32:
        var buf = Uint8List(4);
        buf..buffer.asByteData().setInt32(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.I64:
        var buf = Uint8List(8);
        buf..buffer.asByteData().setInt64(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.I128:
        var uuid = command.argument_values[i] as List<int>;
        command_payload.addAll(uuid);
        break;
      case ArgumentType.F32:
        var buf = Uint8List(4);
        buf..buffer.asByteData().setFloat32(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.F64:
        var buf = Uint8List(8);
        buf..buffer.asByteData().setFloat64(0, command.argument_values[i], Endian.little);
        command_payload.addAll(buf);
        break;
      case ArgumentType.String:
        var buf = Uint8List(4);
        buf..buffer.asByteData().setUint32(0, (command.argument_values[i] as String).length, Endian.little);
        command_payload.addAll(buf);
        break;
    }
    i++;
  }

  //String contents
  int j = 0;
  for (final type in command.argument_types) {
    if (type == ArgumentType.String) {
      command_payload.addAll(utf8.encode(command.argument_values[j]));
    }
    j++;
  }

  //Now that we have the entire command message, we can go back and fill in the packet header.

  //command payload length (excluding header bytes) [4 bytes]
  var buf = Uint8List(4);
  buf..buffer.asByteData().setUint32(0, command_payload.length - PACKET_HEADER_SIZE, Endian.little);
  command_payload.setRange(16, 20, buf);

  //Packet index [2 bytes] (We're only setting the first packet right now so it's 0)
  buf = Uint8List(2);
  var packetIndex = 0;
  buf..buffer.asByteData().setUint16(0, packetIndex, Endian.little);
  command_payload.setRange(20, 22, buf);


  // Number of packets [2 bytes] (always 1 for now, we'll edit it within the socket API if the
  // particular implementation needs the message sent in parts)
  buf = Uint8List(2);
  var packetCount = 1;
  buf..buffer.asByteData().setUint16(0, packetCount, Endian.little);
  command_payload.setRange(22, 24, buf);

  //Packet length [4 bytes] (Same as payload length if we only have only one packet)
  buf = Uint8List(4);
  buf..buffer.asByteData().setUint32(0, command_payload.length - PACKET_HEADER_SIZE, Endian.little);
  command_payload.setRange(24, 28, buf);

  //Bytes 28-31 are currently unused, we'll leave them as 0.
  
  return command_payload;
}
