class ErrSpec {
  String message;
  ErrSpec causedBy;

  static ErrSpec fromMessage(String message) {
    return ErrSpec(message, null);
  }

  ErrSpec(String message, ErrSpec causedBy) {
    this.message = message;
    this.causedBy = causedBy;
  }
}