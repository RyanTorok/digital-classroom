import 'errspec.dart';

class Result<O> {
  bool ok;
  O item;
  ErrSpec err;

  O unwrap() {
    if (ok)
      return item;
    else throw new Exception("Tried to call unwrap() on a Result that was not ok.");
  }

  ErrSpec unwrapErr() {
    if (!ok)
      return err;
    else throw new Exception("Tried to call unwrap_err() on a Result that was ok.");
  }

  Result<T> ifOk<T>(T function(O), [String messageOnError]) {
    if (this.ok) {
      return Ok(function(this.unwrap()));
    } else {
      return Err(ErrSpec(messageOnError, this.unwrapErr()));
    }
  }
}

Result<O> leafErr<O>(String msg) {
  return Err(ErrSpec.fromMessage(msg));
}

Result<O> Ok<O>(O item) {
  var result = Result();
  result.ok = true;
  result.item = item;
  return result;
}

Result<O> Err<O>(ErrSpec e) {
  var result = Result();
  result.ok = false;
  result.err = e;
  return result;
}