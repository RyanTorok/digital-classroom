# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/rtorok/digital_classroom/ui/linux/flutter/generated_plugin_registrant.cc" "/home/rtorok/digital_classroom/ui/linux/cmake-build-debug/CMakeFiles/ui.dir/flutter/generated_plugin_registrant.cc.o"
  "/home/rtorok/digital_classroom/ui/linux/main.cc" "/home/rtorok/digital_classroom/ui/linux/cmake-build-debug/CMakeFiles/ui.dir/main.cc.o"
  "/home/rtorok/digital_classroom/ui/linux/my_application.cc" "/home/rtorok/digital_classroom/ui/linux/cmake-build-debug/CMakeFiles/ui.dir/my_application.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../flutter/ephemeral"
  "/usr/include/gtk-3.0"
  "/usr/include/at-spi2-atk/2.0"
  "/usr/include/at-spi-2.0"
  "/usr/include/dbus-1.0"
  "/usr/lib/x86_64-linux-gnu/dbus-1.0/include"
  "/usr/include/gio-unix-2.0"
  "/usr/include/cairo"
  "/usr/include/pango-1.0"
  "/usr/include/harfbuzz"
  "/usr/include/atk-1.0"
  "/usr/include/pixman-1"
  "/usr/include/freetype2"
  "/usr/include/libpng16"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
