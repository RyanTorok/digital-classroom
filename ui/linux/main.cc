#include "my_application.h"
#include <iostream>

int main(int argc, char** argv) {
    printf("hello!\n");
    g_autoptr(MyApplication) app = my_application_new();
    return g_application_run(G_APPLICATION(app), argc, argv);
}
