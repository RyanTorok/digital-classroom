all: commands client ui

commands:
	python autogen_commands/main.py 64

client:
	cd client && $(MAKE) 

client-linux:
	cd client && $(MAKE) linux 

ui-linux:
	cd ui && flutter build linux

linux: client-linux ui-linux

client-android:
	cd client && $(MAKE) android

ui-android:
	cd ui && flutter build android

android: client-android ui-android

run-linux: client-linux
	cd ui && flutter run -d linux

test-linux:
	sudo true
	cd client && $(MAKE) run &
	cd ui && flutter test test/*.dart;


run-android: client-android
	cd ui && flutter run -d XT1650
