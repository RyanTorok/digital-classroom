import psycopg2 as pg
import datetime

# connect to dcl_devel database
conn = pg.connect("dbname=dcl_devel user=rtorok")

cursor = conn.cursor()

DB_UPDATE = True


def valid_mode(mode):
    return mode == 'linked' or mode == 'socket' or mode == 'none'


def valid_type(type):
    return type == 'int8' or \
           type == 'int16' or \
           type == 'int32' or \
           type == 'int32' or \
           type == 'int64' or \
           type == 'int128' or \
           type == 'uint8' or \
           type == 'uint16' or \
           type == 'uint32' or \
           type == 'uint32' or \
           type == 'uint64' or \
           type == 'uint128' or \
           type == 'uuid' or \
           type == 'string' or \
           type == 'binary' or \
           type == 'pointer'


def valid_err_code(name):
    cursor.execute("SELECT COUNT(*) FROM error_codes WHERE name = '{}'".format(name))
    return cursor.fetchone()[0] != 0


def get_err_code(name):
    cursor.execute("SELECT code FROM error_codes WHERE name = '{}'".format(name))
    return cursor.fetchone()[0]


str_opcode = input('Command opcode: ')
while True:
    try:
        opcode = int(str_opcode)
        break
    except ValueError:
        str_opcode = input('Try again: ')

name = str(input("Command name: "))
while len(name) == 0:
    name = str(input("Try again: "))
mode_desktop = input("Mode on desktop [linked, socket, none]: ")
while not valid_mode(mode_desktop):
    mode_desktop = input("Try again: ")
mode_web = input("Mode on web [linked, socket, none]: ")
while not valid_mode(mode_web):
    mode_web = input("Try again: ")
mode_mobile = input("Mode on mobile [linked, socket, none]: ")
while not valid_mode(mode_mobile):
    mode_mobile = input("Try again: ")
unsafe = input("Is command unsafe? [y/n]: ").lower()
while unsafe != 'y' and unsafe != 'yes' and unsafe != 'n' and unsafe != 'no':
    unsafe = input("Try again: ").lower()

def get_args(prompt):
    variables = []
    i = 0
    while True:
        type = ''
        while not valid_type(type):
            type = input(prompt + " " + str(i) + " type [int8, int16, int32, int64, int128, uint8, uint16, uint32, "
                                                 "uint64, uint128, "
                                                 "uuid, string, binary, pointer]: ")
            if len(type) == 0:
                break
        if len(type) == 0:
            break
        name = ''
        if prompt == 'Argument':
            name = input("Argument " + str(i) + " name: ")
            # _r is reserved as the name for the internal variable in the dart script
            while len(name) == 0 or not name.isalnum() or not str(name[0]).isalpha() or name == '_r':
                name = input("Try again: ")
        arg_description = input(prompt + " " + str(i) + " description: ")
        while len(arg_description) == 0:
            arg_description = input("Try again: ")
        variables.append((name, type, arg_description))
        i += 1
    return variables


description = input("Command description: ")
while len(description) == 0:
    description = input("Try again: ")

args = get_args("Argument")
return_values = get_args("Return value")
i = 0
throws = []
while True:
    error_name = ''
    while not valid_err_code(error_name):
        error_name = input("Throws error code " + str(i) + ": ")
        if len(error_name) == 0:
            break
    if len(error_name) == 0:
        break
    err_description = input("Error " + str(i) + " description: ")
    while len(err_description) == 0:
        err_description = input("Try again: ")
    throws.append((get_err_code(error_name), error_name, err_description))
    i += 1

for arg in args:
    if arg[1] == 'string' or arg[1] == 'int128' or arg[1] == 'uint128' or arg[1] == 'uuid' or arg[1] == 'pointer':
        throws.append((get_err_code("ERR_NONUTF8"), "ERR_NONUTF8", "If any argument string is not valid UTF-8"))
        throws.append((get_err_code("ERR_NULLPTR"), "ERR_NULLPTR", "If any pointer argument is null"))
        break


if mode_desktop == 'socket' or mode_web == 'socket' or mode_mobile == 'socket':
    throws.append((get_err_code("ERR_NOBACKEND"), "ERR_NOBACKEND",
                   "If this command is invoked in socket mode and the backend is not available"))

throws.sort(key=lambda t: t[0])

if unsafe == 'no' or unsafe == 'n':
    bool_unsafe = False
else:
    bool_unsafe = True

if DB_UPDATE:
    cursor.execute("INSERT INTO commands VALUES ({}, '{}', '{}', '{}', '{}', '{}', {});".format(opcode, name, mode_desktop, mode_web,
                   mode_mobile, description, bool_unsafe))
    i = 0
    for arg in args:
        cursor.execute("INSERT INTO arguments (command, name, type, index, description) values ({}, '{}', '{}', {}, '{}');"
                       .format(opcode, arg[0], arg[1], i, arg[2]))
        i += 1

    i = 0
    for return_val in return_values:
        cursor.execute("INSERT INTO return_values (command, type, index, description) values ({}, '{}', {}, '{}');"
            .format(opcode, return_val[1], i, return_val[2]))
        i += 1

    i = 0
    for throw in throws:
        cursor.execute("INSERT INTO throws (command, error_code, description) values ({}, {}, '{}');"
                       .format(opcode, throw[0],  throw[2]))
        i += 1

rust_argument_types = {
    'int8': 'i8',
    'int16': 'i16',
    'int32': 'i32',
    'int64': 'i64',
    'int128': 'i128',
    'uint8': 'u8',
    'uint16': 'u16',
    'uint32': 'u32',
    'uint64': 'u64',
    'uint128': 'u128',
    'float32': 'f32',
    'float64': 'f64',
    'uuid': 'uuid::Uuid',
    'string': '&str',
    'binary': '&[u8]',
    'pointer': '*mut u8',
}

rust_return_types = {
    'int8': 'i8',
    'int16': 'i16',
    'int32': 'i32',
    'int64': 'i64',
    'int128': 'i128',
    'uint8': 'u8',
    'uint16': 'u16',
    'uint32': 'u32',
    'uint64': 'u64',
    'uint128': 'u128',
    'float32': 'f32',
    'float64': 'f64',
    'uuid': 'uuid::Uuid',
    'string': 'String',
    'binary': 'Vec<u8>'
}

rust_default_return_values = {
    'int8': '0i8',
    'int16': '0i16',
    'int32': '0i32',
    'int64': '0i64',
    'int128': '0i128',
    'uint8': '0u8',
    'uint16': '0u16',
    'uint32': '0u32',
    'uint64': '0u64',
    'uint128': '0u128',
    'float32': '0.0f32',
    'float64': '0.0f64',
    'uuid': 'uuid::Uuid::nil()',
    'string': 'String::new()',
    'binary': 'vec![]'
}

if bool_unsafe:
    str_unsafe = 'unsafe '
else:
    str_unsafe = ''

# create new rust file for command
function = "pub(crate) {0}fn {1}(".format(str_unsafe, name)
for i in range(len(args)):
    arg = args[i]
    function += arg[0] + ": " + rust_argument_types[arg[1]]
    if i < len(args) - 1:
        function += ", "
function += ') -> '

# do we have any return values besides the exit code?
if len(return_values) == 0:
    function += ' u16 {'
    placeholder = 'OK'
else:
    function += '(u16, '
    placeholder = '(OK, '
    for i in range(len(return_values)):
        ret = return_values[i]
        function += rust_return_types[ret[1]]
        placeholder += rust_default_return_values[ret[1]]
        if i < len(return_values) - 1:
            function += ', '
            placeholder += ', '
    function += ') {'
    placeholder += ')'

function += '\n\t' + placeholder + '\n}'

date = datetime.datetime.now().strftime("%d %b %Y")


file_docs = '// FILE: commands/cmd_' + name + '.rs\n' \
       '// \n' \
       '// DESCRIPTION: Implementation of the command ' + name + '\n' \
       '// \n' \
       '// CHANGE LOG:\n' \
       '// ' + date + ' -- Initial generation \n' \

text_width = 100


def split_description_comment(description, width):
    i = 0
    string = ''
    while i + width < len(description):
        off = 0
        while not description[i + width + off].isspace():
            off += 1
            if len(description) <= i + width + off:
                break
        else:
            string += '/// ' + description[i:i + width + off] + '\n'
            # add 1 extra so the next line doesn't start with the space we broke on
            i += i + width + off + 1
            # just a hack to make the break above act as a 'break outer'
            continue
        break

    string += '/// ' + description[i:] + '\n'
    return string


includes = 'use crate::command_utils::err_codes::OK;\n'


function_docs = '/// Command: ' + name + '\n' \
                '/// \n' \
                '' + split_description_comment(description, text_width) + '/// \n' \
                '/// Arguments:\n'
for arg in args:
    function_docs += '/// ' + arg[0] + ' (' + arg[1] + ') ' + arg[2] + '\n'
if len(args) == 0:
    function_docs += '/// None\n'
function_docs += '/// \n' \
                 '/// Returns:\n'
for ret in return_values:
    function_docs += '/// (' + ret[1] + ') ' + ret[2] + '\n'
if len(return_values) == 0:
    function_docs += '/// None\n'
function_docs += '/// \n' \
                 '/// Throws:\n'
for throw in throws:
    function_docs += '/// ' + throw[1] + ' -- ' + throw[2] + '\n'
if len(throws) == 0:
    function_docs += '/// None\n'


new_command_file = open('client/src/commands/cmd_' + name + '.rs', 'w')
new_command_file.seek(0)
new_command_file.write(file_docs + '\n' + includes + '\n' + function_docs + '\n' + function)
new_command_file.truncate()
new_command_file.close()

conn.commit()
conn.close()
