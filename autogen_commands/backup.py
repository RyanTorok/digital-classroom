if need_match:
    function_body = '\tmatch {\n' + tab_lines(function_body) + '\n\t} {\n' + '\t\tOk(r) => '
    if len(return_values) > 0:
        function_body += 'wrap_c_tuple_' + str(len(return_values) + 1) + '(OK, '
    else:
        function_body += 'OK'
    for j in range(len(return_values)):
        function_body += 'r.' + str(j)
        if j < len(return_values) - 1:
            function_body += ', '
    if len(return_values) > 0:
        function_body += ')'
    function_body += ',\n\t\tErr(e) => '
    if len(return_values) > 0:
        function_body += 'wrap_c_tuple_' + str(len(return_values) + 1) + '(e, '
    else:
        function_body += 'e'
    for r in range(len(return_values)):
        function_body += rust_default_values[return_values[r][0]]
        if r < len(return_values) - 1:
            function_body += ', '
    if len(return_values) > 0:
        function_body += ')'
    function_body += ',\n\t\t_ =>{ unreachable!() }\n\t}\n'
