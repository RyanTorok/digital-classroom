using System.Threading.Tasks;

namespace Main.API
{
    public class API
    {
        private GUI_Socket _socket;

        public void AttachGUI()
        {
            GUI_Socket socket = new GUI_Socket();
            socket.Connect();
        }
        
        //Command Example

        public Task<Result<ExampleResponse>> Example(int a, int b, int c)
        {
            Task<Result<ExampleResponse>> task = new Task<Result<ExampleResponse>>(() =>
            {
                CommandResponse response = _socket.SendCommandForResponse(new ExampleSend(a, b, c));
                response.getInt(0);

            });
        }

        private class ExampleSend : Command
        {
            public ExampleSend(int i, int i1, int i2)
            {
                throw new System.NotImplementedException();
            }

            public override Opcode Name()
            {
                return Opcode.Example;
            }

            public override object[] Arguments()
            {
                
            }
        }
        
        public class ExampleResponse : CommandResponse
        {
            private int d, e, f;

            private ExampleResponse(int d, int e, int f)
            {
                this.d = d;
                this.e = e;
                this.f = f;
            }
        }
    }
}