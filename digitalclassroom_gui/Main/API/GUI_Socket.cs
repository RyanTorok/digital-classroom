using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Text;
using Socket = System.Net.Sockets.Socket;
using SocketType = System.Net.Sockets.SocketType;
using System.Threading;
using System.Threading.Tasks;
using Main.API;

namespace Main
{
    public class GUI_Socket
    {
        private const string SOCKET_FILE =
            "/usr/share/digital-classroom/sockets/gui.sock";

        private const int BufferSize = 65536;
        private byte[] buf = new byte[BufferSize];
        private readonly BlockingCollection<string> _responseQueue = new BlockingCollection<string>();
        private StringBuilder _buildCommand;
        
        //tell the receive loop to stop looping
        private bool _stopRequested = false;
        
        private Socket _socket;
        private static readonly Encoding Encoding = Encoding.UTF8;
        private Thread _receiveLoop;

        public void Connect()
        {
            _socket = new Socket(AddressFamily.Unix, SocketType.Stream, ProtocolType.IP);
            var unixEp = new UnixEndPoint(SOCKET_FILE);
            try
            {
                _socket.Connect(unixEp);
            }
            catch (SocketException e)
            {
                Console.Error.WriteLine("Fatal error: could not connect socket: " + e.Message);
                Environment.Exit(-1); 
            }
            _receiveLoop = new Thread(LoopReceive);
            _receiveLoop.Start();
        }

        ~GUI_Socket()
        {
            _stopRequested = true;
            _socket?.Close();
        }

        private void Send(string msg)
        {
            _socket.Send(Encoding.GetBytes(escape(msg)));
            _socket.Send(new[] {(byte) '\n'});
        }

        private string ReceiveLine()
        {
            return _responseQueue.Take();
        }

        private void LoopReceive()
        {
            //Receive should immediately return if the socket closes, so we can break out of the loop.
            while (!_stopRequested)
            {
                Receive();
            }
        }

        private void Receive()
        {
            if (_buildCommand == null)
                _buildCommand = new StringBuilder();
            int receivedBytes = _socket.Receive(buf);
            int start = 0;
            for (int i = 0; i < receivedBytes; i++)
            {
                if (buf[i] == (byte) '\n')
                {
                    _responseQueue.Add(_buildCommand.ToString());
                    _buildCommand = new StringBuilder();
                }
                _buildCommand.Append((char) buf[i]);
            }
        }
        
        private string escape(string msg)
        {
            return msg.Replace("\\", "\\\\").Replace("\n", "\\n");
        }

        internal CommandResponse SendCommandForResponse(Command command)
        {
            Send(command.ToString());
            return CommandResponse.parse(ReceiveLine());
        }
    }
}