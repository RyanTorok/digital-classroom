using System;

namespace Main.API
{
    public class Result<T>
    {
        private bool ok;
        private T item;
        private Backtrace error;

        public Result(Backtrace bt)
        {
            ok = false;
            error = bt;
        }

        public Result(T item)
        {
            ok = true;
            this.item = item;
        }
    


    public T unwrap()
        {
            if (!ok) 
                throw new StateException("Attempting to call unwrap() on a Result that was an error.");
            return item;
        }

        public Backtrace unwrapError()
        {
            if (ok)
                throw new StateException("Attempting to call unwrapError() on a Result that was ok.");
            return error;
        }
        
        public static Result<TS> unwrapOrWrapError<TS>(Result<TS> result, string messageOnError)
        {
            if (result.ok)
                return result;
            return wrapErr(messageOnError, result);
        }

        public static Result<TS> wrapErr<TS>(string message, Result<TS> causedBy)
        {
            return new Result<TS>(causedBy.unwrapError().wrapIn(message));
        }

    }

    public class Backtrace 
    {
        private string errorMessage;
        private Backtrace causedBy;

        public Backtrace(string errorMessage, Backtrace causedBy)
        {
            this.errorMessage = errorMessage;
            this.causedBy = causedBy;
        }

        public Backtrace wrapIn(string message)
        {
            return new Backtrace(message, this);
        }
    }

    class StateException : Exception
    {
        public StateException()
        {
        }

        public StateException(String msg) : base(msg)
        {
        }
    }
}