namespace Main
{
    public abstract class Command
    {
        public abstract Opcode Name();
        public abstract object[] Arguments();
    }

    public enum Opcode
    {
        Example
    }
}