use crate::identifiers::uuid::{Uuid, NULL_UUID};
use crate::command_executor;
use crate::identifiers::address::Address;
use crate::utils::digital_maps::floor_plan::FloorPlan;

//ADMIN commands

//create_account: creates a new account on the specified domain; returns domain-unique user_id
fn create_account(domain_id: Uuid, username: &str, password: &str) -> Uuid {
    NULL_UUID
}


//login: authenticates a returning user.
fn login(domain_id: Uuid, username: &str, password: &str) {
}

//CLASS commands

//new_campus: initializes a new campus in the given domain; returns campus_id
fn new_campus(campus_name: &str, address: Address, map_encoding: FloorPlan) -> Uuid {
    NULL_UUID
}
