extern crate libc;

use libc::execvp;
use libc::c_uchar;
use std::ffi::CString;
use libc::c_int;
use libc::c_schar;
use std::ptr::null;

pub fn fork() -> i32 {
    unsafe { return libc::fork(); }
}

pub fn execl(path: &str, arg: &str) -> Result<(), &'static str> {
    println!("Executing {}", path);
    let code: c_int;
    let mut mangled_file: String = String::from("AAAAAAAA");
    mangled_file.push_str(path);
    let mut mangled_arg = String::from("AAAAAAAA");
    mangled_arg.push_str(arg);
    let path_c_string = ((CString::new(mangled_file).expect("CString failed").as_ptr()) as u64 + 8) as *const i8;
    let arg_c_string = ((CString::new(mangled_arg).expect("CString failed").as_ptr()) as u64 + 8) as *const i8;

    code = unsafe { libc::execl(path_c_string, path_c_string, arg_c_string, 0 as *const libc::c_schar) };

    return Err("Failed to exec with error code");
}