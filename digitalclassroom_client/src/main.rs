mod api;
mod classes;
mod command_executor;
mod search_engine;
mod tools;
mod utils;
mod identifiers;
mod gui_socket;
mod system;
mod filesystem;

use std::env;
use std::io;
use command_executor::executor;
use crate::utils::console_options::ConsoleOptions;
use std::io::Write;
use crate::gui_socket::{attach, start_listener};

const VERSION: f32 = 0.1;

fn main() {

    let args: Vec<String> = env::args().collect();
    let mut properties = ConsoleOptions::new();
    let mut first = true;

    let read_arg = |args: &&Vec<String>, i: usize, expected: &str, after: &str| -> String {
        let maybe_argument = args.get(i);
        if maybe_argument.is_some() {
           return maybe_argument.unwrap().to_owned();
        } else {
            panic!("error: expected {} after {}", expected, after);
        }
    };

    let parse_arg = |args: &Vec<String>, i: usize, properties: &mut ConsoleOptions| -> u32 {
        let _arg = read_arg(&args, i, "argument", "<should never see this>");
        let arg = _arg.as_ref();
        match arg {
            "-d" | "--debug" => properties.debug = true,

            "-g" | "--gui" => {
                properties.gui = Option::from(read_arg(&args, i + 1, "path to GUI", arg).to_string());
                return 2;
            },

            "-h" | "--help" => println!("Digital Classroom, Version {:.1}\n HELP HERE", VERSION),

            "--version" => println!("Digital Classroom, Version {:.1}", VERSION),

            _ => panic!("error: unexpected argument '{}'", arg)
        };
        return 1;
    };

    let mut i: u32 = 1;
    while i < args.len() as u32 {
        if first {
            first = false;
            continue;
        }
        i += parse_arg(&args, i as usize, &mut properties);
    }

    //if we don't specify a GUI to connect, then start the CLI.
    if properties.gui.is_none() {
        println!("Welcome to Digital Classroom, Version {:.1}", VERSION);
        println!("Connect any compliant GUI, or type commands directly to debug.");
        let reader = io::stdin();
        loop {
            print!(">>> ");
            let _result = io::stdout().flush();
            let mut buf = String::new();
            let _result = reader.read_line(&mut buf);
            executor::execute(buf);
        }
    } else {
        attach(properties.gui.unwrap());
    }
    start_listener();
    panic!("Failed to start dcl_gui_socket listener.");
}
