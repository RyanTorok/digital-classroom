
pub struct Uuid {
    least_significant_bits: u64,
    most_significant_bits: u64
}

pub(crate) const NULL_UUID: Uuid = Uuid {least_significant_bits: 0, most_significant_bits: 0};

impl Uuid {

    fn from_bytes_le(bytes: &[u8]) -> Uuid {
        let mut lsb: u64 = 0;
        let mut msb: u64 = 0;
        for i in 0..8 {
            lsb = lsb | (bytes[i] << (i * 8) as u8) as u64
        }
        for j in 8..16 {
            msb = msb | (bytes[j] << ((j - 8) * 8) as u8) as u64
        }
        return Uuid {
            least_significant_bits: lsb,
            most_significant_bits: msb
        }
    }

    fn from_bytes_be(bytes: &[u8]) -> Uuid {
        let mut lsb: u64 = 0;
        let mut msb: u64 = 0;
        for i in 0..8 {
            msb = (msb << 8) | bytes[i] as u64;
        }
        for j in 8..16 {
            lsb = (lsb << 8) | bytes[j] as u64;
        }
        return Uuid {
            least_significant_bits: lsb,
            most_significant_bits: msb
        }
    }

    fn get_byte_le(&self, index: u8) -> u8 {
        if (index >= 16) {
            return 0;
        }
        if (index < 8) {
            return (self.least_significant_bits >> (8 * index) as u64 & 0xFF) as u8;
        } else {
            return (self.most_significant_bits >> (8 * (index - 8)) as u64 & 0xFF) as u8;
        }
    }
}