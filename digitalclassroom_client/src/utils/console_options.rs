pub struct ConsoleOptions {
    pub debug: bool,
    pub gui: Option<String>
}

impl ConsoleOptions {

    pub(crate) fn new() -> ConsoleOptions {
        return ConsoleOptions{debug: false, gui: None};
    }
}
