use crate::system::linux_syscalls;
use crate::system::linux_syscalls::{fork, execl};
use std::net::{TcpStream};
use std::io::{Read, Write, Error, BufRead};
use std::str::from_utf8;
use std::os::unix::net::{UnixStream, UnixListener};
use std::thread;
use std::time::Duration;
use std::io::BufReader;
use crate::command_executor::executor;
use std::path::Path;

const SOCKET_PATH: &str = "./temp/sockets/";
const SOCKET_FILE_NAME: &str = "dcl_gui_socket";

// Starts the Unix Server Socket to receive requests from programs started by calling attach() below.
// This function should never return unless there is an error.
pub fn start_listener() -> bool {
    let path = Path::new(SOCKET_PATH);
    let success = init_socket_dir(&path);
    if !success {
        return false;
    }
    let mut buf = path.to_path_buf().to_path_buf();
    buf.push(Path::new(SOCKET_FILE_NAME));
    let socket_path = buf.as_path();

    let _listener: Result<std::os::unix::net::UnixListener, Error> = UnixListener::bind(socket_path);
    let listener: UnixListener;
    if _listener.is_ok() {
        listener = _listener.unwrap();
    } else {
        panic!("listener was null. {}", _listener.expect_err("").to_string());
    }

    for mut stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                /* connection succeeded */
                thread::spawn(|| handle_gui_client(stream));
                println!("Connection success.");
            }
            Err(err) => {
                /* connection failed */
                println!("Connection failed.");
                break;
            }
        }
    }
    return true;
}

// Runs the client program using fork-exec. The logic in start_listener() above will handle the
// socket requests the child program will presumably make.
pub fn attach(gui: String) -> bool {

    // Start the GUI by fork/exec.
    let pid = fork();
    if pid == 0 {
        // Child - execute the parameter program
        let result = execl("/bin/sh", &gui);
        if result.is_err() {
            panic!(result.expect_err("failed to exec."));
        }
    } else {
        // Parent - move on like nothing even happened.
    }
    //Only the parent should return from this function
    return true;
}

// Helper functions to set up the sockets and run commands received by the GUI

fn handle_gui_client(stream: UnixStream) {
    let mut reader = BufReader::new(stream);
    loop {
        let mut response = String::new();
        let result = reader.read_line(&mut response);
        if result.is_err() {
            eprintln!("error: failed to read instruction from gui");
        } else {
            println!("Received message: {}", response);
            let handle_cmd = thread::spawn(|| executor::decode_execute(response));
        }
    }
}

fn init_socket_dir(socket_path: &Path) -> bool {
    if !socket_path.exists() {
        std::fs::create_dir_all(socket_path);
    } else {
        let mut buf = socket_path.to_path_buf();
        buf.push(Path::new(SOCKET_FILE_NAME));
        let path = buf.as_path();
        // We can't create a socket if the socket file already exists.
        // We should be the only instance running, so we're safe to delete the old socket file.
        if path.exists() {
            let result = std::fs::remove_file(path);
            if result.is_err() {
                return false;
            }
        }
    }
    return true;
}

//might not need this function if we can resuse the same socket file for all concurrently running GUIs.
/*fn allocate_socket(socket_id: i32) -> Result<UnixListener, &'static str> {
    //try to create a socket of the form ./temp/sockets/dcl_socket_<pid>
    let path: &str = "./temp/sockets/";
    let dir = Path::new(path);
    if !dir.exists() {
        std::fs::create_dir(dir);
    }
    let path_str = format!("{}dcl_socket_{}", path, pid);
    let path = Path::new::<str>(path_str.as_ref());
    let option = path.to_str();
    if option.is_some() {
        let listener: Result<std::os::unix::net::UnixListener, Error> = UnixListener::bind(option.unwrap());
        if listener.is_err() {
            return Err("An error occurred when creating the socket listener.");
        } else {
            return Ok(listener.unwrap());
        }
    } else {
        return Err("An error occurred when constructing the socket path.")
    }

*/