use std::thread;

pub fn execute(_cmd: String) {
}

pub fn decode_execute(_cmd: String) {
    let cmd = gui_decode(unescape(_cmd));
    println!("Hello i'm a thread! You told me to execute {0}", &cmd);
    execute(cmd);
}

fn gui_decode(cmd: String) -> String {
    return cmd;
}

fn unescape(cmd: String) -> String {
    return cmd.replace("\\n", "\n").replace("\\\\", "\\");
}
